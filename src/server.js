import fallback from 'express-history-api-fallback'
import express from 'express'


const app = express()
const root = `${__dirname}/public`

app.use(express.static(root))
app.use(fallback(__dirname + '/public/index.html'))

app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '/public/index.html'), function(err) {
        if (err) {
        res.status(500).send(err)
        }
    })
})