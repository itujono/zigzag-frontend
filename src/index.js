import React from "react"
import ReactDOM from "react-dom"
import { App } from "./components/App"
import "./styles/app.scss"
import FastClick from "react-fastclick-alt"

// prettier-ignore
ReactDOM.render(<FastClick><App /></FastClick>, document.getElementById("app") )
