import * as Types from "../constants"
import { instance } from "../common"

export const loadingProduct = (condition = true) => ({ type: Types.LOADING_PRODUCT, condition })
export const productError = message => ({ type: Types.PRODUCT_ERROR, payload: message })
export const removeProductError = error => ({ type: Types.REMOVE_PRODUCT_ERROR, payload: error })

export const fetchProducts = () => dispatch => {
    dispatch(loadingProduct())
    instance
        .get(`/product/list`)
        .then(response => {
            dispatch({ type: Types.FETCH_PRODUCTS, payload: response.data.result })
        })
        .catch(err => {
            if (err) {
                dispatch(loadingProduct(false))
            }
        })
}

export const fetchProductsByCategory = categoryId => dispatch => {
    dispatch(loadingProduct())
    instance
        .get(`/product/categories/${categoryId}`)
        .then(response => {
            dispatch({ type: Types.FETCH_PRODUCTS_BY_CATEGORY, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const fetchLatestProducts = () => dispatch => {
    dispatch(loadingProduct())
    instance
        .get(`/product/list/null/1/null/null/null/null/null`)
        .then(response => {
            dispatch({ type: Types.FETCH_LATEST_PRODUCTS, payload: response.data.result })
        })
        .catch(err => {
            if (err) {
                dispatch(loadingProduct(false))
            }
        })
}

export const fetch10LatestProducts = () => dispatch => {
    dispatch(loadingProduct())
    instance
        .get(`/product/list/1/1`)
        .then(response => {
            dispatch({ type: Types.FETCH_10_LATEST_PRODUCTS, payload: response.data.result })
        })
        .catch(err => {
            if (err) {
                if (err.code === "ECONNABORTED") {
                    console.log("Kok lama fetch 10 latest nya?")
                    dispatch(productError("timeout"))
                }
                dispatch(loadingProduct(false))
            }
        })
}

export const fetchProductItem = productId => dispatch => {
    dispatch(loadingProduct())
    instance
        .get(`/product/detail/${productId}`)
        .then(response => {
            dispatch({ type: Types.FETCH_PRODUCT_ITEM, payload: response.data.result })
        })
        .catch(err => console.log(`Error kali ini dipersembahkan oleh: ${err.response}`))
}

export const fetchCategories = () => dispatch => {
    return instance
        .get(`/categories`)
        .then(response => {
            dispatch({ type: Types.FETCH_CATEGORIES, payload: response.data.result })
        })
        .catch(err => console.log(err.response))
}

export const fetchComments = productId => dispatch => {
    dispatch(loadingProduct())
    instance
        .get(`/product/show_comment_product/${productId}`)
        .then(response => {
            dispatch({ type: Types.FETCH_COMMENTS, payload: response.data.result })
        })
        .catch(err => {
            if (err) {
                dispatch(loadingProduct(false))
            }
        })
}

export const createComment = (comment, cb) => dispatch => {
    dispatch(loadingProduct())
    instance
        .post(`/comment/save_comment`, comment, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: Types.CREATE_COMMENT, payload: comment })
            cb()
        })
        .catch(err => console.log(err.response))
}

export const createRating = data => dispatch => {
    dispatch(loadingProduct())
    return instance
        .post(`/rating/save_rating`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({
                type: Types.CREATE_RATING,
                payload: response.data.result,
                ratingMessage: "Terima kasih udah kasih rating barang ini"
            })
        })
        .catch(err => console.error(err.response))
}

export const productIsRated = (productId, userId) => dispatch => {
    instance
        .get(`/rating/is_rated/${productId}/${userId}`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            const payload = response.data.result.is_rated
            const ratingId = response.data.result.rating_id
            dispatch({ type: Types.PRODUCT_IS_RATED, payload, ratingId })
        })
        .catch(err => console.error(err.response))
}

export const updateRating = data => dispatch => {
    dispatch(loadingProduct())
    return instance
        .put(`/rating/update_rating`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: Types.UPDATE_RATING, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const fetchRestockProducts = () => dispatch => {
    dispatch(loadingProduct())
    instance
        .get("/product/list/null/null/null/null/null/1")
        .then(response => {
            dispatch({ type: Types.FETCH_RESTOCK_PRODUCTS, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const fetch10RestockProducts = () => dispatch => {
    dispatch(loadingProduct())
    instance
        .get("/product/list/1/null/null/null/null/1")
        .then(response => {
            dispatch({ type: Types.FETCH_10_RESTOCK_PRODUCTS, payload: response.data.result })
        })
        .catch(err => {
            if (err.code === "ECONNABORTED") {
                dispatch(productError("timeout"))
            }

            console.error(err.response)
        })
}

export const fetchBannerPromoProducts = () => dispatch => {
    dispatch(loadingProduct())
    instance
        .get(`banner/list`)
        .then(response => {
            dispatch({ type: Types.FETCH_BANNER_PROMO_PRODUCTS, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const searchProducts = keyword => dispatch => {
    dispatch(loadingProduct())
    return instance
        .get(`/product/searching?query=${keyword}`)
        .then(response => {
            localStorage.setItem("keyword", keyword)
            dispatch({ type: Types.SEARCH_PRODUCTS, payload: response.data.result })
        })
        .catch(err => {
            console.error(err.response)
            dispatch(productError(err.response.data.result && err.response.data.result.search[0]))
        })
}
