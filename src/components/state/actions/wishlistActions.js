import { FETCH_WISHLISTS, CREATE_WISHLIST, DELETE_WISHLIST, LOADING_INDICATOR } from "../constants"
import { instance } from "../common"

export const loadingIndicator = (condition = true) => ({ type: LOADING_INDICATOR, condition })

export const fetchWishlists = () => dispatch => {
    dispatch(loadingIndicator())
    return instance
        .get(`/wishlist/list`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: FETCH_WISHLISTS, payload: response.data.result })
        })
        .catch(err => {
            if (err) {
                console.log(err.response)
                dispatch(loadingIndicator(false))
            }
        })
}

export const createWishlist = data => dispatch => {
    dispatch(loadingIndicator())
    instance
        .post(`/wishlist/save_wishlist`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: CREATE_WISHLIST, payload: data })
        })
        .then(() => dispatch(fetchWishlists()))
        .catch(err => console.log(err.response))
}

export const deleteWishlist = wishlistId => dispatch => {
    dispatch(loadingIndicator())
    instance
        .delete(`/wishlist/delete_wishlist`, {
            params: { id: wishlistId },
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(response => {
            dispatch({ type: DELETE_WISHLIST, payload: wishlistId })
        })
        .then(() => dispatch(fetchWishlists()))
        .catch(err => console.log(err))
}
