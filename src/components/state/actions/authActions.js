import {
    SIGNIN_USER,
    SIGNUP_USER,
    SIGNOUT_USER,
    LOADING_INDICATOR,
    AUTH_ERROR,
    FORGOT_PASSWORD,
    SAVE_NEW_PASSWORD,
    FETCH_REFRESH_TOKEN
} from "../constants"
import { instance } from "../common"

export const loadingIndicator = (condition = true) => ({ type: LOADING_INDICATOR, condition })

export const authError = (message, errorType) => ({ type: AUTH_ERROR, payload: message, errorType })

export const fetchRefreshToken = refreshToken => dispatch => {
    instance
        .post(`/customer/refresh_token`, refreshToken)
        .then(response => {
            console.log(response.data)
            dispatch({ type: FETCH_REFRESH_TOKEN, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const signinUser = data => dispatch => {
    dispatch(loadingIndicator())
    return instance
        .post(`/login`, data)
        .then(response => {
            const { access_token, refresh_token } = response.data.result

            dispatch({ type: SIGNIN_USER })
            localStorage.setItem("token", access_token)
            localStorage.setItem("refreshToken", refresh_token)
        })
        .catch(err => {
            // if (err) {
            dispatch(loadingIndicator(false))
            // }
            const error = err.response.data.result
            if (err) {
                for (let idx in error) {
                    console.log(error[idx])
                    dispatch(authError(error[idx], "login"))
                }
            }
        })
}

export const signinUserViaFacebook = data => dispatch => {
    dispatch(loadingIndicator())
    return instance.post("/login", data).then(response => {})
}

export const signupUser = (data, cb) => dispatch => {
    dispatch(loadingIndicator())
    instance
        .post(`/register`, data)
        .then(response => {
            dispatch({ type: SIGNUP_USER })
            cb()
        })
        .catch(err => {
            if (err) {
                dispatch(loadingIndicator(false))
            }
            console.log(Object.values(err.response.data.result)[0])
            const error = err && Object.values(err.response.data.result)[0]
            dispatch(authError(error[0], "register"))
        })
}

export const signoutUser = () => dispatch => {
    dispatch(loadingIndicator())
    instance
        .get(`/customer/logout`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                Accept: "application/json"
            }
        })
        .then(response => {
            dispatch({ type: SIGNOUT_USER })
            localStorage.clear()
            window.location.replace("/logout")
        })
        .then(() => window.location.replace("/logout"))
        .catch(err => {
            if (err) {
                dispatch(loadingIndicator(false))
                localStorage.clear()
                window.location.replace("/logout")
            }
        })
}

export const forgotPassword = (email, cb) => dispatch => {
    dispatch(loadingIndicator())
    instance
        .post(`/forgot_password`, email)
        .then(response => {
            dispatch({ type: FORGOT_PASSWORD })
            cb()
        })
        .catch(err => {
            console.log(err.response)
            dispatch(authError("Email yang kamu masukin nggak ada di database kami.", "forgot"))
        })
}

export const saveNewPassword = data => dispatch => {
    dispatch(loadingIndicator())
    return instance
        .post(`/new_password`, data)
        .then(response => {
            dispatch({ type: SAVE_NEW_PASSWORD, payload: response.data })
        })
        .catch(err => {
            dispatch(authError(err.response.data.result, "newPassword"))
        })
}
