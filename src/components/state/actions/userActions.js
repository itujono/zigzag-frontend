import {
    FETCH_USER,
    FETCH_USER_DATA,
    UPDATE_PROFILE,
    LOADING_INDICATOR,
    USER_ERROR,
    CHANGE_PASSWORD,
    FETCH_CUSTOMER_SERVICES,
    UPGRADE_ACCOUNT,
    UPGRADE_CONFIRMATION,
    FETCH_LIST_UPGRADE_ACCOUNT,
    UPGRADE_ERROR,
    CHANGE_PROFILE_PICTURE
} from "../constants"
import { signoutUser, fetchRefreshToken } from "./authActions"
import { baseURL, instance } from "../common"

export const loadingIndicator = (condition = true) => ({ type: LOADING_INDICATOR, condition })

export const userError = message => ({ type: USER_ERROR, payload: message })

export const upgradeError = message => ({ type: UPGRADE_ERROR, payload: message })

export const fetchUser = () => dispatch => {
    return instance
        .get(`/customer/details`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: FETCH_USER, payload: response.data.result })
        })
        .catch(err => {
            if (err && err.response.status === 401) {
                // dispatch(fetchRefreshToken(refreshToken))
                dispatch(signoutUser())
                // localStorage.removeItem('token')
                // localStorage.removeItem('token')
            }
        })
}

export const unfetchUser = () => dispatch => {
    instance
        .get(`/customer/details`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: FETCH_USER, payload: null })
        })
        .catch(err => {
            if (err) {
                console.log(err.response)
            }
        })
}

export const changeProfilePicture = file => dispatch => {
    dispatch(loadingIndicator())
    return instance
        .post(`/customer/update_profile_photo`, file, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            console.log(response.data)
            dispatch({ type: CHANGE_PROFILE_PICTURE, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const updateProfile = data => dispatch => {
    dispatch(loadingIndicator())
    return instance
        .post(`/customer/update`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: UPDATE_PROFILE, payload: response.data.result })
        })
        .then(() => dispatch(fetchUser()))
        .catch(error => console.log(error.response))
}

export const changePassword = data => dispatch => {
    dispatch(loadingIndicator())
    instance
        .put(`/customer/change_password`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: CHANGE_PASSWORD, payload: data })
        })
        .then(() => {
            dispatch(signoutUser())
            localStorage.removeItem("token")
            localStorage.removeItem("refreshToken")
        })
        .catch(err => {
            dispatch(userError("Hmm kayaknya ada yang salah ni. Coba cek lagi, ya."))
        })
}

export const fetchCustomerServices = () => dispatch => {
    instance
        .get(`/customer_service`)
        .then(response => {
            dispatch({ type: FETCH_CUSTOMER_SERVICES, payload: response.data.result })
        })
        .catch(err => console.log(err.response))
}

export const upgradeAccount = () => dispatch => {
    instance
        .get(`/customer/upgrade_account`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: UPGRADE_ACCOUNT, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const fetchListUpgradeAccount = () => dispatch => {
    instance
        .get(`/customer/list_upgrade_account`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: FETCH_LIST_UPGRADE_ACCOUNT, payload: response.data.result })
        })
        .catch(err => {
            dispatch(upgradeError(err.response))
        })
}

export const upgradeConfirmation = (data, cb) => dispatch => {
    instance
        .post(`/customer/confirm_upgrade_account`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: UPGRADE_CONFIRMATION, payload: response.data.result })
            cb()
        })
        .catch(err => {
            const result = err.response.data.result
            console.log(result)
            dispatch(upgradeError(result))
        })
}

export const fetchUserData = data => ({
    type: FETCH_USER_DATA,
    payload: data
})
