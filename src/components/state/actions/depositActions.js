import { baseURL, instance } from "../common"
import {
    SAVE_DEPOSIT,
    FETCH_DEPOSITS,
    LOADING_DEPOSIT,
    CONFIRM_DEPOSIT,
    DEPOSIT_ERROR,
    UPDATE_DEPOSIT,
    REMOVE_ENTIRE_ERROR
} from "../constants"
import { signoutUser } from "./authActions"

export const loadingDeposit = () => ({ type: LOADING_DEPOSIT })

export const depositError = message => ({ type: DEPOSIT_ERROR, payload: message })

export const removeEntireError = () => ({ type: REMOVE_ENTIRE_ERROR })

export const fetchDeposits = () => dispatch => {
    dispatch(loadingDeposit())
    instance
        .get(`/customer/list_deposit`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({
                type: FETCH_DEPOSITS,
                payload: response.data.result,
                total: response.data.result
                    .filter(depo => depo.status === 3)
                    .map(depo => depo.total)
                    .reduce((acc, curr) => acc + curr, 0)
            })
        })
        .catch(err => {
            console.log(err.response)
            // if (err && err.response.status === 401) {
            //     dispatch(signoutUser())
            //     localStorage.removeItem('refreshToken')
            //     localStorage.removeItem('refreshrefreshToken')
            // }
        })
}

export const saveDeposit = (data, cb) => dispatch => {
    dispatch(loadingDeposit())
    instance
        .post(`/customer/save_deposit`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: SAVE_DEPOSIT, payload: data })
            cb()
        })
        .catch(err => console.log(err))
}

export const updateDeposit = amount => dispatch => {
    instance
        .put(`/customer/update_deposit`, amount, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: UPDATE_DEPOSIT, payload: response.data.result })
        })
        .catch(err => console.error(err))
}

export const confirmDeposit = (data, cb) => dispatch => {
    dispatch(loadingDeposit())
    instance
        .post(`/customer/upload_deposit`, data, {
            headers: {
                "Content-Type": "multipart/form-data",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(response => {
            dispatch({ type: CONFIRM_DEPOSIT, payload: data })
            cb()
        })
        .catch(error => {
            if (error) {
                dispatch(depositError(error.response.data.result))
            }
        })
}
