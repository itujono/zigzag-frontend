import { instance } from "../common"
import {
    FETCH_EXPEDITIONS,
    SUBMIT_ORDER,
    FETCH_ORDER_HISTORY,
    GET_COST_JNE,
    GET_COST_TIKI,
    GET_COST_POS,
    LOADING_ORDER,
    SAVE_TEMP_DATA,
    FETCH_TEMP_DATA,
    CONFIRM_ORDER,
    ORDER_ERROR,
    CANCEL_ORDER,
    FETCH_COSTS,
    SAVE_ORDER_CODE,
    FETCH_BANK_ACCOUNT,
    FETCH_AIRWAY_BILLS,
    FETCH_ORDER_CODE,
    SAVE_TEMP_EXPEDITION
} from "../constants"

export const loadingOrder = (condition = true) => ({ type: LOADING_ORDER, payload: condition })

export const orderError = message => ({ type: ORDER_ERROR, payload: message })

export const fetchExpeditions = () => dispatch => {
    dispatch(loadingOrder())
    instance
        .get(`/ekspedisi`)
        .then(response => {
            dispatch({ type: FETCH_EXPEDITIONS, payload: response.data.result })
        })
        .catch(err => console.error(`Error kali ini dipersembahkan oleh ${err.response}`))
}

export const getCostJne = (origin, dest, weight, courier) => dispatch => {
    dispatch(loadingOrder())
    instance
        .get(`/cost/${origin}/${dest}/${weight}/${courier}`)
        .then(response => {
            dispatch({ type: GET_COST_JNE, payload: response.data.result.results[0].costs })
        })
        .catch(err => console.log(err.response))
}

export const getCostTiki = (origin, dest, weight, courier) => dispatch => {
    instance
        .get(`/cost/${origin}/${dest}/${weight}/${courier}`)
        .then(response => {
            dispatch({ type: GET_COST_TIKI, payload: response.data.result.results[0].costs })
        })
        .catch(err => console.log(err.response))
}

export const getCostPos = (origin, dest, weight, courier) => dispatch => {
    instance
        .get(`/cost/${origin}/${dest}/${weight}/${courier}`)
        .then(response => {
            dispatch({ type: GET_COST_POS, payload: response.data.result.results[0].costs })
        })
        .catch(err => console.log(err.response))
}

export const fetchTiki = (origin, dest, weight, courier) => {
    instance.get(`/cost/${origin}/${dest}/${weight}/${courier}`)
}

export const fetchJne = (origin, dest, weight, courier) => {
    instance.get(`/cost/${origin}/${dest}/${weight}/${courier}`)
}

export const fetchPos = (origin, dest, weight, courier) => {
    instance.get(`/cost/${origin}/${dest}/${weight}/${courier}`)
}

export const fetchCosts = ({ origin, destination, weight }) => dispatch => {
    dispatch(loadingOrder())
    instance
        .get(`/cost/${origin}/${destination}/${weight}/jne:sicepat:jnt`)
        .then(response => {
            dispatch({
                type: FETCH_COSTS,
                payload: response.data.result.results,
                details: {
                    weight: response.data.result.query.weight,
                    destination: response.data.result.destination_details
                }
            })
        })
        .catch(err => console.error(err.response))
}

export const submitOrder = (data, cb) => dispatch => {
    dispatch(loadingOrder())
    instance
        .post(`/order/save_order`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: SUBMIT_ORDER, payload: response.data.results })
            cb()
        })
        .catch(err => console.error(err.response))
}

export const fetchOrderCode = () => dispatch => {
    dispatch(loadingOrder())
    instance
        .get(`/order/fetch_order_code`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: FETCH_ORDER_CODE, payload: response.data.result })
        })
        .catch(err => {
            console.error(err.response)
        })
}

export const fetchOrderHistory = (page = 1) => dispatch => {
    dispatch(loadingOrder())
    instance
        .get(`/order/history_order?page=${page}`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        // .get(`/order/history_order/${id}`, {
        //     headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        // })
        .then(response => {
            dispatch({ type: FETCH_ORDER_HISTORY, payload: response.data.result })
        })
        .catch(err => {
            if (err.code === "ECONNABORTED") {
                dispatch(orderError("timeout"))
            }
            console.error(err.response)
        })
}

export const confirmOrder = (data, cb) => dispatch => {
    dispatch(loadingOrder())
    instance
        .post(`/order/confirmation_order`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: CONFIRM_ORDER, payload: data })
            cb()
        })
        .catch(err => {
            if (err) {
                dispatch(loadingOrder(false))
                if (err.code === "ECONNABORTED") {
                    dispatch(orderError("timeout"))
                }

                console.error(err.response.data.result)
                dispatch(orderError(err.response.data.result))
            }
        })
}

export const cancelOrder = data => dispatch => {
    dispatch(loadingOrder())
    instance
        .post(`/order/cancel_order`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: CANCEL_ORDER, payload: response.data.status })
        })
        .then(() => dispatch(fetchOrderHistory()))
        .catch(err => console.error(err.response))
}

export const saveTempExpedition = data => dispatch => {
    dispatch(loadingOrder())
    return instance
        .post(`/order/save_ekspedition`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            const data = response.data.data
            localStorage.setItem("expedition", JSON.stringify(data))
            dispatch({ type: SAVE_TEMP_EXPEDITION, payload: data })
        })
        .catch(err => console.error(err.response))
}

export const saveTempData = data => {
    if (data) localStorage.setItem("temp", JSON.stringify(data))
    return {
        type: SAVE_TEMP_DATA,
        payload: data
    }
}

export const fetchTempData = data => {
    return {
        type: FETCH_TEMP_DATA,
        payload: data
    }
}

export const saveOrderCode = data => ({
    type: SAVE_ORDER_CODE,
    payload: data
})

export const fetchBankAccount = () => dispatch => {
    dispatch(loadingOrder())
    instance
        .get(`/bank_account_list`)
        .then(response => {
            dispatch({ type: FETCH_BANK_ACCOUNT, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}

export const fetchAirwayBills = (number, courier) => dispatch => {
    dispatch(loadingOrder())
    instance
        .get(`/resi/${number}/${courier}`)
        .then(response => {
            console.log(response.data)
            dispatch({ type: FETCH_AIRWAY_BILLS, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}
