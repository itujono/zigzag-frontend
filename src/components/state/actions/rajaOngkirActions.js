import { FETCH_CITIES, FETCH_PROVINCES, FETCH_SUBDISTRICTS } from "../constants"
import { instance } from "../common"

export const fetchProvinces = () => dispatch => {
    instance
        .get(`/province`)
        .then(response => {
            dispatch({ type: FETCH_PROVINCES, payload: response.data.result })
        })
        .catch(err => console.log(err.response))
}

export const fetchCities = () => dispatch => {
    instance
        .get(`/get_city`)
        .then(response => {
            dispatch({ type: FETCH_CITIES, payload: response.data.result })
        })
        .catch(err => console.log(err.response))
}

export const fetchSubdistricts = cityId => dispatch => {
    instance
        .get(`/get_subdistrict/${cityId}`)
        .then(response => {
            dispatch({ type: FETCH_SUBDISTRICTS, payload: response.data.result })
        })
        .catch(err => console.error(err.response))
}
