import {
    FETCH_CARTS,
    CREATE_CART,
    DELETE_CART,
    LOADING_CARTS,
    UPDATE_CART_ITEM,
    GET_TOTAL_CARTS,
    GET_ACTUAL_PRICE,
    REMOVE_ENTIRE_CARTS,
    ERROR_CARTS
} from "../constants"
import { instance } from "../common"

export const loadingCarts = (condition = true) => ({ type: LOADING_CARTS, condition })

const errorCarts = message => ({ type: ERROR_CARTS, payload: message })

export const fetchCarts = () => dispatch => {
    dispatch(loadingCarts())
    instance
        .get(`/cart/list`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: FETCH_CARTS, payload: response.data.result })
        })
        .catch(error => {
            if (error) {
                if (error.code === "ECONNABORTED") {
                    dispatch(errorCarts("timeout"))
                }
                dispatch(loadingCarts(false))
                // localStorage.removeItem('token')
                // localStorage.removeItem('refreshToken')
                // window.location.replace('/login')
            }
        })
}

export const createCart = cart => dispatch => {
    dispatch(loadingCarts())
    return instance
        .post(`/cart/save_cart`, cart, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(() => {
            dispatch({ type: CREATE_CART, payload: cart })
        })
        .then(() => dispatch(fetchCarts()))
        .catch(err => console.log(`Error di Create Cart kali ini dipersembahkan oleh ${err.response}`))
}

export const updateCartItem = data => dispatch => {
    dispatch(loadingCarts())
    instance
        .put(`/cart/update_qty_cart`, data, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({ type: UPDATE_CART_ITEM, payload: data })
            dispatch(getTotalCarts())
            dispatch(fetchCarts())
        })
        .catch(err => console.warn(`Error Update Cart Item kali ini dipersembahkan oleh ${err}`))
}

export const getTotalCarts = () => dispatch => {
    instance
        .get(`/cart/list`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            dispatch({
                type: GET_TOTAL_CARTS,
                payload: response.data.result.map(cart => cart.total_price).reduce((acc, curr) => acc + curr, 0),
                weight: response.data.result.map(cart => cart.weight * cart.qty).reduce((acc, curr) => acc + curr, 0),
                totalPerItem: response.data.result.map(cart => cart.id)
            })
        })
        .catch(error => console.log(error))
}

export const getActualPrice = user => dispatch => {
    instance
        .get(`/cart/list`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => {
            const { result } = response.data
            const priceTemp = result.map(cart => cart.price)

            dispatch({
                type: GET_ACTUAL_PRICE,
                payload: priceTemp.filter(price => price.price_type === "member")
            })
        })
        .catch(err => console.log(err.response))
}

export const removeEntireCarts = () => ({ type: REMOVE_ENTIRE_CARTS })

export const deleteCart = cartId => dispatch => {
    dispatch(loadingCarts())
    instance
        .delete(`/cart/delete_cart`, {
            params: { id: cartId },
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then(response => dispatch({ type: DELETE_CART, payload: cartId }))
        .catch(error => console.log(`Error di Delete Cart kali ini dipersembahkan oleh ${error}`))
}
