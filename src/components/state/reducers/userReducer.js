import * as Types from "../constants"

const initialState = {}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.LOADING_INDICATOR:
            if (action.condition === false) {
                return { ...state, loading: false }
            } else {
                return { ...state, loading: true }
            }
        case Types.FETCH_USER:
            return { ...state, user: action.payload, loading: false }
        case Types.FETCH_USER_DATA:
            return { ...state, userData: action.payload, loading: false }
        case Types.UPDATE_PROFILE:
            return { ...state, user: action.payload, loading: false }
        case Types.CHANGE_PROFILE_PICTURE:
            return { ...state, picture: action.payload, loading: false }
        case Types.CHANGE_PASSWORD:
            return {
                ...state,
                loading: false,
                password: action.payload,
                loading: false
            }
        case Types.FETCH_CUSTOMER_SERVICES:
            return { ...state, cs: action.payload, loading: false }
        case Types.UPGRADE_ACCOUNT:
            return { ...state, request: action.payload, loading: false }
        case Types.UPGRADE_CONFIRMATION:
            return { ...state, status: action.payload, loading: false }
        case Types.FETCH_LIST_UPGRADE_ACCOUNT:
            return { ...state, list: action.payload, loading: false }
        case Types.USER_ERROR:
            return { ...state, error: action.payload, loading: false }
        case Types.UPGRADE_ERROR:
            return { ...state, error: action.payload, loading: false }
        default:
            return state
    }
}
