import {
    FETCH_EXPEDITIONS,
    SUBMIT_ORDER,
    FETCH_ORDER_HISTORY,
    GET_COST_JNE,
    GET_COST_TIKI,
    GET_COST_POS,
    LOADING_ORDER,
    SAVE_TEMP_DATA,
    FETCH_TEMP_DATA,
    CONFIRM_ORDER,
    ORDER_ERROR,
    CANCEL_ORDER,
    FETCH_COSTS,
    SAVE_ORDER_CODE,
    FETCH_BANK_ACCOUNT,
    FETCH_AIRWAY_BILLS,
    FETCH_ORDER_CODE,
    SAVE_TEMP_EXPEDITION
} from "../constants"

const initialState = {
    expeditions: [],
    temp: {}
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOADING_ORDER:
            return { ...state, loading: action.payload === true ? true : false }
        case ORDER_ERROR:
            return { ...state, error: action.payload, loading: false }
        case FETCH_EXPEDITIONS:
            return { ...state, expeditions: action.payload, loading: false }
        case SUBMIT_ORDER:
            return { ...state, response: action.payload, loading: false }
        case FETCH_ORDER_HISTORY:
            return { ...state, orderHistory: action.payload, loading: false }
        case FETCH_ORDER_CODE:
            return { ...state, orderCode: action.payload, loading: false }
        case SAVE_TEMP_DATA:
            return { ...state, temp: action.payload, loading: false }
        case CONFIRM_ORDER:
            return { ...state, loading: false }
        case CANCEL_ORDER:
            return { ...state, status: action.payload, loading: false }
        case FETCH_TEMP_DATA:
            return { ...state, temp: action.payload }
        case GET_COST_JNE:
            return { ...state, jne: action.payload, loading: false }
        case GET_COST_TIKI:
            return { ...state, tiki: action.payload }
        case GET_COST_POS:
            return { ...state, pos: action.payload }
        case FETCH_COSTS:
            return { ...state, costs: action.payload, details: action.details, loading: false }
        case SAVE_ORDER_CODE:
            return { ...state, orderCode: action.payload, loading: false }
        case SAVE_TEMP_EXPEDITION:
            return { ...state, expedition: action.payload, loading: false }
        case FETCH_BANK_ACCOUNT:
            return { ...state, bankAccount: action.payload, loading: false }
        case FETCH_AIRWAY_BILLS:
            return { ...state, awb: action.payload, loading: false }
        default:
            return state
    }
}
