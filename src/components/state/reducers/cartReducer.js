import {
    DELETE_CART,
    CREATE_CART,
    FETCH_CARTS,
    LOADING_CARTS,
    UPDATE_CART_ITEM,
    GET_TOTAL_CARTS,
    GET_ACTUAL_PRICE,
    REMOVE_ENTIRE_CARTS,
    ERROR_CARTS
} from "../constants"

const initialState = {
    carts: [],
    total: 0,
    totalPerItem: 0,
    cartItem: {},
    qty: 0
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOADING_CARTS:
            if (action.condition === false) {
                return { ...state, loading: false }
            } else {
                return { ...state, loading: true }
            }
        case FETCH_CARTS:
            return { ...state, carts: action.payload, loading: false }
        case CREATE_CART:
            return { ...state, carts: [...state.carts, action.payload], loading: false }
        case DELETE_CART:
            return { ...state, carts: state.carts.filter(cart => cart.id !== action.payload), loading: false }
        case GET_ACTUAL_PRICE:
            return { ...state, actualPrice: action.payload }
        case REMOVE_ENTIRE_CARTS:
            return { ...state, carts: null }
        case GET_TOTAL_CARTS:
            return {
                ...state,
                total: action.payload,
                weight: action.weight,
                loading: false
            }
        case UPDATE_CART_ITEM:
            return {
                ...state,
                loading: false
            }
        case ERROR_CARTS:
            return { ...state, error: action.payload, loading: false }
        default:
            return state
    }
}
