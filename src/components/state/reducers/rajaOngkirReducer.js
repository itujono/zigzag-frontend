import { FETCH_CITIES, FETCH_PROVINCES, GET_COSTS, FETCH_SUBDISTRICTS } from "../constants"

const initialState = { subdistricts: [] }

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CITIES:
            return { ...state, cities: action.payload }
        case FETCH_PROVINCES:
            return { ...state, provinces: action.payload }
        case GET_COSTS:
            return { ...state, costs: action.payload }
        case FETCH_SUBDISTRICTS:
            return { ...state, subdistricts: action.payload }
        default:
            return state
    }
}
