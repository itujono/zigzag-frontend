import * as Types from "../constants"

const initialState = {
    comments: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.LOADING_PRODUCT:
            if (action.condition === false) {
                return { ...state, loading: false }
            } else {
                return { ...state, loading: true }
            }
        case Types.PRODUCT_ERROR:
            return { ...state, error: action.payload, loading: false }
        case Types.REMOVE_PRODUCT_ERROR:
            if (action.payload) {
                return { ...state, error: null, loading: false }
            }
        case Types.FETCH_PRODUCTS:
            return { ...state, products: action.payload, loading: false }
        case Types.FETCH_PRODUCT_ITEM:
            return { ...state, product: action.payload, loading: false }
        case Types.FETCH_CATEGORIES:
            return { ...state, categories: action.payload, loading: false }
        case Types.FETCH_PRODUCTS_BY_CATEGORY:
            return { ...state, productByCategory: action.payload, loading: false }
        case Types.FETCH_COMMENTS:
            return { ...state, comments: action.payload, loading: false }
        case Types.CREATE_COMMENT:
            return { ...state, comments: [...state.comments, action.payload], loading: false }
        case Types.CREATE_RATING:
            return { ...state, rating: action.payload, ratingMessage: action.ratingMessage, loading: false }
        case Types.FETCH_RESTOCK_PRODUCTS:
            return { ...state, restock: action.payload, loading: false }
        case Types.FETCH_10_RESTOCK_PRODUCTS:
            return { ...state, restock: action.payload, loading: false }
        case Types.FETCH_LATEST_PRODUCTS:
            return { ...state, latests: action.payload, loading: false }
        case Types.FETCH_10_LATEST_PRODUCTS:
            return { ...state, latests: action.payload, loading: false }
        case Types.FETCH_BANNER_PROMO_PRODUCTS:
            return { ...state, bannerProducts: action.payload, loading: false }
        case Types.PRODUCT_IS_RATED:
            return { ...state, isRated: action.payload, ratingId: action.ratingId }
        case Types.SEARCH_PRODUCTS:
            return { ...state, searchResults: action.payload, loading: false }
        default:
            return state
    }
}
