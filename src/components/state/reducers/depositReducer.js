import * as Types from "../constants"

const initialState = {
    deposits: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.LOADING_DEPOSIT:
            return { ...state, loading: true }
        case Types.REMOVE_ENTIRE_ERROR:
            return { ...state, error: null }
        case Types.FETCH_DEPOSITS:
            return { ...state, deposits: action.payload, loading: false, total: action.total }
        case Types.SAVE_DEPOSIT:
            return { ...state, deposits: [...state.deposits, action.payload], loading: false }
        case Types.CONFIRM_DEPOSIT:
            return { ...state, loading: false }
        case Types.UPDATE_DEPOSIT:
            return { ...state, total: action.payload, loading: false }
        case Types.DEPOSIT_ERROR:
            return { ...state, error: action.payload, loading: false }
        default:
            return state
    }
}
