import { combineReducers } from "redux"
import { FetchZipCodesReducer } from "../reducers/FetchZipCodesReducer"
import authReducer from "../reducers/authReducer"
import rajaOngkirReducer from "../reducers/rajaOngkirReducer"
import userReducer from "../reducers/userReducer"
import productsReducer from "../reducers/productsReducer"
import wishlistReducer from "./wishlistReducer"
import cartReducer from "./cartReducer"
import depositReducer from "./depositReducer"
import orderReducer from "./orderReducer"
import { reducer as FormReducer } from "redux-form"
import { SIGNOUT_USER } from "../constants"

const AppReducer = combineReducers({
    zipCodes: FetchZipCodesReducer,
    auth: authReducer,
    rajaOngkir: rajaOngkirReducer,
    products: productsReducer,
    user: userReducer,
    wishlist: wishlistReducer,
    cart: cartReducer,
    deposit: depositReducer,
    order: orderReducer,
    form: FormReducer
})

export const rootReducer = (state, action) => {
    if (action.type === SIGNOUT_USER) {
        state = undefined
    }

    return AppReducer(state, action)
}
