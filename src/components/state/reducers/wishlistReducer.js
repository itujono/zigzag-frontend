import { FETCH_WISHLISTS, CREATE_WISHLIST, DELETE_WISHLIST, LOADING_INDICATOR } from "../constants"


const initialState = {
    wishlists: [],
    added: false
}


export default (state = initialState, action) => {

    switch (action.type) {
        case LOADING_INDICATOR:
            if (action.condition === false) {
                return { ...state, loading: false }
            } else {
                return { ...state, loading: true }
            }
        case FETCH_WISHLISTS:
            return  {...state, wishlists: action.payload, loading: false }
        case CREATE_WISHLIST:
            return { ...state, wishlists: [ ...state.wishlists, action.payload ], loading: false }
        case DELETE_WISHLIST:
            return { ...state, wishlists: state.wishlists.filter(product => product.wishlist_id !== action.payload), loading: false }
        default:
            return state
    }

}