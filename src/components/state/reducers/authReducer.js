import * as Types from "../constants"

const initialState = {
    auth: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.LOADING_INDICATOR:
            if (action.condition === false) {
                return { ...state, loading: false }
            } else {
                return { ...state, loading: true }
            }
        case Types.SIGNIN_USER:
            return { ...state, authenticated: true, loading: false }
        case Types.SIGNIN_USER_VIA_FACEBOOK:
            return { ...state, loading: false, authenticated: true }
        case Types.SIGNUP_USER:
            return { ...state, authenticated: true, loading: false }
        case Types.SIGNOUT_USER:
            return { ...state, authenticated: false, user: null, loading: false }
        case Types.FORGOT_PASSWORD:
            return { ...state, loading: false }
        case Types.SAVE_NEW_PASSWORD:
            return { ...state, loading: false }
        case Types.AUTH_ERROR:
            return {
                ...state,
                authenticated: false,
                loading: false,
                message: action.payload,
                errorType: action.errorType
            }
        default:
            return state
    }
}
