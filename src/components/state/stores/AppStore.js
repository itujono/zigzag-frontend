import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk"
// import promiseMiddleware from "redux-promise-middleware"
import { rootReducer } from "../reducers/AppReducer"

const url = window.location.pathname.split("/")

if (url[1] === "api" && url[2] === "confirm_forgot_password") {
    window.location.replace("/new-password")
}

export const createAppStore = () => {
    return createStore(rootReducer, applyMiddleware(thunk))
}
