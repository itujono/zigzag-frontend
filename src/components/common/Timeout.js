import React from "react"
import { Modal, Header, Button } from "semantic-ui-react"

class Timeout extends React.Component {
    state = { open: false }

    handleCloseModal = () => this.setState({ open: false })

    render() {
        const { open } = this.props

        return (
            <Modal className="timeout-popup" dimmer="inverted" open={open} size="tiny">
                <Modal.Content>
                    <Modal.Description>
                        <Header content="Ada koneksi?" />
                        <p>Kayaknya koneksi internet kamu lagi nggak bagus. Coba refresh sekali lagi ya.</p>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        )
    }
}

export default Timeout
