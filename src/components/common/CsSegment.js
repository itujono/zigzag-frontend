import React from "react"
import { Segment, Header, Divider, List, Image } from "semantic-ui-react"


const CsSegment = ({ csState, onExpandCs, cs, text }) => {
    return (
        <Segment color="purple" className={csState ? "cs-segment expanded" : "cs-segment"}>
            <Header
                as="h5"
                onClick={onExpandCs}
                content={text && text.header || "Ada yang bikin bingung?"}
                subheader={text && text.subheader || "Langsung chat aja CS pribadi kamu"}
            />
            <Divider />
            <Header as="h5">
                <Image circular src="https://react.semantic-ui.com/images/avatar/small/rachel.png" />
                <Header.Content>
                    {cs && cs.name}
                    <Header.Subheader>Kenalin, saya CS pribadi kamu</Header.Subheader>
                </Header.Content>
            </Header>
            <List relaxed>
                <List.Item>
                    <List.Header as="h6" content="Whatsapp" />
                    <List.Description>
                        { cs && cs.wa }
                    </List.Description>
                </List.Item>
                <List.Item>
                    <List.Header as="h6" content="Line" />
                    <List.Description>
                        { cs && cs.line }
                    </List.Description>
                </List.Item>
            </List>
        </Segment>
    )
}

export default CsSegment