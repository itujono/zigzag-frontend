import React from "react"
import { Segment, Header } from "semantic-ui-react"

class Boundary extends React.Component {
    state = { error: null }

    componentDidCatch(error, info) {
        this.setState({ error })
        console.log(error, info)
    }

    render() {
        if (this.state.error)
            return (
                <Segment textAlign="center">
                    <Header content="Oops!" subheader="Ada yang salah nih! :(" />
                </Segment>
            )

        return this.props.children
    }
}

export default Boundary
