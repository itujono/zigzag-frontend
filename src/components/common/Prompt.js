import React from "react"
import { Modal, Button } from "semantic-ui-react"

const Prompt = ({ open, onClose, header, confirm, loading, children, yesText, noText, size, closeIcon }) => {
    return (
        <Modal open={open} size={size || "tiny"} onClose={onClose} closeIcon={closeIcon}>
            <Modal.Header>{header}</Modal.Header>
            <Modal.Content>{children}</Modal.Content>
            <Modal.Actions>
                <Button basic onClick={onClose}>
                    {noText || "Batal"}
                </Button>
                {yesText && (
                    <Button
                        loading={loading}
                        disabled={loading}
                        className="btn-zigzag"
                        icon="checkmark"
                        labelPosition="right"
                        content={yesText}
                        onClick={() => confirm(true)}
                    />
                )}
            </Modal.Actions>
        </Modal>
    )
}

export default Prompt
