import React from "react"
import { Container, Image } from "semantic-ui-react";
import logo from "../../images/zigzag-logo.png";


const Logo = ({ size }) => {
    return (
        <Container textAlign="center" className="logo-container">
            <Image src={logo} size={size} centered />
        </Container>
    )
}

export default Logo