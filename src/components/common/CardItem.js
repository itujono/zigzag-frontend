import React from "react";
import { Card } from "semantic-ui-react";




const CardItem = ({children, className}) => {
	return (
		<Card className={className}>
			{children}
		</Card>
	)
}




export default CardItem;
