import React from "react";
import { Dimmer, Loader, Segment } from "semantic-ui-react";



const Loading = ({ content, inverted }) => (
    <Segment>
        <Dimmer inverted={inverted} active page>
            <Loader content={content || "Mohon bersabar..."} />
        </Dimmer>
    </Segment>
)

export default Loading
