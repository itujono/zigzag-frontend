import React from "react"
import { Grid, Header, Card, Icon, Responsive, Segment } from "semantic-ui-react"
import ProductItem from "./ProductItem"
import { connect } from "react-redux"
import { fetchUser } from "../state/actions/userActions"
import { Link } from "react-router-dom"
import Emoji from "../common/Emoji"
import { mobile, tablet } from "."
import NoData from "./NoData"

class ProductList extends React.Component {
    render() {
        const { loading, user, restock, latests } = this.props

        return (
            <Grid className={`product-list ${!restock && !latests ? "loading" : ""}`}>
                <Grid.Row>
                    <Grid.Column className="latests">
                        <Header as="h2" className="section-header">
                            <Header.Content>
                                Produk Terbaru&nbsp; <Emoji label="gown" symbol="👗" />
                                <Header.Subheader>
                                    Baru masuk! Baru masuk! Baru masuk! &nbsp;{" "}
                                    <Link to="/p/produk-terbaru" style={{ display: mobile ? "block" : "initial" }}>
                                        Selengkapnya <Icon size="small" name="chevron right" />
                                    </Link>
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                        {latests ? (
                            <Card.Group itemsPerRow={mobile ? 2 : tablet ? 4 : 5}>
                                {latests && latests.length > 0 ? (
                                    latests
                                        .slice(0, 10)
                                        .map(item => (
                                            <ProductItem
                                                noLazy
                                                key={item.product_id}
                                                as={Card}
                                                user={user ? user : null}
                                                productId={item.product_id}
                                                product={item}
                                            />
                                        ))
                                ) : (
                                    <NoData image empty message="Belum ada produk terbaru buat sekarang" />
                                )}
                            </Card.Group>
                        ) : // <p>Sabar, masih memuat data...</p>
                        null}
                        <Responsive as={Segment} basic {...Responsive.onlyMobile} textAlign="center" padded>
                            <Link to="/p/produk-terbaru" style={{ display: mobile ? "block" : "initial" }}>
                                Lihat <strong>Produk Terbaru</strong> selengkapnya{" "}
                                <Icon size="small" name="chevron right" />
                            </Link>
                        </Responsive>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column className="restock">
                        <Header as="h2" className="section-header">
                            <Header.Content>
                                Restock&nbsp; <Emoji label="bikini" symbol="👙" />
                                <Header.Subheader>
                                    Produk laris yang baru diupdate. &nbsp;{" "}
                                    <Link to="/p/restock" style={{ display: mobile ? "block" : "initial" }}>
                                        Selengkapnya <Icon size="small" name="chevron right" />
                                    </Link>
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                        {restock ? (
                            <Card.Group itemsPerRow={mobile ? 2 : 5}>
                                {restock && restock.length > 0 ? (
                                    restock
                                        .slice(0, 10)
                                        .map(item => (
                                            <ProductItem
                                                noLazy
                                                key={item.product_id}
                                                as={Card}
                                                user={user ? user : null}
                                                productId={item.product_id}
                                                product={item}
                                            />
                                        ))
                                ) : (
                                    <NoData image empty message="Belum ada barang restock buat sekarang" />
                                )}
                            </Card.Group>
                        ) : // <p>Sabar, masih memuat data...</p>
                        null}
                        <Responsive as={Segment} basic {...Responsive.onlyMobile} textAlign="center" padded>
                            <Link to="/p/restock" style={{ display: mobile ? "block" : "initial" }}>
                                Lihat <strong>Produk Restock</strong> selengkapnya{" "}
                                <Icon size="small" name="chevron right" />
                            </Link>
                        </Responsive>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

const mapState = ({ user }) => ({
    user: user.user
})

export default connect(
    mapState,
    { fetchUser }
)(ProductList)
