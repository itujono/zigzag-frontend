import moment from "moment"
import "moment/locale/id"
import Posed from "react-pose"

export const formatter = new Intl.NumberFormat("en-ID", {
    style: "currency",
    currency: "IDR"
})

moment.locale("id")

export const tanggal = (date, format) => {
    return moment(date).format(format)
}

export const tanggalRelative = date => {
    return moment()
        .startOf(date)
        .fromNow()
}

export const priceType = {
    promo: "PROMO",
    vip: "VIP",
    member: "REGULER",
    partner: "PARTNER"
}

export const idKoko = 40

export const colors = [
    { name: "Hijau", code: "green" },
    { name: "Merah", code: "red" },
    { name: "Kuning", code: "yellow" },
    { name: "Pink", code: "pink" },
    { name: "Silver", code: "silver" },
    { name: "Abu-abu", code: "grey" },
    { name: "Hitam", code: "black" },
    { name: "Biru", code: "blue" },
    { name: "Coklat", code: "brown" },
    { name: "Ungu", code: "purple" },
    { name: "Merah marun", code: "purple" },
    { name: "Tosca", code: "teal" },
    { name: "Black", code: "black" },
    { name: "White", code: "white" },
    { name: "Red", code: "red" },
    { name: "Grey", code: "grey" }
]

export const PosedContainer = Posed.div({
    enter: { staggerChildren: 50 }
})

export const Div = Posed.div({
    enter: { y: 0, opacity: 1 },
    exit: { y: 30, opacity: 0 }
})

export const PosedTable = Posed.table({
    enter: { y: 0, opacity: 1 },
    exit: { y: 30, opacity: 0 }
})

export const Par = Posed.p({
    enter: { y: 0, opacity: 1 },
    exit: { y: 30, opacity: 0 }
})

export const H2 = Posed.h2({
    enter: { y: 0, opacity: 1 },
    exit: { y: 30, opacity: 0 }
})

export const Formee = Posed.form({
    enter: { y: 0, opacity: 1 },
    exit: { y: 30, opacity: 0 }
})

export const Img = Posed.img({
    enter: { y: 0, opacity: 1 },
    exit: { y: 30, opacity: 0 }
})

export const imageURL = "https://zigzagbatam.com:8080/storage/img/product"
export const photoURL = "https://zigzagbatam.com:8080/storage/img"
export const imageCategory = "https://zigzagbatam.com:8080/storage/img/category"
export const imageEkspedisi = "https://zigzagbatam.com:8080/storage/img/ekspedisi"

export const mobile = window.innerWidth < 415
export const tablet = window.innerWidth > 414 && window.innerWidth < 769

export const banks = {
    receiver: [
        {
            key: "bca",
            value: "bca 234.23423.2342",
            text: "BCA 234.23423.2342 an Zigzag Online Shop"
        },
        {
            key: "bni",
            value: "bni 234.23423.2342",
            text: "BNI 234.23423.2342 an Zigzag Online Shop"
        },
        {
            key: "mandiri",
            value: "mandiri 234.23423.2342",
            text: "Mandiri 234.23423.2342 an Zigzag Online Shop"
        }
    ],
    sender: [
        { key: "bca", value: "bca", text: "BCA" },
        { key: "bni", value: "bni", text: "BNI" },
        { key: "mandiri", value: "mandiri", text: "Mandiri" },
        { key: "cimb", value: "cimb", text: "CIMB Niaga" },
        { key: "bri", value: "bri", text: "BRI" },
        { key: "mega", value: "mega", text: "Mega" }
    ]
}
