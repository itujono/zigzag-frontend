import React from "react"
import { Grid, Card, Image, Reveal, Header, Button, Icon } from "semantic-ui-react"
import { Div, PosedContainer } from "."
import Slider from "react-slick"
import { Link } from "react-router-dom"
import disclaimer from "../../images/disclaimer.png"
import welcome from "../../images/welcome.png"

const Hero = ({ bannerProducts }) => {
    const noBanner = !bannerProducts || bannerProducts.length < 1

    return (
        <Grid as={PosedContainer} stackable className="hero-section">
            <Grid.Column width={noBanner ? 16 : 12}>
                <Slider dots={true} autoplay={true} touchThreshold={20} autoplaySpeed={4000} pauseOnHover pauseOnFocus>
                    <Card fluid>
                        <Image fluid src={welcome} />
                    </Card>
                    <Card fluid>
                        <Image fluid src={disclaimer} />
                    </Card>
                </Slider>
            </Grid.Column>

            <Grid.Column width={4} className="smaller">
                {bannerProducts &&
                    bannerProducts.map(item => {
                        const title =
                            item.title &&
                            item.title
                                .split(" ")
                                .join("-")
                                .toLowerCase()

                        return (
                            <Card as={Div} fluid key={item.id}>
                                <Reveal animated="move" instant>
                                    <Reveal.Content visible>
                                        <Image
                                            fluid
                                            src="https://static-id.zacdn.com/cms/staticpages/322x165_BAG_LNCMAYONETTE_WOMEN_20180808.jpg"
                                        />
                                    </Reveal.Content>
                                    <Reveal.Content as={Link} to={`/promo/${title}`} hidden>
                                        <Header as="h3" content={item.title} />
                                        {item.description}
                                    </Reveal.Content>
                                </Reveal>
                            </Card>
                        )
                    })}
                {/* <Card as={Div} fluid>
					<Reveal animated="move" instant>
						<Reveal.Content visible>
							<Image fluid src="https://static-id.zacdn.com/cms/dy/360x420_N1_TOPRATEDHEELSANDWEDGESSEMUADIBAWAH399_WOMEN_02180923.jpg" />
						</Reveal.Content>
						<Reveal.Content hidden>
							<Header as="h3" content="Midnight Sale!" />
							Tapi ini lah yang dimau oleh orang Kroasia.
							<div>
								<Button className="btn-zigzag" labelPosition="right" icon="chevron right" content="Lihat" />
							</div>
						</Reveal.Content>
					</Reveal>
				</Card> */}
            </Grid.Column>
        </Grid>
    )
}

export default Hero
