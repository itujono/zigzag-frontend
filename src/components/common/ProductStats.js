import React from "react"
import { Grid, List, Icon } from "semantic-ui-react"

const ProductStats = ({ product, category, shoeStockCount, stockCount, selectedColor, selectedSize }) => {
    return (
        <Grid columns={2}>
            <Grid.Row>
                <Grid.Column>
                    <List relaxed>
                        <List.Item>
                            <Icon name="chart area" />
                            <List.Content>
                                <List.Header>Material</List.Header>
                                {product && product.material}
                            </List.Content>
                        </List.Item>
                    </List>
                </Grid.Column>
                <Grid.Column>
                    <List relaxed>
                        <List.Item>
                            <Icon name="save outline" />
                            <List.Content>
                                <List.Header>Berat</List.Header>
                                {product && product.weight} gr
                            </List.Content>
                        </List.Item>
                    </List>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column>
                    <List relaxed>
                        <List.Item>
                            <Icon name="bath" />
                            <List.Content>
                                <List.Header>Kategori</List.Header>
                                {product && product.categories.name}
                            </List.Content>
                        </List.Item>
                    </List>
                </Grid.Column>
                <Grid.Column>
                    <List relaxed>
                        <List.Item>
                            <Icon name="shield alternate" />
                            <List.Content>
                                <List.Header>Stok</List.Header>
                                {selectedColor && category !== 2 ? selectedColor + " - " + stockCount + " pcs" : "-"}
                                {category === 2 && selectedSize ? selectedColor + " - " + shoeStockCount + " pcs" : "-"}
                            </List.Content>
                        </List.Item>
                    </List>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default ProductStats
