import React from "react"
import CardItem from "./CardItem"
import { Link } from "react-router-dom"
import Tag from "../common/Tag"
import { formatter, imageURL, priceType, idKoko } from "../common"
import { connect } from "react-redux"
import { createCart } from "../state/actions/cartActions"
import LazyLoad from "react-lazyload"
import { Image, Card, Button, Popup } from "semantic-ui-react"
import LazyImage from "./Lazy"

const { promo, member, vip, partner } = priceType

class ProductItem extends React.Component {
    handleAddToCart = product => {
        const { user } = this.props
        const priceTemp = JSON.parse(product.price)

        const promoPrice =
            product &&
            priceTemp
                .filter(price => {
                    if (product.promo && product.promo === 1) return price.price_type === promo
                })
                .map(price => price.price)[0]

        const priceAll =
            product &&
            priceTemp
                .filter(price => {
                    if (user && user.acc_type === 1) return price.price_type === member
                    else if (user && user.acc_type === 2) return price.price_type === vip
                    else return price.price_type === partner
                })
                .map(price => price.price)[0]

        const actualPrice = promoPrice ? promoPrice : priceAll

        const newData = {
            ...product,
            qty: 1,
            price: product.price,
            product_id: product.product_id,
            total_price: actualPrice
        }

        this.props.createCart(newData)
    }

    render() {
        const { product, loading, user, noLazy, productId } = this.props
        const priceTemp = product.price

        const promoPrice =
            product &&
            priceTemp
                .filter(price => {
                    if (product.promo && product.promo === 1) return price.price_type === promo
                })
                .map(price => price.price)[0]

        const priceAll =
            product &&
            priceTemp
                .filter(price => {
                    if (user) {
                        if (user.id === idKoko) return price.price_type === member
                        else {
                            if (user.acc_type === 1) return price.price_type === member
                            else if (user.acc_type === 2) return price.price_type === vip
                            else return price.price_type === partner
                        }
                    } else return price.price_type === member
                })
                .map(price => price.price)[0]

        const actualPrice = promoPrice ? promoPrice : priceAll

        const memberPrice = priceTemp.filter(price => price.price_type === member).map(price => price.price)[0]
        const picture =
            product.picture &&
            product.picture
                .sort(item => {
                    if (item.caption.includes("FEATURED") || item.caption.includes("featured")) return -1
                    return 0
                })
                .map(item => item.image)

        const match = product.product_id === productId
        const productUrl =
            product.product_id +
            "-" +
            product.name
                .split(" ")
                .join("-")
                .toLowerCase()

        const placeholdit = "https://react.semantic-ui.com/images/wireframe/image.png"
        const pictureUrl = picture && picture.length > 1 ? `${imageURL}/${picture[0]}` : `${imageURL}/${picture}`

        return (
            <CardItem className={match && loading ? "product-item loading" : "product-item"}>
                <LazyLoad height={200} throttle={200} offset={100}>
                    {noLazy ? (
                        <Image as={Link} size="medium" to={`/product/${productUrl}`} src={pictureUrl} />
                    ) : (
                        <LazyImage
                            as={Link}
                            id={product.product_id}
                            size="medium"
                            to={`/product/${productUrl}`}
                            src={pictureUrl}
                            className={pictureUrl === placeholdit ? "fit-cover" : ""}
                        />
                    )}
                </LazyLoad>

                {product.hot === 1 && (
                    <Popup
                        trigger={<Tag floating color="red" icon="🔥" label="fire" />}
                        content="Produk ini lagi hot-hot nya"
                    />
                )}
                {product.out_of_stock === true && <Tag basic floating color="purple" text="Stok habis" />}
                <Card.Content>
                    <Card.Header as="a">{product.name}</Card.Header>
                    <Card.Meta>
                        <span className="price">
                            {formatter.format(actualPrice)} <br />
                            {user && user.acc_type !== 1 && user.id !== idKoko && actualPrice !== memberPrice && (
                                <span className="was">{formatter.format(memberPrice)}</span>
                            )}
                        </span>
                    </Card.Meta>
                </Card.Content>
                <Card.Content extra>
                    <Button
                        as={Link}
                        to={`/product/${productUrl}`}
                        basic
                        fluid
                        className="add-to-cart"
                        size="small"
                        content="Beli"
                        icon="plus"
                    />
                </Card.Content>
            </CardItem>
        )
    }
}

const actionList = { createCart }

const mapState = ({ cart }) => ({
    carts: cart.carts,
    loading: cart.loading
})

export default connect(
    mapState,
    actionList
)(ProductItem)
