import React, { Component } from 'react'
import { connect } from 'react-redux'



export default ChildComponent => {
    class ComposedComponent extends Component {

        componentDidMount = () => {
            this.shouldNavigateAway()
        }

        shouldNavigateAway = () => {
            if (localStorage.getItem('token')) this.props.history.goBack()
        }

        render() {
            return <ChildComponent {...this.props} />
        }

    }


    const mapState = ({ auth }) => {
        return { auth: auth.authenticated }
    }



    return connect(mapState)(ComposedComponent)
}