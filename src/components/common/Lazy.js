import React from "react";
import { Visibility, Image, Loader } from 'semantic-ui-react'

class LazyImage extends React.Component {

    state = {
        show: false
    }

    showImage = () => this.setState({ show: true })

    render() {
        const { size } = this.props
        if (!this.state.show) {
            return (
                <Visibility onOnScreen={this.showImage}>
                    <Loader active inline="centered" size={size} />
                </Visibility>
            )
        }
        return <Image {...this.props} />
    }
}

export default LazyImage