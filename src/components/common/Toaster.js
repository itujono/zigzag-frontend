import React from "react"
import { TransitionablePortal, Segment, Header } from "semantic-ui-react"

const segmentStyle = {
    position: "fixed",
    padding: "2em",
    bottom: 15,
    right: 20
}

function Toaster({ header = "", subheader = "", positive = true, open, onClose }) {
    return (
        <TransitionablePortal transition={{ animation: "fade left", duration: 300 }} onClose={onClose} open={open}>
            <Segment className="toaster" inverted color={positive ? "teal" : "orange"} style={segmentStyle}>
                <Header content={header} subheader={subheader} />
            </Segment>
        </TransitionablePortal>
    )
}

export default Toaster
