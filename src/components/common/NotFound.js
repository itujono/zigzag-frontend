import React from "react"
import { Grid, Segment, Container, Header, Button, Image } from "semantic-ui-react"
import Emoji from "../common/Emoji"
import { mobile } from "."
import maintenance from "../../images/maintenance.png"
import zigzag from "../../images/zigzag-logo.png"

const heading = (
    <React.Fragment>
        <span>Tersesat! </span>
        <Emoji label="shit" symbol="💩" />
    </React.Fragment>
)

class NotFound extends React.Component {
    render() {
        return (
            <Grid stackable centered columns={mobile ? 1 : 2} className="centered-grid no-border">
                <Grid.Column>
                    {/* Maintenance */}
                    {/* <Segment padded="very" centered className="wizard-success">
                        <Container textAlign="center">
                            <Image src={zigzag} size="tiny" centered />
                            <Header
                                as="h2"
                                style={{ marginBottom: "2em" }}
                                content="Under Maintenance"
                                subheader="Terima kasih sudah mengunjungi Zigzag Batam. Sekarang, kami sedang dalam masa perbaikan website. Silakan balik beberapa saat lagi. Thank you!"
                            />
                            <Image src={maintenance} size="large" centered />
                        </Container>
                    </Segment> */}

                    <Segment padded="very" centered className="wizard-success">
                        <Container textAlign="center">
                            <Header as="h2" content={heading} />
                            <p className="mb2em">Duh, kejauhan kamu maen tuh. Puter balik lagi gih!</p>
                            <Button
                                className="btn-zigzag"
                                content="Kembali ke Home"
                                onClick={() => window.location.replace("/")}
                            />
                        </Container>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

export default NotFound
