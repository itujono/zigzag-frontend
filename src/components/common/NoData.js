import React from "react"
import { Segment, Header, Icon, Container, Image } from "semantic-ui-react"
import NoDataImage from "../../images/no-data.png"
import EmptyState from "../../images/empty-state.png"
import Emoji from "../common/Emoji"

const NoData = ({ message, icon, image, className, basic, noHeader = false, textAlign, empty = false }) => {
    if (basic)
        return (
            <Segment basic className={`no-data basic ${className}`}>
                <p>
                    {message} <Emoji label="sleepy" symbol=" 😪" />{" "}
                </p>
            </Segment>
        )

    return (
        <Segment secondary textAlign={textAlign || "center"} className={`no-data ${className}`}>
            {image ? (
                <Image src={empty ? EmptyState : NoDataImage} size="small" centered />
            ) : (
                <Icon name={icon} size="huge" />
            )}
            {!noHeader && <Header as="h3" textAlign="center" content={empty ? "Masih kosong nih" : "Nggak ada data"} />}
            <p>
                {message} <Emoji label="sleepy" symbol=" 😪" />{" "}
            </p>
        </Segment>
    )
}

export default NoData
