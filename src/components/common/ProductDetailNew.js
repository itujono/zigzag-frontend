import React from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import { fetchProductItem } from "../state/actions/productActions"
import { createCart, fetchCarts, updateCartItem } from "../state/actions/cartActions"
import { createWishlist, fetchWishlists } from "../state/actions/wishlistActions"
import {
    Grid,
    Image,
    Modal,
    Responsive,
    Container,
    Segment,
    Message,
    Button,
    Icon,
    Header,
    Dropdown,
    Rating,
    Label
} from "semantic-ui-react"
import Tag from "../common/Tag"
import Slider from "react-slick"
import { imageURL, mobile, priceType, idKoko, formatter } from "."
import ProductSpecs from "./ProductSpecs"

const { promo, vip, member, partner } = priceType

const message = (
    <Container className="mb0">
        <span> Nice! Kamu dapat harga spesial karena kamu adalah VIP member kami </span> &nbsp;
        <span role="img" aria-label="bowing">
            🙇
        </span>
    </Container>
)

const messageVip = vipPrice => (
    <React.Fragment>
        <p>
            {" "}
            Gabung jadi VIP member kami, dan dapatkan produk ini cuma seharga {formatter.format(vipPrice)}{" "}
            <span role="img" aria-label="bowing">
                🙇
            </span>
        </p>
        <Button as={Link} basic className="link-btn" size="mini" to="/upgrade">
            <Icon name="plus" /> Gabung
        </Button>
    </React.Fragment>
)

class ProductDetailNew extends React.Component {
    state = { imageModal: false }

    componentDidMount() {
        const token = localStorage.getItem("token")

        this.props.fetchProductItem(this.props.productId)

        if (token) {
            this.props.fetchCarts()
            this.props.fetchWishlists()
        }
    }

    handleModalImage = imageModal => {
        if (mobile) this.setState({ imageModal })
    }

    handleCloseModal = () => this.setState({ imageModal: false })

    handleSelectColor = (e, { value, options }) => {
        this.setState({
            selectedColor: value,
            selectedItem: options.find(item => item.item.color === value).item || {}
        })
    }

    handleSelectSize = item => this.setState({ selectedSize: { size: item.size, id: item.id, stock: item.stock } })

    render() {
        const { product, loading, prices, user, colorOptions, addedToCart, addedToWishlist } = this.props
        const { imageModal, selectedItem, selectedSize } = this.state
        const actualPrice = prices.promoPrice ? prices.promoPrice : prices.priceAll

        return (
            <Grid stackable columns={2} centered padded="very" className={`product-detail ${loading ? "loading" : ""}`}>
                {product && (
                    <React.Fragment>
                        <Grid.Column>
                            {product.picture.length > 1 ? (
                                <Slider dots pauseOnFocus infinite autoplay autoplaySpeed={2000}>
                                    {product.picture.map(item => (
                                        <React.Fragment key={item.id}>
                                            <Responsive
                                                as={Modal}
                                                {...Responsive.onlyMobile}
                                                open={imageModal === item.id && mobile}
                                                content={<Image src={imageURL + "/" + item.id} />}
                                                size="fullscreen"
                                                onClose={this.handleCloseModal}
                                                closeIcon
                                            />
                                            <Container className="wrapper" style={{ paddingLeft: 0, paddingRight: 0 }}>
                                                <Image
                                                    src={imageURL + "/" + item.image}
                                                    onClick={() => this.handleImageModal(item.id)}
                                                />
                                                <p className="caption">{item.caption}</p>
                                            </Container>
                                        </React.Fragment>
                                    ))}
                                </Slider>
                            ) : (
                                <React.Fragment>
                                    <Image src={imageURL + "/" + product.picture[0].image} size="big" rounded />
                                    <p className="caption">{product.picture[0].caption}</p>
                                </React.Fragment>
                            )}
                        </Grid.Column>
                        <Grid.Column>
                            <Container>
                                <Segment basic>
                                    <Responsive
                                        {...Responsive.onlyMobile}
                                        as={Tag}
                                        color="red"
                                        text="HOT"
                                        floating={false}
                                    />
                                    <Header as="h2">
                                        {product.name} &nbsp; &nbsp;{" "}
                                        {product.hot === 1 && (
                                            <Responsive
                                                minWidth={760}
                                                as={Tag}
                                                floating
                                                icon="🔥 "
                                                label="fire"
                                                color="red"
                                                text="HOT "
                                            />
                                        )}
                                        {product.out_of_stock === true && (
                                            <Tag basic color="purple" text="Stok produk ini lagi habis" />
                                        )}
                                    </Header>
                                    <div className="price">
                                        {formatter.format(actualPrice)}{" "}
                                        {user && user.acc_type !== 1 && actualPrice !== prices.memberPrice && (
                                            <span className="was"> {formatter.format(prices.memberPrice)} </span>
                                        )}
                                    </div>
                                    {user && user.acc_type === 2 && (
                                        <Message
                                            className="message-info"
                                            color="green"
                                            content={message}
                                            size="small"
                                        />
                                    )}
                                    {user && user.acc_type === 1 && (
                                        <Message
                                            className="message-info"
                                            color="green"
                                            content={messageVip(prices.vipPrice) || 0}
                                            size="small"
                                        />
                                    )}
                                </Segment>

                                <Segment basic className="product-rating">
                                    <Header as="h4" content="Rating barang" />
                                    <Rating
                                        icon="heart"
                                        maxRating={5}
                                        defaultRating={product.rating_product}
                                        // onRate={this.handleRate}
                                        disabled={!user}
                                    />
                                    <span className="rating-amount">({product.total_customer_rating} rating)</span>
                                </Segment>

                                <Segment basic className="colors">
                                    <Grid stackable columns={2}>
                                        <Grid.Column>
                                            <Header as="h4" content="Pilihan warna" />
                                            <Dropdown
                                                search
                                                placeholder="Pilih warna nya..."
                                                options={colorOptions}
                                                onChange={this.handleSelectColor}
                                            />
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Header as="h4" content="Pilih size tapak nya" />
                                            {/* prettier-ignore */}
                                            {selectedItem && selectedItem.value
                                                ? selectedItem.value.map(item => (
                                                      <Label
                                                          as="a"
                                                          color="teal"
                                                          basic={
                                                              !selectedSize ||
                                                              (selectedSize && selectedSize.id !== item.id)
                                                          }
                                                          active={selectedSize && selectedSize.id === item.id}
                                                          onClick={() => this.handleSelectSize(item)}
                                                          key={item.id}
                                                      >
                                                          {item.size} <Label.Detail>{item.stock}pcs</Label.Detail>
                                                      </Label>
                                                  ))
                                                : "-"}
                                        </Grid.Column>
                                    </Grid>
                                </Segment>

                                <Segment basic className="product-stats">
                                    <ProductSpecs product={product} />
                                </Segment>

                                <Segment basic>
                                    <Button
                                        fluid={mobile}
                                        icon={addedToCart ? "checkmark" : "plus"}
                                        content={
                                            !user
                                                ? "Login dulu ya"
                                                : addedToCart
                                                ? "Tambah lagi ke cart"
                                                : "Add to cart"
                                        }
                                        color={addedToCart ? "grey" : null}
                                        // onClick={() => this.handleAddToCart(product)}
                                        loading={loading}
                                        className="btn-zigzag"
                                        disabled={!user || loading}
                                    />{" "}
                                    &nbsp; &nbsp; {mobile ? <p style={{ textAlign: "center" }}>atau</p> : "atau"}
                                    <Button
                                        basic
                                        fluid={mobile}
                                        icon={addedToWishlist ? "heart" : "heart empty"}
                                        loading={loading}
                                        className="link-btn"
                                        content={addedToWishlist ? "Sudah di Wishlist kamu" : "Tambahkan ke wishlist"}
                                        // onClick={() => this.handleAddWishlist(product.product_id)}
                                        disabled={addedToWishlist || !user}
                                    />
                                    <p className="added">
                                        {addedToCart && (
                                            <React.Fragment>
                                                <Icon name="comment alternate" /> &nbsp;
                                                <span>Produk ini udah ada di cart kamu</span>
                                            </React.Fragment>
                                        )}
                                    </p>
                                </Segment>
                            </Container>
                        </Grid.Column>
                    </React.Fragment>
                )}
            </Grid>
        )
    }
}

const mapState = ({ products, user, cart, wishlist }, ownProps) => {
    const productId = Number(ownProps.match.params.id)
    const product = products.product

    const priceTemp = product && product.price
    const promoPrice =
        priceTemp &&
        priceTemp
            .filter(price => {
                if (product.promo && product.promo === 1) return price.price_type === promo
            })
            .map(price => price.price)[0]
    const priceAll =
        priceTemp &&
        priceTemp
            .filter(price => {
                if (user.user) {
                    if (user.user.id === idKoko) return price.price_type === member
                    else {
                        if (user.user.acc_type === 1) return price.price_type === member
                        else if (user.user.acc_type === 2) return price.price_type === vip
                        else return price.price_type === partner
                    }
                } else {
                    return price.price_type === member
                }
            })
            .map(price => price.price)[0]
    const memberPrice = priceTemp && priceTemp.filter(price => price.price_type === member).map(price => price.price)[0]
    const vipPrice = priceTemp && priceTemp.filter(price => price.price_type === vip).map(price => price.price)[0]

    const moreDetail =
        product &&
        Object.entries(product.product_more_detail).map(([color, value]) => {
            if (value.length > 1) {
                return { color, value }
            }
            return { color, ...value[0] }
        })

    const colorOptions =
        moreDetail && moreDetail.map(item => ({ key: item.color, value: item.color, text: item.color, item }))

    const addedToWishlist = wishlist && wishlist.wishlists.find(wishlist => wishlist.product_id === product.product_id)
    const addedToCart = cart && cart.carts.find(item => item.product_id === productId)

    return {
        user: user.user,
        loading: products.loading,
        prices: { promoPrice, priceAll, memberPrice, vipPrice },
        productId,
        colorOptions,
        product,
        moreDetail,
        addedToCart,
        addedToWishlist
    }
}

const actionList = { fetchCarts, fetchWishlists, fetchProductItem }

// prettier-ignore
export default connect( mapState, actionList )(ProductDetailNew)
