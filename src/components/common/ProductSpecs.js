import React from "react"
import { Grid, List, Icon } from "semantic-ui-react"

const ProductSpecs = ({ product }) => {
    return (
        <Grid columns={2}>
            <Grid.Column>
                <List relaxed>
                    <List.Item>
                        <Icon name="chart area" />
                        <List.Content>
                            <List.Header>Material</List.Header>
                            {product.material}
                        </List.Content>
                    </List.Item>
                </List>
            </Grid.Column>
            <Grid.Column>
                <List relaxed>
                    <List.Item>
                        <Icon name="save outline" />
                        <List.Content>
                            <List.Header>Berat</List.Header>
                            {product.weight} gram
                        </List.Content>
                    </List.Item>
                </List>
            </Grid.Column>
            <Grid.Column>
                <List relaxed>
                    <List.Item>
                        <Icon name="bath" />
                        <List.Content>
                            <List.Header>Kategori</List.Header>
                            {product.categories.name}
                        </List.Content>
                    </List.Item>
                </List>
            </Grid.Column>
        </Grid>
    )
}

export default ProductSpecs
