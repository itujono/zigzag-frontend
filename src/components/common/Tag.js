import React from "react"
import { Label, Icon } from "semantic-ui-react"
import Emoji from "../common/Emoji"

const Tag = ({ icon, label, size, basic, floating, color, text, className }) => (
    <Label color={color} basic={basic} size={size} circular floating={floating} className={className}>
        {icon && <Emoji label={label || ""} symbol={icon} />}
        {text && text}
    </Label>
)

export default Tag
