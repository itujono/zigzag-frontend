import React from "react"
import { connect } from "react-redux"
import { formatter, imageURL, priceType, mobile, idKoko } from "../common"
import { Link } from "react-router-dom"
import Tag from "../common/Tag"
import Slider from "react-slick"
import { createCart, fetchCarts, updateCartItem } from "../state/actions/cartActions"
import { createWishlist, fetchWishlists } from "../state/actions/wishlistActions"
import {
    fetchComments,
    createComment,
    createRating,
    fetchProductItem,
    updateRating,
    productIsRated
} from "../state/actions/productActions"
import { fetchUser } from "../state/actions/userActions"
import { fetchOrderHistory } from "../state/actions/orderActions"
import {
    Grid,
    Segment,
    Button,
    Container,
    Header,
    Image,
    List,
    Icon,
    Message,
    Rating,
    Responsive,
    Label,
    Dropdown,
    Modal
} from "semantic-ui-react"
import Emoji from "./Emoji"
import Comments from "./Comments"
import ProductStats from "./ProductStats"

const { promo, member, vip, partner } = priceType

const message = (
    <Container className="mb0">
        <span> Nice! Kamu dapat harga spesial karena kamu adalah VIP member kami </span> &nbsp;
        <Emoji label="bowing" symbol="🙇" />
    </Container>
)

class ProductDetail extends React.Component {
    state = {
        added: false,
        adding: false,
        initialLoading: true,
        clicked: false,
        rateError: false,
        selectedColor: null,
        selectedSize: null,
        error: "",
        colorSelected: false,
        stockTemp: null,
        shoeStockTemp: null
    }

    componentDidMount() {
        const { productId } = this.props

        this.props.fetchProductItem(productId)
        this.props.fetchComments(productId)
        const { user } = this.props

        if (user) {
            this.props.fetchOrderHistory()
            this.props.fetchCarts()
            this.props.fetchWishlists()
            this.props.productIsRated(productId, user.id)
        }

        this.setState({ initialLoading: false, colorOptions: this.props.colorOptions })
    }

    componentDidUpdate(prevProps, prevState) {
        const { product, productId } = this.props

        if (product && prevProps.product) {
            if (prevProps.productId !== productId) {
                this.props.fetchProductItem(productId)
            }
        }

        if (prevState.addedToCart !== this.state.addedToCart) {
            this.setState({ addedToCart: this.state.addedToCart })
        }
    }

    handleAddToCart = product => {
        const { user, moreDetail, category, carts } = this.props
        const { selectedSize, selectedColor, selectedItem } = this.state
        const { priceTemp, priceAll } = this.props.prices
        const moreDetailId =
            moreDetail && moreDetail.filter(item => item.color === selectedColor).map(item => item.value[0].id)[0]
        const alreadyThere =
            carts && selectedItem
                ? carts.find(item => item.product_more_detail_id === selectedItem.id)
                : carts.find(item => item.color === selectedColor) &&
                  carts.find(item => item.product_id === product.product_id)

        const newData = {
            qty: 1,
            product_id: product.product_id,
            size: selectedItem && selectedItem.size !== null ? selectedItem.size : 0,
            price: JSON.stringify(priceTemp),
            color: selectedColor,
            total_price: alreadyThere ? (Number(alreadyThere.qty) + 1) * priceAll : priceAll,
            product_more_detail_id: category === 2 ? selectedItem && selectedItem.id : moreDetailId,
            weight: product.weight
        }

        if (!user) this.props.history.push("/login")

        if (category === 2) {
            if (selectedColor && selectedColor !== null && selectedItem && selectedItem !== null) {
                let addedLagi = []

                if (this.state.addedToCart && this.state.addedToCart.length > 0) {
                    let existed = this.state.addedToCart.find(item => item.id === selectedItem.id)

                    if (!existed) {
                        addedLagi = [...this.state.addedToCart, { ...selectedItem, stock: selectedItem.stock - 1 }]
                    } else {
                        let addedToCart = [...this.state.addedToCart]
                        const index = addedToCart.findIndex(item => item.id === selectedItem.id)

                        addedToCart[index].stock = addedToCart[index].stock - 1

                        addedLagi = addedToCart
                    }
                } else {
                    addedLagi = [{ ...selectedItem, stock: selectedItem.stock - 1 }]
                }

                this.setState({ error: null })
                this.props.createCart(newData).then(() => {
                    this.setState({
                        selectedItem: { ...this.state.selectedItem, stock: selectedItem.stock - 1 },
                        addedToCart: addedLagi
                    })
                })
            } else {
                this.setState({ error: "Jangan lupa pilih size nya juga ya" })
            }
        } else {
            if (selectedColor && selectedColor !== null && selectedSize === null) {
                const name = Object.keys(this.state).find(item => item === selectedColor)

                this.setState({ error: null })
                this.props.createCart(newData).then(() => {
                    this.setState({
                        [name]: this.state[name] - 1
                    })
                })
            } else {
                this.setState({ error: "Jangan lupa pilih warna nya ya" })
            }
        }
    }

    handleAddWishlist = () => {
        const { user, product, moreDetail } = this.props
        const { selectedSize, selectedColor } = this.state
        const moreDetailId =
            moreDetail &&
            moreDetail
                .filter(item => item.color === selectedColor)
                .map(item => item.value[0].product_color_detail_id)[0]

        const newData = {
            product_id: product.product_id,
            price: JSON.stringify(product.price),
            product_more_detail_id: moreDetailId,
            size: selectedSize && selectedSize !== null ? selectedSize : 0,
            color: selectedColor
        }

        if (!user) this.props.history.push("/login")
        else {
            if (selectedColor && selectedColor !== null) {
                this.setState({ error: null })
                this.props.createWishlist(newData)
            } else {
                this.setState({ error: "Jangan lupa pilih warna nya ya" })
            }
        }
    }

    handleRate = (e, { rating }) => {
        const { product, user, fetchProductItem, productId, ratingId } = this.props

        const data = {
            product_id: productId,
            value_rating: rating
        }

        const dataUpdate = {
            id: ratingId,
            value_rating: rating
        }

        if (user) {
            if (product && product.is_rated === 0) {
                this.props.createRating(data).then(() => fetchProductItem(productId))
            } else {
                this.props.updateRating(dataUpdate).then(() => fetchProductItem(productId))
            }
        } else {
            this.setState({ rateError: true })
        }
    }

    handleSelectColor = (e, { item }) => {
        const name = item.value
        const stock = item.stock
        // const stock = data.options.filter(item => item.value === data.value).map(item => item.stock)
        const existed = Object.keys(this.state).find(item => item === name)

        this.setState({
            [name]: existed ? this.state[name] : Number(stock),
            // [name]: existed ? this.state[name] : Number(stock),
            selectedColor: name,
            colorSelected: true,
            error: null
        })
    }

    handleSelectSize = (e, data) => {
        this.setState({
            selectedSize: data.content,
            selectedItem: data.item
        })
    }

    handleIsEmpty = () => {
        const { category } = this.props
        const { addedToCart, selectedItem, selectedColor } = this.state

        const isThere = addedToCart && selectedItem && addedToCart.find(item => item.id === selectedItem.id)

        if (category === 2 && selectedItem) {
            if (isThere) return isThere.stock
            else return selectedItem.stock
        } else
            for (let color in this.state) {
                if (color === selectedColor) {
                    return this.state[color]
                }
            }
    }

    handleImageModal = imageModal => this.setState({ imageModal })

    handleCloseModal = () => this.setState({ imageModal: null })

    render() {
        const {
            product,
            loading,
            cartLoading,
            user,
            comments,
            added,
            prices,
            colorOptions,
            category,
            moreDetail,
            // isPurchased,
            pictureData,
            shoeTemp,
            addedToCart
        } = this.props
        const { promoPrice, priceAll, vipPrice, memberPrice } = prices
        const {
            initialLoading,
            selectedColor,
            rateError,
            selectedSize,
            error,
            selectedItem,
            shoeStockTemp,
            imageModal
        } = this.state
        const placeholdit = "https://react.semantic-ui.com/images/wireframe/image.png"
        const picture = pictureData && pictureData.map(pic => pic.image)
        const caption = pictureData && pictureData.map(pic => pic.caption)
        const image = picture ? imageURL + "/" + picture : placeholdit
        const actualPrice = promoPrice ? promoPrice : priceAll
        const size =
            moreDetail &&
            moreDetail.filter(item => item.color === selectedColor).map(item => item.value.map(item => item.size))[0]
        const stockCount = Object.keys(this.state).filter(item => item === selectedColor)
        const shoeTemporary = shoeTemp(selectedColor)
        const shoeStock =
            shoeTemporary && shoeTemporary.filter(item => item.size === selectedSize).map(item => item.stock)[0]
        const shoeStockCount = shoeStockTemp !== null ? shoeStockTemp : shoeStock
        const isEmpty = this.handleIsEmpty()

        const messageVip = (
            <React.Fragment>
                <p>
                    {" "}
                    Gabung jadi VIP member kami, dan dapatkan produk ini cuma seharga {formatter.format(vipPrice)}{" "}
                    <Emoji label="bowing" symbol="🙇" />
                </p>
                <Button as={Link} basic className="link-btn" size="mini" to="/upgrade">
                    <Icon name="plus" /> Gabung
                </Button>
            </React.Fragment>
        )

        return (
            <Grid
                stackable
                columns={2}
                centered
                padded="very"
                className={`product-detail ${initialLoading || cartLoading ? "loading" : ""}`}
            >
                <Grid.Column width={7}>
                    {pictureData && pictureData.length > 1 ? (
                        <Slider dots pauseOnFocus infinite touchThreshold={25} autoplay autoplaySpeed={2000}>
                            {pictureData.map(item => (
                                <React.Fragment key={item.image}>
                                    <Modal
                                        open={imageModal === item.image && mobile}
                                        content={<Image src={imageURL + "/" + item.image} />}
                                        size="fullscreen"
                                        onClose={this.handleCloseModal}
                                        closeIcon
                                    />
                                    <Container
                                        className="wrapper"
                                        style={{ paddingLeft: 0, paddingRight: 0 }}
                                        key={item.image}
                                    >
                                        <Image
                                            src={loading ? placeholdit : imageURL + "/" + item.image}
                                            onClick={() => this.handleImageModal(item.image)}
                                        />
                                        <p className="caption">{item.caption}</p>
                                    </Container>
                                </React.Fragment>
                            ))}
                        </Slider>
                    ) : (
                        <React.Fragment>
                            <Image src={loading ? placeholdit : image} size="big" rounded />
                            <p className="caption">{caption}</p>
                        </React.Fragment>
                    )}
                </Grid.Column>
                <Grid.Column width={7}>
                    <React.Fragment>
                        <Container>
                            <Responsive {...Responsive.onlyMobile} as={Tag} color="red" text="HOT" floating={false} />
                            <Header as="h2">
                                {product && product.name} &nbsp; &nbsp;{" "}
                                {product && product.hot === 1 && (
                                    <Responsive
                                        minWidth={760}
                                        as={Tag}
                                        floating
                                        icon="🔥 "
                                        label="fire"
                                        color="red"
                                        text="HOT "
                                    />
                                )}
                                {product && product.out_of_stock === true && (
                                    <Tag basic color="purple" text="Stok produk ini lagi habis" />
                                )}
                            </Header>
                            <div className="price">
                                {formatter.format(actualPrice)}{" "}
                                {user && user.acc_type !== 1 && actualPrice !== memberPrice && (
                                    <span className="was"> {formatter.format(memberPrice)} </span>
                                )}
                            </div>
                            {user && user.acc_type === 2 && (
                                <Message className="message-info" color="green" content={message} size="small" />
                            )}
                            {user && user.acc_type === 1 && vipPrice && (
                                <Message className="message-info" color="green" content={messageVip} size="small" />
                            )}
                        </Container>

                        <Container className="product-rating">
                            <Header as="h4" content="Rating barang" />
                            <Rating
                                icon="heart"
                                maxRating={5}
                                defaultRating={product && product.rating_product}
                                onRate={this.handleRate}
                                disabled={!user}
                            />
                            <span className="rating-amount">({product && product.total_customer_rating} rating)</span>
                            {rateError && <p>Maaf, kamu harus login dulu kalo mau nge-rate produk ini, ya</p>}
                        </Container>

                        <Container className="colors">
                            {category && category === 2 && size ? (
                                <Grid stackable columns={2}>
                                    <Grid.Column>
                                        <Header as="h4" content="Pilihan warna" />
                                        {colorOptions &&
                                            colorOptions.map(item => {
                                                const selected = selectedColor && selectedColor === item.value

                                                return (
                                                    <Label
                                                        color="purple"
                                                        basic={!selected}
                                                        className={selected ? "selected" : ""}
                                                        as="a"
                                                        key={item.key}
                                                        size="small"
                                                        item={item}
                                                        onClick={this.handleSelectColor}
                                                    >
                                                        <span>{item.text}</span>
                                                    </Label>
                                                )
                                            })}
                                        {/* <Dropdown
                                            search
                                            placeholder="Pilih warna nya..."
                                            options={colorOptions}
                                            value={selectedColor}
                                            onChange={this.handleSelectColor}
                                        /> */}
                                        {error && <Message icon="bell outline" header="Oops!" error content={error} />}
                                    </Grid.Column>
                                    <Grid.Column className="shoe-size">
                                        <Header as="h4" content="Ukuran tapak" />
                                        {moreDetail &&
                                            moreDetail
                                                .filter(item => item.color === this.state.selectedColor)
                                                .map(item =>
                                                    item.value.map(detail => {
                                                        const { addedToCart } = this.state
                                                        const added =
                                                            addedToCart &&
                                                            addedToCart.find(cart => cart.id === detail.id)
                                                        const stock =
                                                            added && detail.id === added.id ? added.stock : detail.stock
                                                        const selected = selectedItem && selectedItem.id === detail.id

                                                        return (
                                                            <Label
                                                                as="a"
                                                                basic={!selected}
                                                                color="purple"
                                                                className={selected ? "selected" : ""}
                                                                size="small"
                                                                key={detail.id}
                                                                item={detail}
                                                                onClick={this.handleSelectSize}
                                                            >
                                                                <span>{detail.size}</span> &nbsp; {stock} pcs
                                                            </Label>
                                                        )
                                                    })
                                                )}
                                    </Grid.Column>
                                </Grid>
                            ) : (
                                <React.Fragment>
                                    <Header as="h4" content="Pilihan warna" />
                                    {colorOptions &&
                                        colorOptions.map(item => {
                                            const selected = selectedColor && selectedColor === item.value

                                            return (
                                                <Label
                                                    color="purple"
                                                    basic={!selected}
                                                    className={selected ? "selected" : ""}
                                                    as="a"
                                                    key={item.key}
                                                    size="small"
                                                    item={item}
                                                    onClick={this.handleSelectColor}
                                                >
                                                    <span>{item.text}</span>
                                                </Label>
                                            )
                                        })}
                                    {/* <Dropdown
                                        search
                                        selection
                                        placeholder="Pilih warna nya..."
                                        options={colorOptions}
                                        value={selectedColor}
                                        onChange={this.handleSelectColor}
                                        selectOnBlur={false}
                                    /> */}
                                    {selectedColor && (
                                        <p className="help-text">
                                            Warna <strong>{selectedColor.toLowerCase()}</strong>{" "}
                                            {this.state[stockCount] > 0
                                                ? `sisa ${this.state[stockCount]} pcs`
                                                : " udah abis"}
                                            {/* {this.state.message} */}
                                        </p>
                                    )}
                                    {error && <Message icon="bell outline" header="Oops!" error content={error} />}
                                </React.Fragment>
                            )}
                        </Container>

                        <Segment as={Container} padded className="product-stats">
                            <ProductStats
                                product={product}
                                category={category}
                                selectedColor={selectedColor}
                                selectedSize={selectedSize}
                                stockCount={stockCount}
                                shoeStockCount={shoeStockCount}
                            />
                        </Segment>
                        <Container>
                            {user ? (
                                <Button
                                    fluid={mobile}
                                    icon={addedToCart ? "checkmark" : "plus"}
                                    content={addedToCart ? "Tambah lagi ke cart" : "Add to cart"}
                                    color={addedToCart ? "grey" : null}
                                    onClick={() => this.handleAddToCart(product)}
                                    loading={cartLoading}
                                    className="btn-zigzag"
                                    disabled={isEmpty < 1 || cartLoading}
                                />
                            ) : (
                                <Button
                                    as={Link}
                                    to="/login"
                                    icon="plus"
                                    fluid={mobile}
                                    className="btn-zigzag"
                                    content="Login dulu ya"
                                />
                            )}
                            &nbsp; &nbsp; {mobile ? <p style={{ textAlign: "center" }}>atau</p> : "atau"}
                            <Button
                                fluid={mobile}
                                basic
                                icon={added ? "heart" : "heart empty"}
                                loading={loading}
                                className="link-btn"
                                content={added ? "Sudah di Wishlist kamu" : "Tambahkan ke wishlist"}
                                onClick={() => this.handleAddWishlist(product.product_id)}
                                disabled={added || !user}
                            />
                            <p className="added">
                                {addedToCart && (
                                    <React.Fragment>
                                        <Icon name="comment alternate" /> &nbsp;
                                        <span>Produk ini udah ada di cart kamu</span>
                                    </React.Fragment>
                                )}
                            </p>
                        </Container>
                        <Container className="product-comments">
                            <Comments
                                comments={comments}
                                loading={this.props.loadingComments}
                                fetchComments={this.props.fetchComments}
                                createComment={this.props.createComment}
                                product={product}
                                user={user}
                                // isPurchased={isPurchased}
                            />
                        </Container>
                    </React.Fragment>
                </Grid.Column>
            </Grid>
        )
    }
}

// prettier-ignore
const mapState = ({ products, cart, user, auth, wishlist, order }, ownProps) => {
    const productName = ownProps.match.params.name.split(" ").join("-").toLowerCase()
    const productsReady = productName && products.products && products.products.length > 0
    const productExternal = productsReady && products.products.filter(product =>
            product.name .split(" ") .join("-") .toLowerCase() === productName)[0]
    const initialProduct = products && products.product && products.product

    const product = initialProduct ? initialProduct : productExternal

    const productId = Number(ownProps.match.params.id)
    const added = wishlist.wishlists && product && wishlist.wishlists
        .find(wishlist => wishlist.product_id === product.product_id)
    const addedToCart = cart.carts && cart.carts.find(item => item.product_id === productId)
    const priceTemp = product && product.price && product.price
    const promoPrice = product && priceTemp && priceTemp
        .filter(price => {
            if (product.promo && product.promo === 1) return price.price_type === promo
        })
        .map(price => price.price)[0]
    const priceAll = product && priceTemp && priceTemp
        .filter(price => {
            if (user.user) {
                if (user.user.id === idKoko) return price.price_type === member
                else {
                    if (user.user.acc_type === 1) return price.price_type === member
                    else if (user.user.acc_type === 2) return price.price_type === vip
                    else return price.price_type === partner
                }
            } else {
                return price.price_type === member
            }
        })
        .map(price => price.price)[0]
    const memberPrice = priceTemp && priceTemp.filter(price => price.price_type === member).map(price => price.price)[0]
    const vipPrice = priceTemp && priceTemp.filter(price => price.price_type === vip).map(price => price.price)[0]
    const productColor = product && product.color && product.color
    const category = product && product.categories.id
    const pictureData = product && product.picture.sort(item => {
        if (item.caption.includes("featured") || item.caption.includes("FEATURED")) return -1
        return 0
    })

    const moreDetail = product && Object.entries(product.product_more_detail)
        .map(([color, value]) => ({ color, value }))

    const colorOptions = moreDetail && moreDetail.map(item => ({
        key: item.color,
        text: item.color,
        value: item.color,
        stock: item.value[0] && item.value[0].stock
    }))
    // const isPurchased = user.user && order.orderHistory && order.orderHistory.length > 0 && order.orderHistory
    //     .filter(item => item.status_order_id > 3 && item.status_order_id < 10)
    //     .map(item => item.order_detail)
    //     .flat()
    //     .filter(item => item.product_id === productId)[0]

    const purchased = user.user && order.orderHistory && order.orderHistory.length > 0 && order.orderHistory
    .filter(item => item.status_order_id > 3 && item.status_order_id < 10)
    .map(item => item.order_detail)

    let stock = selectedColor =>
        moreDetail && moreDetail.filter(item => item.color === selectedColor).map(item => item.value[0].stock)[0]

    const shoeTemp = selectedColor =>
        moreDetail && moreDetail.filter(item => item.color === selectedColor).map(item => item.value)[0]

    return {
        product,
        carts: cart.carts,
        user: user.user,
        wishlists: wishlist.wishlists,
        loading: wishlist.loading,
        cartLoading: cart.loading,
        loadingType: cart.loadingType,
        type: wishlist.loadingType,
        comments: products.comments,
        loadingComments: products.loading,
        prices: { priceTemp, promoPrice, priceAll, memberPrice, vipPrice },
        auth: auth.authenticated,
        isRated: products.isRated,
        ratingId: products.ratingId,
        orderHistory: order.orderHistory,
        ratingMessage: products.ratingMessage,
        added,
        productColor,
        category,
        moreDetail,
        colorOptions,
        productId,
        pictureData,
        // isPurchased, <== reference this!
        purchased,
        stock,
        shoeTemp,
        addedToCart
    }
}

const actionList = {
    createCart,
    updateCartItem,
    createWishlist,
    createRating,
    fetchUser,
    fetchComments,
    createComment,
    fetchWishlists,
    fetchCarts,
    fetchProductItem,
    updateRating,
    fetchOrderHistory,
    productIsRated
}

export default connect(
    mapState,
    actionList
)(ProductDetail)
