import React from "react"
import { Button, Comment, Form, Header, Container } from "semantic-ui-react"
import { tanggal } from "../common"

class Comments extends React.Component {
    state = {
        comments: [],
        allLoaded: false,
        value: "",
        submittedComment: ""
    }

    componentDidUpdate(prevProps) {
        const commentList = this.renderCommentsList()

        if (prevProps.comments !== this.props.comments) {
            this.setState({ comments: commentList, allLoaded: true })
        }
    }

    handleChange = (e, { value }) => this.setState({ value })

    renderRestComments = () => {
        const commentList = this.renderCommentsList()
        const { comments } = this.props
        if (comments) {
            this.setState({ comments: commentList, allLoaded: true })
        }
    }

    handleSubmitComment = () => {
        const { value } = this.state
        const { product_id } = this.props.product
        this.setState({ submittedComment: value })

        const newData = { product_id, comment: value }

        if (value) {
            this.props.createComment(newData, () => {
                this.props.fetchComments(product_id)
            })
            this.setState({ value: "" })
        }
    }

    renderCommentsList = () => {
        let commentList =
            this.props.comments &&
            this.props.comments.map(comment => (
                <Comment key={comment.created_date}>
                    <Comment.Avatar
                        as="a"
                        src={comment.customer_picture || "https://react.semantic-ui.com/images/avatar/small/steve.jpg"}
                    />
                    <Comment.Content>
                        <Comment.Author as="a">{comment.customer_name}</Comment.Author>
                        <Comment.Metadata>
                            <span>{tanggal(comment.created_date, "dddd, DD MMMM YYYY")}</span>
                        </Comment.Metadata>
                        <Comment.Text>{comment.comment}</Comment.Text>
                        <Comment.Actions>
                            <a>Balas</a>
                        </Comment.Actions>
                    </Comment.Content>
                </Comment>
            ))

        return commentList
    }

    render() {
        const { allLoaded } = this.state
        const { comments, loading, user /* isPurchased */ } = this.props

        let commentMessage = ""

        if (comments && comments.length > 0) {
            commentMessage = "Ada " + comments.length + " komentar"
        } else if (comments && comments.length === 0) {
            commentMessage = "Belum ada yang komen, ih"
        }

        const commentList = this.renderCommentsList()

        return (
            <Comment.Group threaded>
                <Header as="h3" dividing>
                    Komentar tentang produk ini
                    <Header.Subheader>{commentMessage}</Header.Subheader>
                </Header>

                {commentList}

                {/* {comments &&
                    comments.length > 3 && (
                        <Container textAlign="center" className="load-all">
                            <Button
                                fluid
                                basic
                                content={ !allLoaded ? "Load semua komentar" : "Oke, semua udah di-load" }
                                icon={allLoaded ? null : "angle down"}
                                disabled={allLoaded}
                                onClick={this.renderRestComments}
                            />
                        </Container>
                    )} */}

                {/* { isPurchased && <Form
                    loading={loading}
                    reply
                    onSubmit={this.handleSubmitComment}
                >
                    <Form.TextArea
                        name="comment"
                        placeholder="Gimana penilaian kamu tentang produk ini?"
                        rows={3}
                        value={this.state.value}
                        onChange={this.handleChange}
                    />
                    <p className="help-text">
                        Komentar benar-benar harus mengenai produk ini. Admin
                        berhak membatalkan atau menghapus konten komentar yang
                        keluar dari topik.
                    </p>
                    <Button
                        type="submit"
                        content={!user ? "Login dulu ya" : "Kasih komen kamu"}
                        labelPosition="left"
                        icon="edit"
                        disabled={!user || !this.state.value}
                        className="btn-zigzag"
                    />
                </Form> } */}
            </Comment.Group>
        )
    }
}

export default Comments
