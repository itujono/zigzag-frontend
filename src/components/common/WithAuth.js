import React, { Component } from 'react'
import { connect } from 'react-redux'



export default ChildComponent => {
    class ComposedComponent extends Component {

        componentDidMount = () => {
            this.shouldNavigateAway()
        }
        
        componentDidUpdate = () => {
            this.shouldNavigateAway()
        }

        shouldNavigateAway = () => {
            if (!localStorage.getItem('token')) window.location.replace('/login')
        }

        render() {
            return <ChildComponent {...this.props} />
        }

    }


    const mapState = ({ user }) => {
        return {
            auth: localStorage.getItem('token'),
            user: user.user
        }
    }



    return connect(mapState)(ComposedComponent)
}