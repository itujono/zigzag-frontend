import React from "react"
import { Provider } from "react-redux"
import { Grid } from "semantic-ui-react"
import { createAppStore } from "../components/state/stores/AppStore"
import { AppRouter } from "./routers/AppRouter"
import { mobile } from "./common"

export class App extends React.Component {
    render() {
        return (
            <Provider store={createAppStore()}>
                <Grid centered padded>
                    <Grid.Column width={mobile ? 16 : 14}>
                        <AppRouter />
                    </Grid.Column>
                </Grid>
            </Provider>
        )
    }
}
