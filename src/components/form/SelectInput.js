import React from "react";
import { Form, Select } from "semantic-ui-react";

const SelectInput = ({ input, className, helpText, placeholder, multiple, options, label, search, item, selection, meta: { touched, error } }) => {
    return (
        <Form.Field error={touched && !!error} className={className}>
        <label>{label}</label>
            <Select
                value={input.value || null}
                onChange={(e, data) => input.onChange(data.value)}
                placeholder={placeholder}
                options={options}
                multiple={multiple}
                search={search}
                selection={selection}
            />
            {helpText && <p className="help-text">{helpText}</p>}
            {touched && error && ( <p className="form-error"> {error} </p> )}
        </Form.Field>
    );
};

export default SelectInput;
