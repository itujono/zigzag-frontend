import React from "react";
import { Form, Image } from "semantic-ui-react";
import { imageEkspedisi } from "../common";

const RadioInput = ({ input, width, text, image, type, label, noSegment }) => {

    return (
        <Form.Field width={width}>
        <div className={noSegment ? '' : "ui padded segment"}>
            <div className={`ui radio checkbox`}>
                <input
                    {...input}
                    type={type}
                />{" "}
                <label>{label}</label>
                { image && <Image src={image} size="small" /> }
                { text && text }
            </div>
        </div>
        </Form.Field>
    );
};

export default RadioInput;
