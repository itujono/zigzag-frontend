import React from "react";
import { Form } from "semantic-ui-react";

const TextArea = ({
    input,
    rows,
    width,
    type,
    helpText,
    label,
    placeholder,
    meta: { touched, error }
}) => {
    return (
        <Form.Field error={touched && !!error} width={width}>
            <label>{label}</label>
            <textarea {...input} placeholder={placeholder} rows={rows} />
            {touched && error && ( <p className="form-error"> {error} </p> )}
            { helpText && <span className="help-text">{helpText}</span> }
        </Form.Field>
    );
};

export default TextArea;
