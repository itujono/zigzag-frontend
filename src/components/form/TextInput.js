import React from "react"
import { Form } from "semantic-ui-react"

const TextInput = ({
    input,
    width,
    label,
    type,
    placeholder,
    disabled,
    maxLength,
    helpText,
    meta: { touched, error }
}) => {
    return (
        <Form.Field error={touched && !!error} width={width}>
            <label>{label}</label>
            <input
                {...input}
                onFocus={() => input.onFocus()}
                placeholder={placeholder}
                type={type}
                disabled={disabled}
                maxLength={maxLength}
            />
            {touched && error && <p className="form-error"> {error} </p>}
            {helpText && <span className="help-text">{helpText}</span>}
        </Form.Field>
    )
}

export default TextInput
