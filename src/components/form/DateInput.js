import React from "react"
import { Form } from "semantic-ui-react"
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import moment from "moment"

const DateInput = ({
    input: { value, onChange, onBlur, ...restInput },
    label,
    fluid,
    width,
    placeholder,
    meta: { touched, error },
    ...rest
}) => {
    if (value) {
        value = moment(value, "X")
    }

    return (
        <Form.Field error={touched && !!error} width={width} fluid={fluid}>
            <label>{label}</label>
            <DatePicker
                {...rest}
                placeholderText={placeholder}
                selected={value ? moment(value) : null}
                onChange={onChange}
                onBlur={() => onBlur()}
                filterDate={date => moment() > date}
                {...restInput}
            />
            {touched && error && <p className="form-error"> {error} </p>}
        </Form.Field>
    )
}

export default DateInput
