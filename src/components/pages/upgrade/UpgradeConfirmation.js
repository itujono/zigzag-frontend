import React from "react"
import { Link, withRouter } from "react-router-dom"
import { Field, reduxForm } from "redux-form"
import moment from "moment"
import TextInput from "../../form/TextInput"
import SelectInput from "../../form/SelectInput"
import { banks, mobile } from "../../common"
import { Grid, Form, Button, Header, Image, Message, Segment, Container } from "semantic-ui-react"
import { connect } from "react-redux"
import { upgradeConfirmation, fetchListUpgradeAccount, fetchUser } from "../../state/actions/userActions"
import { fetchBankAccount } from "../../state/actions/orderActions"
import DateInput from "../../form/DateInput"

class UpgradeConfirmation extends React.Component {
    state = { received: false, file: null, imagePreview: "" }

    componentDidMount() {
        this.props.fetchListUpgradeAccount()
        this.props.fetchBankAccount()
    }

    renderKodeOrder = () => {
        const list = this.props.list

        if (list) {
            return list.map(code => ({
                key: code.order_code,
                text: code.order_code,
                value: code.order_code
            }))
        }
    }

    handleChange = (e, orderCode) => {
        this.setState({ orderCode })
    }

    handleImageChange = e => {
        this.setState({ file: e.target.files[0] })
    }

    handleSubmitForm = data => {
        const { file } = this.state
        let formData = new FormData()
        formData.append("evidence", file)
        formData.append("order_code", data.order_code)
        formData.append("bank_sender", data.bank_sender)
        formData.append("bank_number_sender", data.bank_number_sender)
        formData.append("bank_receiver", data.bank_receiver)
        formData.append("date", moment(data.date).format("Y-M-DD"))
        formData.append("total_transfer", data.total_transfer)

        if (formData && formData !== null) {
            this.props.upgradeConfirmation(formData, () => {
                this.setState({ received: true })
            })
        }
    }

    handleConfirmationSubmit = () => this.setState({ received: true })

    render() {
        const { handleSubmit, loading, error, bankAccountOptions, list, orderCode } = this.props
        const { imagePreview, file } = this.state
        const err = error()

        if (this.state.received) {
            return (
                <Grid stackable centered columns={mobile ? 1 : 3} className="centered-grid no-border">
                    <Grid.Column>
                        <Segment padded="very" centered className="wizard-success">
                            <svg
                                width="133px"
                                height="133px"
                                viewBox="0 0 133 133"
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                style={{ marginBottom: "2.5em" }}
                            >
                                <g id="check-group" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <circle id="filled-circle" fill="#8732ff" cx="66.5" cy="66.5" r="54.5" />
                                    <circle id="white-circle" fill="#FFFFFF" cx="66.5" cy="66.5" r="55.5" />
                                    <circle
                                        id="outline"
                                        stroke="#8732ff"
                                        strokeWidth="4"
                                        cx="66.5"
                                        cy="66.5"
                                        r="54.5"
                                    />
                                    <polyline id="check" stroke="#FFFFFF" strokeWidth="4" points="41 70 56 85 92 49" />
                                </g>
                            </svg>
                            <Container textAlign="center">
                                <Header as="h2" content="Konfirmasi upgrade akun selesai!" />
                                <p className="mb2em">
                                    Oke, terima kasih udah ngelakuin konfirmasi upgrade akun VIP. Akan segera kami
                                    validasi. Silakan duduk tenang, and keep shopping all the way!
                                </p>
                                <Button
                                    className="btn-zigzag"
                                    content="Kembali ke Home"
                                    onClick={() => window.location.replace("/")}
                                />
                            </Container>
                        </Segment>
                    </Grid.Column>
                </Grid>
            )
        }

        return (
            <Grid stackable centered columns={mobile ? 1 : 2} className="centered-grid main-form">
                <Grid.Column>
                    <Header as="h2" className="form-header">
                        <Header.Content>
                            Konfirmasi Pembayaran Upgrade Akun
                            <Header.Subheader>
                                Silakan masukkan data-data di bawah ini, agar status akun kamu bisa segera diproses.
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    <Form
                        loading={loading}
                        encType="multipart/form-data"
                        onSubmit={handleSubmit(this.handleSubmitForm)}
                    >
                        <Field
                            name="order_code"
                            type="text"
                            placeholder="Masukkan kode order kamu"
                            component={TextInput}
                            helpText="Cek folder spam kalo di inbox email kamu nggak ada"
                            onChange={this.handleChange}
                            label="Kode order (lihat dari inbox di email kamu)"
                        />

                        <Form.Group>
                            <Field
                                name="bank_sender"
                                type="text"
                                placeholder="Nama bank yang kamu gunakan sebagai pengirim"
                                component={TextInput}
                                label="Bank pengirim"
                                width={4}
                            />
                            <Field
                                name="bank_number_sender"
                                type="text"
                                placeholder="Masukkan nomor rekening kamu (pengirim)"
                                component={TextInput}
                                label="Nomor rekening pengirim"
                                width={12}
                            />
                        </Form.Group>
                        <Field
                            name="bank_receiver"
                            type="text"
                            placeholder="Nomor rekening Zigzag yang kamu transfer"
                            component={SelectInput}
                            options={bankAccountOptions}
                            label="Nomor rekening yang dituju"
                            search
                            selection
                        />
                        <Field
                            name="total_transfer"
                            type="number"
                            placeholder="Tidak perlu pakai titik/koma. Benar: 50000 - Salah: 50.000"
                            component={TextInput}
                            label="Nominal yang ditransfer (Rp)"
                        />
                        <Field
                            name="date"
                            todayButton="Hari ini"
                            type="text"
                            placeholder="Ditransfer pada tanggal..."
                            component={DateInput}
                            label="Tanggal transfer"
                            dateFormat="YYYY-MM-DD"
                        />
                        <Form.Field>
                            <label>Upload Bukti</label>

                            {file && imagePreview !== "" ? (
                                <React.Fragment>
                                    <Image src={imagePreview} size="medium" style={{ marginBottom: "1em" }} />
                                    <Button
                                        icon="picture"
                                        className="btn-zigzag"
                                        content="Ganti"
                                        onClick={() => this.setState({ file: null, imagePreview: "" })}
                                    />
                                </React.Fragment>
                            ) : (
                                <div className="upload-btn-wrapper edit-img-wrapper wizard">
                                    <input
                                        type="file"
                                        name="evidence"
                                        style={{ height: "100%", cursor: "pointer" }}
                                        // accept=".png, .jpg, .jpeg"
                                        onChange={this.handleImageChange}
                                    />
                                </div>
                            )}
                        </Form.Field>

                        {err && err.length > 0 && (
                            <Message style={{ display: "block" }} error header="Oops!" content={err[0]} />
                        )}

                        <Button
                            type="submit"
                            disabled={this.props.invalid || !file || file === null}
                            content="Submit"
                            fluid
                            className="mt2em btn-zigzag"
                        />
                    </Form>
                    <div className="switching">
                        <Button to="/" as={Link} basic className="link-btn mb2em">
                            Kembali ke Home
                        </Button>
                    </div>
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.order_code) errors.order_code = "Kode order nya kok kosong?"
    if (!values.bank_sender) errors.bank_sender = "Nama bank nya juga gak boleh kosong ya"
    if (!values.bank_number_sender)
        errors.bank_number_sender = "Nomor rekening kamu sebagai pengirim wajib diisi loh ya"
    if (!values.total_transfer) errors.total_transfer = "Berapa nomimal yang kamu transfer?"
    else if (values.total_transfer.includes(".") || values.total_transfer.includes(","))
        errors.total_transfer = "Gak perlu pake titik atau koma ya"
    if (!values.bank_receiver) errors.bank_receiver = "Kirim ke rekening kami yang mana?"
    if (!values.date) errors.date = "Kapan transfer nya ini juga wajib dikasihtau ya"
    if (!values.total_transfer) errors.total_transfer = "Transfer berapa emang nya?"

    return errors
}

const mapState = ({ user, order }) => {
    const error = () => {
        if (user && user.error) {
            for (let i in user.error) {
                return user.error[i]
            }
        }
    }

    return {
        error,
        status: user.status,
        list: user.list,
        orderCode: user.list && user.list.order_code,
        bankAccountOptions:
            order.bankAccount &&
            order.bankAccount.map(item => ({
                key: item.id,
                text: item.bank_account + " " + item.number + " " + "a.n." + " " + item.under_name,
                value: item.bank_account + " " + item.number
            }))
    }
}

const actionList = { upgradeConfirmation, fetchListUpgradeAccount, fetchBankAccount, fetchUser }

// prettier-ignore
export default reduxForm({
    form: "UpgradeConfirmation",
    validate,
    destroyOnUnmount: false,
    enableReinitialize: true
})( withRouter( connect( mapState, actionList )(UpgradeConfirmation) ) )
