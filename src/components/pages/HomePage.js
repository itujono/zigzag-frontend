import React from "react"
import { Grid, Container } from "semantic-ui-react"
import { connect } from "react-redux"
import {
    fetchCategories,
    fetch10RestockProducts,
    fetch10LatestProducts,
    fetchBannerPromoProducts,
    fetchProductsByCategory
} from "../state/actions/productActions"
import { fetchCustomerServices } from "../state/actions/userActions"
import ProductList from "../common/ProductList"
import HeroSection from "../common/HeroSection"
import CategorySection from "./CategorySection"
import CsSegment from "../common/CsSegment"
import ath from "../../images/add-to-homescreen.png"
import Timeout from "../common/Timeout"

class HomePage extends React.Component {
    state = { athNotif: false, cs: false }

    componentDidMount() {
        this.props.fetchCategories()
        this.props.fetch10RestockProducts()
        this.props.fetch10LatestProducts()
        this.props.fetchBannerPromoProducts()
        this.props.fetchCustomerServices()
        // this.renderAddToHomeScreenNotif()
    }

    renderAddToHomeScreenNotif = () => {
        setTimeout(() => {
            this.setState({ athNotif: true })
        }, 3000)
    }

    handleFetchProductsByCategory = (categoryId, categoryName) => {
        this.props.fetchProductsByCategory(categoryId)
        this.props.history.push(`/category/${categoryName}`)
    }

    handleCloseNotif = () => this.setState({ athNotif: false })

    handleExpandCs = () => this.setState(prevState => ({ cs: !prevState.cs }))

    render() {
        const {
            featured,
            categories,
            loading,
            restock,
            latests,
            bannerProducts,
            cs,
            error,
            userData,
            partnerType
        } = this.props

        return (
            <div className="main-grid">
                <Grid padded="vertically">
                    <Grid.Column width={16}>
                        <Timeout open={error && error === "timeout"} />
                        <Container>
                            <HeroSection bannerProducts={bannerProducts} />
                            <CategorySection loading={loading} categories={categories} />
                            <ProductList featured={featured} restock={restock} latests={latests} loading={loading} />
                        </Container>
                        {userData && cs && !partnerType && (
                            <CsSegment
                                cs={cs}
                                csState={this.state.cs}
                                onExpandCs={this.handleExpandCs}
                                text={{
                                    header: "Hi, selamat datang!",
                                    subheader: "CS pribadi kamu selalu siap membantu"
                                }}
                            />
                        )}
                        {/* <Responsive
                            {...Responsive.onlyMobile}
                            as={Modal}
                            open={this.state.athNotif}
                            onClose={this.handleCloseNotif}
                            basic
                            size="small"
                        >
                            <Modal.Header content='"Install" Zigzag' />
                            <Modal.Content>
                                <p>
                                    Tahukah kamu bahwa kamu bisa <strong>Add to Home Screen</strong> (install) website
                                    ini di HP mu supaya kamu dapat mengakses website ini lebih cepat, tanpa harus
                                    membuka browser lagi.
                                </p>
                                <Image fluid src={ath} />
                            </Modal.Content>
                        </Responsive> */}
                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}

const mapState = ({ products, user }) => {
    const userCs = user.user && user.user.customer_service_id
    const cs =
        user.cs &&
        user.cs
            .filter(item => item.id === userCs)
            .map(item => ({ name: item.name, wa: item.whatsapp, line: item.line }))[0]
    const userData = user.user && Object.keys(user.user).length > 0
    const partnerType = user.user && user.user.acc_type === 3

    return {
        featured: products.products && products.products.filter(product => product.featured === 1),
        categories: products.categories,
        loading: products.loading,
        restock: products.restock,
        latests: products.latests,
        bannerProducts: products.bannerProducts,
        error: products.error,
        cs,
        partnerType,
        userData
    }
}

const actionList = {
    fetchCategories,
    fetch10RestockProducts,
    fetch10LatestProducts,
    fetchBannerPromoProducts,
    fetchCustomerServices,
    fetchProductsByCategory
}

export default connect(
    mapState,
    actionList
)(HomePage)
