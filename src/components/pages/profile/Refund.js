import React from "react";
import { Grid, Header, List, Icon, Image, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { PosedContainer, Div } from "../../common";



class Refund extends React.Component {

    render() {

        return (
            <Grid>
                <Grid.Column as={PosedContainer}>
                    <Header as="h2" className="form-header">
                        <Header.Content as={Div}>
                            Refund
                            <Header.Subheader>
                                Daftar belanjaan/produk yang kamu refund.
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    <List divided verticalAlign="middle" relaxed="very" size="large" className="refund-list" >
                        <List.Item as={Div}>
                            <Image
                                avatar
                                size="tiny"
                                src="https://randomuser.me/api/portraits/men/65.jpg"
                            />
                            <List.Content>
                                <List.Header as={Link} to="/product">
                                    Kode order: XC 22329 &nbsp;
                                    <Icon name="circle" size="tiny" /> Total belanja: IDR560,000
                                </List.Header>
                                <List.Description>
                                    Kamu me-refund item ini pada hari Kamis, 20 Agustus 2018
                                </List.Description>
                            </List.Content>
                        </List.Item>
                    </List>
                    <Button as={Link} to="/refund" icon="plus" color="black" content="Request refund" size="small" className="btn-zigzag" />
                </Grid.Column>
            </Grid>
        );
    }
}



export default Refund