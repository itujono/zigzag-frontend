import React from "react"
import { Grid, Form, Header, Button, Container, Image, Divider, Segment } from "semantic-ui-react"
import { fetchSubdistricts } from "../../state/actions/rajaOngkirActions"
import Tag from "../../common/Tag"
import { PosedContainer, Div, Formee, photoURL, mobile } from "../../common"
import { connect } from "react-redux"
import { Formik } from "formik"

class Basic extends React.Component {
    state = {
        edit: false,
        selectedProvince: null,
        selectedCity: null,
        helpText: false,
        hovered: false,
        city: this.props.userCity
    }

    componentDidMount() {
        const { userCity } = this.props
        this.props.fetchSubdistricts(Number(userCity))
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.city) {
            if (this.state.city !== prevState.city) {
                this.props.fetchSubdistricts(this.state.city)
            }
        }
    }

    handleUploadImage = e => {
        e.preventDefault()

        const reader = new FileReader()
        const file = e.target.files[0]

        reader.onloadend = () => {
            this.setState({ file, imagePreview: reader.result })
        }

        reader.readAsDataURL(file)
    }

    handleUpdatePicture = () => {
        const form = new FormData()
        form.append("photo_file", this.state.file)

        if (form && form !== null) {
            this.props
                .onChangeProfilePicture(form)
                .then(() => {
                    this.props.onFetchUser()
                })
                .then(() => {
                    this.setState({ file: null })
                })
        }
    }

    handleCancelUpload = () => this.setState({ file: null, imagePreview: null })

    handleEditForm = () => this.setState({ edit: true })

    cancelEditForm = () => this.setState({ edit: false })

    handleSelectProvince = (e, data) => this.setState({ selectedProvince: data })
    handleSelectCity = (e, data) => this.setState({ selectedCity: data, helpText: true })

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleUpdateProfile = data => {
        const { user, updateProfile, cities } = this.props
        const { city, province, subdistrict } = this.state
        const selectedCities = cities && cities.filter(city => city.province === province)
        const zipCode = selectedCities && selectedCities.filter(city => city.value === city).map(city => city.zip)[0]

        const newData = {
            name: data.name ? data.name : user.name,
            email: data.email ? data.email : user.email,
            tele: data.tele ? data.tele : user.tele,
            address: data.address ? data.address : user.address,
            province: province ? province : user.province,
            city: city ? city : user.city,
            subdistrict: subdistrict ? subdistrict : user.subdistrict,
            zip: zipCode ? zipCode : user.zip
        }

        this.setState({ edit: false })
        updateProfile(newData)
    }

    mouseEntered = () => this.setState({ hovered: true })
    mouseLeft = () => this.setState({ hovered: false })

    handleResetFields = () => {
        this.props.reset()
    }

    render() {
        const {
            user,
            loading,
            provinces,
            cities,
            csName,
            redirect,
            statusUpgrade,
            subdistrict,
            subdistrictOptions,
            subdistricts
        } = this.props
        const { hovered, file, imagePreview } = this.state

        const selectedCities = cities && cities.filter(city => city.province === this.state.province)
        const zipCode =
            selectedCities && selectedCities.filter(city => city.value === this.state.city).map(city => city.zip)
        const cityOptions = selectedCities && selectedCities.length > 0 ? selectedCities : cities
        const zip = values => (zipCode && zipCode.length > 0 ? zipCode[0] : values.zip)
        const provinceName =
            provinces &&
            user &&
            provinces.filter(province => province.value === user.province).map(item => item.text)[0]
        const cityName = cities && user && cities.filter(city => city.value === user.city).map(item => item.text)[0]
        const picture =
            imagePreview && imagePreview !== null
                ? imagePreview
                : user && user.picture !== "" && user.picture !== null
                ? photoURL + "/" + user.picture
                : "https://react.semantic-ui.com/images/avatar/small/steve.jpg"

        const type = user && user.acc_type
        const accountType = type === 1 ? "Reguler" : type === 2 ? "VIP" : "Partner"
        const subdistrictName = subdistricts
            .filter(item => Number(item.subdistrict_id) === user.subdistrict)
            .map(item => item.subdistrict_name)[0]

        return (
            <Grid stackable className={`basic ${loading ? "loading" : ""}`}>
                <Grid.Column as={PosedContainer}>
                    <Grid stackable>
                        <Grid.Column as={Div} width={4}>
                            <Container className="user-detail">
                                <div className="photo-wrapper">
                                    <Image src={picture} className={loading ? "loading" : ""} fluid circular />
                                    {file && imagePreview ? (
                                        <div style={{ marginTop: "2em", marginBottom: "2em" }}>
                                            <Button
                                                loading={loading}
                                                fluid
                                                className="btn-zigzag"
                                                content="Pake foto ini!"
                                                onClick={this.handleUpdatePicture}
                                            />
                                            <Button
                                                fluid
                                                className="link-btn"
                                                basic
                                                content="Ganti yang lain"
                                                onClick={this.handleCancelUpload}
                                            />
                                        </div>
                                    ) : (
                                        <div className="upload-zone">
                                            <input
                                                type="file"
                                                name="picture"
                                                id="picture"
                                                placeholder="Upload..."
                                                onChange={this.handleUploadImage}
                                            />
                                            <label htmlFor="picture">
                                                <span role="img" aria-label="cowboy">
                                                    🤠
                                                </span>{" "}
                                                &nbsp; Pilih foto kamu...
                                            </label>
                                        </div>
                                    )}
                                </div>

                                {/* Basic Segment */}
                                <Segment basic>
                                    <Header as="h3" content={user && user.name} className="name" />
                                    <p>{user && user.email}</p>
                                    <p>{user && user.tele}</p>
                                    <Tag
                                        icon={type === 2 ? "🔥 " : "😉 "}
                                        color="green"
                                        text={`Member ${accountType} `}
                                    />
                                    {type !== 2 && type !== 3 && statusUpgrade !== 2 && statusUpgrade !== 3 && (
                                        <div>
                                            <Button
                                                basic
                                                className="link-btn"
                                                fluid={mobile}
                                                icon={hovered ? "checkmark" : null}
                                                onMouseEnter={this.mouseEntered}
                                                onMouseLeave={this.mouseLeft}
                                                onClick={() => redirect("/upgrade")}
                                                content="Upgrade akun?"
                                            />
                                        </div>
                                    )}
                                    {type === 1 && statusUpgrade === 3 && (
                                        <Button
                                            disabled
                                            basic
                                            className="link-btn"
                                            fluid={mobile}
                                            content="Status VIP kamu sedang diproses"
                                            style={{ textAlign: "left" }}
                                        />
                                    )}
                                    {statusUpgrade === 2 && type !== 2 && (
                                        <Button
                                            basic
                                            className="link-btn"
                                            fluid={mobile}
                                            onClick={() => redirect("/upgrade/confirm")}
                                            content="Konfirmasi upgrade akun?"
                                        />
                                    )}
                                </Segment>
                                <Divider />
                                <Segment basic>
                                    <Header as="h4" content="Alamat Pengiriman" />
                                    <p>{user && user.address}</p>
                                    <p>
                                        {cityName}, {subdistrictName}, {provinceName}
                                    </p>
                                    <p>{user && user.zip}</p>
                                </Segment>
                            </Container>
                        </Grid.Column>

                        <Grid.Column as={Div} width={12} className="profile-details">
                            <Segment basic>
                                {this.state.edit ? (
                                    <Formik
                                        onSubmit={values => {
                                            this.handleUpdateProfile(values)
                                        }}
                                        initialValues={user}
                                        enableReinitialize={true}
                                        render={({ values, handleSubmit, handleChange, isSubmitting }) => (
                                            <Form as={Formee} className="editing" onSubmit={handleSubmit}>
                                                <Form.Group widths="equal">
                                                    <Form.Input
                                                        name="name"
                                                        value={values.name}
                                                        type="text"
                                                        onChange={handleChange}
                                                        placeholder="Nama kamu..."
                                                        label="Nama Lengkap"
                                                    />
                                                    <Form.Input
                                                        name="tele"
                                                        value={values.tele}
                                                        type="text"
                                                        onChange={handleChange}
                                                        placeholder="Nomor HP kamu..."
                                                        label="Nomor HP"
                                                    />
                                                </Form.Group>
                                                <Form.Group widths="equal">
                                                    <Form.Select
                                                        name="province"
                                                        type="text"
                                                        placeholder="Provinsi kamu..."
                                                        label="Provinsi"
                                                        defaultValue={values.province}
                                                        options={provinces}
                                                        onChange={this.handleChange}
                                                        search
                                                        selection
                                                    />
                                                    <Form.Select
                                                        name="city"
                                                        type="text"
                                                        placeholder="Kota kamu..."
                                                        label="Kota"
                                                        defaultValue={values.city}
                                                        options={cityOptions}
                                                        onChange={this.handleChange}
                                                        search
                                                        selection
                                                    />
                                                </Form.Group>
                                                <Form.Group widths="equal">
                                                    <Form.Select
                                                        name="subdistrict"
                                                        type="text"
                                                        placeholder="Kecamatan kamu..."
                                                        label="Kecamatan"
                                                        defaultValue={values.subdistrict}
                                                        options={subdistrictOptions}
                                                        onChange={this.handleChange}
                                                        search
                                                        selection
                                                    />
                                                    <Form.Input
                                                        value={zip(values)}
                                                        name="zip"
                                                        type="number"
                                                        placeholder="Misal: 39433"
                                                        label="Kode pos"
                                                    />
                                                </Form.Group>
                                                <Form.TextArea
                                                    name="address"
                                                    placeholder="Alamat rumah/pengiriman kamu..."
                                                    rows={4}
                                                    onChange={handleChange}
                                                    value={values.address}
                                                    label="Alamat rumah/pengiriman"
                                                />
                                                <Button
                                                    type="submit"
                                                    icon="plus"
                                                    content="Simpan perubahan"
                                                    className="btn-zigzag"
                                                    disabled={isSubmitting}
                                                />
                                                <Button
                                                    basic
                                                    content="Batal"
                                                    className="link-btn"
                                                    onClick={this.cancelEditForm}
                                                />
                                            </Form>
                                        )}
                                    />
                                ) : (
                                    <React.Fragment>
                                        <Segment.Group as={Div} className="line-one" horizontal>
                                            <Segment basic>
                                                <Header as="h4" content="Nama Lengkap" />
                                                {user && user.name}
                                            </Segment>
                                            <Segment basic>
                                                <Header as="h4" content="Email Kamu" />
                                                {user && user.email}
                                            </Segment>
                                            <Segment basic>
                                                <Header as="h4" content="Nomor HP" />
                                                {user && user.tele}
                                            </Segment>
                                        </Segment.Group>
                                        <Segment.Group as={Div} horizontal>
                                            <Segment basic>
                                                <Header as="h4" content="Customer Service" />
                                                {csName || "-"}
                                            </Segment>
                                            <Segment basic>
                                                <Header as="h4" content="Tipe Akun" />
                                                {user && accountType} &nbsp; {type === 2 ? "🔥 " : null}
                                            </Segment>
                                        </Segment.Group>
                                        <Segment.Group as={Div} horizontal>
                                            <Segment basic>
                                                <Header as="h4" content="Provinsi" />
                                                {provinceName}
                                            </Segment>
                                            <Segment basic>
                                                <Header as="h4" content="Kota" />
                                                {cityName}
                                            </Segment>
                                            <Segment basic>
                                                <Header as="h4" content="Kecamatan" />
                                                {subdistrict || "-"}
                                            </Segment>
                                        </Segment.Group>
                                        <Segment.Group as={Div} horizontal>
                                            <Segment basic>
                                                <Header as="h4" content="Alamat Pengiriman" />
                                                {user && user.address}
                                            </Segment>
                                            <Segment basic>
                                                <Header as="h4" content="Kode Pos" />
                                                {user && user.zip}
                                            </Segment>
                                        </Segment.Group>
                                        <Button
                                            type="button"
                                            content="Ubah detail"
                                            icon="wrench"
                                            onClick={this.handleEditForm}
                                            disabled={loading}
                                            className="btn-zigzag"
                                        />
                                    </React.Fragment>
                                )}
                            </Segment>
                        </Grid.Column>
                    </Grid>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ rajaOngkir }) => {
    const subdistrictOptions =
        rajaOngkir.subdistricts &&
        rajaOngkir.subdistricts.map(item => ({
            key: item.subdistrict_id,
            value: Number(item.subdistrict_id),
            text: item.subdistrict_name
        }))

    return {
        subdistricts: rajaOngkir.subdistricts,
        subdistrictOptions
    }
}

// prettier-ignore
export default connect( mapState, { fetchSubdistricts } )(Basic);
