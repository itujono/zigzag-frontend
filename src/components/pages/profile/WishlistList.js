import React from "react";
import { List, Image, Button, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";
import Prompt from "../../common/Prompt";
import NoData from "../../common/NoData";
import { tanggal, formatter, imageURL, priceType, PosedContainer, Div } from "../../common";




const { promo, member, vip, partner } = priceType


class WishlistList extends React.Component {
    state = { deleted: null };

    render() {
        const { handleOpenPrompt, prompt, confirmPrompt, wishlists, deleteWishlist, createCart, loading } = this.props;

        if (wishlists && wishlists.length < 1)
            return (
                <NoData
                    icon="building outline"
                    message="Sama sekali nggak ada yg bisa dilihat buat sekarang ya. Heheh."
                    image
                />
            );

        return (
            <List as={PosedContainer} divided verticalAlign="middle" relaxed="very" size="large" className="wishlist-list" >
                {wishlists && wishlists.map(item => {
                    const { user } = this.props
                    const priceTemp = JSON.parse(item.price)

                    const promoPrice = item && priceTemp.filter(price => {
                        if (item.promo && item.promo === 1) return price.price_type === promo
                    }).map(price => price.price)[0]

                    const priceAll = item && priceTemp.filter(price => {
                        if (user) {
                            if (user && user.acc_type === 1) return price.price_type === member
                            else if (user && user.acc_type === 2) return price.price_type === vip
                            else return price.price_type === partner
                        } else {
                            return price.price_type === member
                        }
                    }).map(price => price.price)[0]

                    return (
                        <List.Item as={Div} key={item.id}>
                            <Image avatar size="tiny" src={imageURL + "/" + item.picture} />
                            <List.Content>
                                <List.Header as={Link} to={`/product/${item.product_id}`} >
                                    {item.name} &bull;{" "} {formatter.format(promoPrice ? promoPrice : priceAll)}
                                </List.Header>
                                <List.Description>
                                    Kamu nge-wishlist item ini pas hari{" "}
                                    {tanggal( item.created_date, "dddd, DD MMMM YYYY" )}
                                </List.Description>
                                <Button
                                    icon="plus"
                                    className="btn-zigzag"
                                    content="Add to cart"
                                    size="small"
                                    onClick={() => createCart(item)}
                                />{" "}
                                &nbsp; atau &nbsp;&nbsp;&nbsp;
                                <Button
                                    icon="trash alternate outline"
                                    basic
                                    content="Hapus dari Wishlist"
                                    size="small"
                                    className="delete-btn"
                                    onClick={handleOpenPrompt}
                                />
                            </List.Content>
                            <Prompt
                                header="Hapus wishlist?"
                                open={prompt}
                                onClose={() => confirmPrompt(false)}
                                confirm={() => { deleteWishlist(item.id); this.setState({ deleted: true }); }}
                                yesText="Pastinya"
                                size="tiny"
                            >
                                Yakin mau hapus{" "} <strong>{item.product_name}</strong> dari Wishlist kamu?
                            </Prompt>
                        </List.Item>
                    )})
                }
            </List>
        );
    }
}

export default WishlistList;
