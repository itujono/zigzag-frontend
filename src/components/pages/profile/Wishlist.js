import React from "react";
import { Grid, Header } from "semantic-ui-react";
import { connect } from "react-redux";
import { fetchWishlists, deleteWishlist } from "../../state/actions/wishlistActions";
import { fetchUser } from "../../state/actions/userActions";
import { createCart } from "../../state/actions/cartActions";
import WishlistList from "./WishlistList";
import Loading from "../../common/Loading";
import { priceType, Div, PosedContainer, idKoko } from "../../common";




const { promo, member, vip, partner } = priceType


class Wishlist extends React.Component {
    state = {
        prompt: false
    };

    componentDidMount() {
        this.props.fetchWishlists()
        this.props.fetchUser()
    }

    handleOpenPrompt = () => this.setState({ prompt: true });

    confirmPrompt = (condition) => {
        this.setState({ prompt: condition })
    }

    handleDeleteWishlist = (product) => {
        this.props.deleteWishlist(product)
        this.setState({ prompt: false })
    }

    handleAddToCart = (product) => {
        const { user } = this.props
        const priceTemp = JSON.parse(product.price)

        const promoPrice = product && priceTemp.filter(price => {
            if (product.promo && product.promo === 1) return price.price_type === promo
        }).map(price => price.price)[0]

        const priceAll = product && priceTemp.filter(price => {
            if (user.id === idKoko) return price.price_type === member
            else {
                if (user.acc_type === 1) return price.price_type === member
                else if (user.acc_type === 2) return price.price_type === vip
                else return price.price_type === partner
            }
        }).map(price => price.price)[0]

        const actualPrice = promoPrice ? promoPrice : priceAll


        const data = {
            name: product.products_name,
            // customer_id: this.props.user.id,
            product_id: product.product_id,
            price: product.price,
            qty: 1,
            weight: product.weight,
            total_price: actualPrice,
            color: product.color,
            size: product.size,
            product_more_detail_id: product.product_more_detail_id
        }

        if (data) {
            this.props.createCart(data)
            this.props.deleteWishlist(product.id)
        }

    }

    render() {
        const { wishlists, loading, user } = this.props

        return (
            <Grid className="">
                <Grid.Column as={PosedContainer}>
                    <Header as="h2" className="form-header">
                        <Header.Content as={Div}>
                            Wishlist
                            <Header.Subheader>
                                Daftar item-item yang kamu sukai di Zigzag.
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    <WishlistList
                        wishlists={wishlists}
                        prompt={this.state.prompt}
                        handleOpenPrompt={this.handleOpenPrompt}
                        confirmPrompt={this.confirmPrompt}
                        deleteWishlist={this.handleDeleteWishlist}
                        createCart={this.handleAddToCart}
                        user={user}
                        loading={loading}
                    />
                </Grid.Column>
            </Grid>
        );
    }
}

const mapState = ({ wishlist, user }) => ({
    wishlists: wishlist.wishlists,
    loading: wishlist.loading,
    user: user.user
});

export default connect( mapState, { fetchWishlists, deleteWishlist, fetchUser, createCart } )(Wishlist);