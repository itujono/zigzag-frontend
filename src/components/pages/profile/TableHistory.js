import React from "react"
import { Table, Header, Responsive, List, Grid, Item } from "semantic-ui-react"
import { formatter } from "../../common"

const TableHistory = ({
    orderDetail,
    otherDetails: { ongkir, discount, subtotal, grandtotal, weight, akunKoko, uniqueCode }
}) => (
    <React.Fragment>
        <Responsive minWidth={760} as={Table} className="history-table">
            <Table.Header>
                <Table.Row textAlign="center">
                    <Table.HeaderCell singleLine textAlign="left">
                        Item
                    </Table.HeaderCell>
                    <Table.HeaderCell>Kategori</Table.HeaderCell>
                    <Table.HeaderCell>Harga</Table.HeaderCell>
                    <Table.HeaderCell>Kuantitas</Table.HeaderCell>
                    <Table.HeaderCell>Total</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {orderDetail &&
                    orderDetail.map(item => {
                        return (
                            <Table.Row key={item.id} textAlign="center">
                                <Table.Cell textAlign="left">
                                    <Header as="h5"> {item.product_name} </Header>
                                    Warna {item.product_color.toLowerCase()} <br />
                                    {item.product_size !== 0 && (
                                        <span style={{ color: "#999" }}>Size {item.product_size}</span>
                                    )}
                                </Table.Cell>
                                <Table.Cell singleLine>{item.category_name}</Table.Cell>
                                <Table.Cell singleLine>
                                    {akunKoko ? "-" : formatter.format(item.product_price)}
                                </Table.Cell>
                                <Table.Cell textAlign="center">{item.product_qty}</Table.Cell>
                                <Table.Cell>{akunKoko ? "-" : formatter.format(item.product_total_price)}</Table.Cell>
                            </Table.Row>
                        )
                    })}
            </Table.Body>

            <Table.Footer>
                <Table.Row textAlign="center">
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell>
                        Total berat <br /> <strong>{weight} gram</strong>
                    </Table.HeaderCell>
                    <Table.HeaderCell disabled>Subtotal</Table.HeaderCell>
                    <Table.HeaderCell disabled>{akunKoko ? "-" : formatter.format(subtotal)}</Table.HeaderCell>
                </Table.Row>
                <Table.Row textAlign="center">
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell>Ongkos kirim</Table.HeaderCell>
                    <Table.HeaderCell>{formatter.format(ongkir)}</Table.HeaderCell>
                </Table.Row>
                <Table.Row textAlign="center">
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell>Discount</Table.HeaderCell>
                    <Table.HeaderCell>
                        - {discount !== null && discount !== 0 ? formatter.format(discount) : 0}
                    </Table.HeaderCell>
                </Table.Row>
                <Table.Row textAlign="center">
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell>Kode unik transfer</Table.HeaderCell>
                    <Table.HeaderCell>{uniqueCode || "-"}</Table.HeaderCell>
                </Table.Row>
                <Table.Row textAlign="center">
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell>
                        <strong>General Total</strong>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                        <strong>{akunKoko ? "-" : formatter.format(grandtotal)}</strong>
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Footer>
        </Responsive>

        <Responsive {...Responsive.onlyMobile} as={Grid} columns={1} className="history-table">
            <Grid.Column>
                <Item.Group divided unstackable>
                    {orderDetail &&
                        orderDetail.map(item => {
                            return (
                                <Item key={item.id} textAlign="center">
                                    <Item.Content>
                                        <Item.Header content={item.product_name} />
                                        <Item.Meta>
                                            Warna <strong>{item.product_color.toLowerCase()}</strong> <br />
                                            {item.product_size !== 0 && (
                                                <span>
                                                    Size <strong>{item.product_size}</strong>
                                                </span>
                                            )}{" "}
                                            &middot; Kategori <strong>{item.category_name}</strong>
                                        </Item.Meta>
                                        <Item.Extra>
                                            <p>
                                                {akunKoko ? "-" : formatter.format(item.product_price)} x{" "}
                                                {item.product_qty}
                                            </p>
                                            <p>
                                                Total harga <br />{" "}
                                                <strong>
                                                    {akunKoko ? "-" : formatter.format(item.product_total_price)}
                                                </strong>
                                            </p>
                                        </Item.Extra>
                                    </Item.Content>
                                    {/* <Table.Cell textAlign="left">
                                    <Header as="h5"> {item.product_name} </Header>
                                    Warna {item.product_color.toLowerCase()} <br />
                                    {item.product_size !== 0 && (
                                        <span style={{ color: "#999" }}>Size {item.product_size}</span>
                                    )}
                                </Table.Cell> */}
                                    {/* <Table.Cell singleLine>{item.category_name}</Table.Cell>
                                    <Table.Cell singleLine>
                                        {akunKoko ? "-" : formatter.format(item.product_price)}
                                    </Table.Cell>
                                    <Table.Cell textAlign="center">{item.product_qty}</Table.Cell>
                                    <Table.Cell>
                                        {akunKoko ? "-" : formatter.format(item.product_total_price)}
                                    </Table.Cell> */}
                                </Item>
                            )
                        })}
                </Item.Group>
                <List bulleted>
                    <List.Item>
                        <List.Header content="Total berat" />
                        {weight} gram
                    </List.Item>
                    <List.Item>
                        <List.Header content="Subtotal" />
                        {akunKoko ? "-" : formatter.format(subtotal)}
                    </List.Item>
                    <List.Item>
                        <List.Header content="Ongkos kirim" />
                        {formatter.format(ongkir)}
                    </List.Item>
                    <List.Item>
                        <List.Header content="Discount" />-{" "}
                        {discount !== null && discount !== 0 ? formatter.format(discount) : 0}
                    </List.Item>
                    <List.Item>
                        <List.Header content="Kode unik transfer" />
                        {uniqueCode || "-"}
                    </List.Item>
                    <List.Item>
                        <List.Header content="General total" />
                        {akunKoko ? "-" : formatter.format(grandtotal)}
                    </List.Item>
                </List>
            </Grid.Column>
        </Responsive>
    </React.Fragment>
)

export default TableHistory
