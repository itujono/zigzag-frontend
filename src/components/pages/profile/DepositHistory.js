import React from "react"
import { Feed, Icon, Button } from "semantic-ui-react"
import moment from "moment"
import { formatter, tanggal, PosedContainer, Div } from "../../common"
import Emoji from "../../common/Emoji"
import NoData from "../../common/NoData"
import { Link, withRouter } from "react-router-dom"

const DepositHistory = ({ deposits, value, history }) => {
    if (status === 1) return ""
    const depositData = [...deposits]

    if (deposits && deposits.length < 1) {
        return <NoData message="Kamu belum ada ngelakuin deposit sama sekali" image />
    }

    const handleRedirect = kodeOrder => {
        localStorage.setItem("kodeOrder", JSON.stringify(kodeOrder))
        history.push("/deposit-confirmation")
    }

    return (
        <Feed as={PosedContainer}>
            {deposits &&
                deposits
                    .sort((a, b) => a.id < b.id)
                    .filter(item => {
                        if (value === 5) return depositData
                        else return item.status === value
                    })
                    .map(deposit => {
                        const code = deposit.status && deposit.status.status_id
                        let status = ""
                        let description = ""

                        if (code === 1)
                            status = (
                                <span>
                                    Pending <Emoji label="pending" symbol="🙁" />
                                </span>
                            )
                        else if (code === 2)
                            status = (
                                <span style={{ color: "#ef6181" }}>
                                    Ditolak <Emoji label="declined" symbol="😥" />
                                </span>
                            )
                        else if (code === 3)
                            status = (
                                <span className="tosca">
                                    Diterima <Emoji label="approved" symbol="🤑" />
                                </span>
                            )
                        else if (code === 5)
                            status = (
                                <span>
                                    Sedang diverifikasi <Emoji label="pending" symbol="🙁" />
                                </span>
                            )

                        if (code !== 4) {
                            description = `Kamu melakukan deposit sebesar ${formatter.format(deposit.total)}`
                        } else {
                            description = `Kamu pake deposit kamu untuk belanja sebesar ${formatter.format(
                                deposit.total
                            )}`
                        }

                        return (
                            <Feed.Event as={Div} key={deposit.id}>
                                <Feed.Label>
                                    <Icon name="compass outline" />
                                </Feed.Label>
                                <Feed.Content>
                                    <Feed.Summary>
                                        {code !== 4 ? "Deposit Baru" : "Pake Buat Belanja"}
                                        <Feed.Date>
                                            {tanggal(deposit.created_date, "dddd, DD MMMM YYYY")} (
                                            {moment(deposit.created_date).fromNow()})
                                        </Feed.Date>
                                    </Feed.Summary>
                                    <Feed.Extra text>{description}</Feed.Extra>
                                    {code !== 4 && <p> Status: {status} </p>}
                                    {code === 1 && (
                                        <React.Fragment>
                                            Belum konfirmasi? &nbsp;
                                            <a onClick={() => handleRedirect(deposit.kode_order)}>
                                                Konfirmasi deposit sekarang
                                            </a>
                                        </React.Fragment>
                                    )
                                    // <Button as={Link} to="/deposit-confirmation" icon labelPosition="right" className="btn-zigzag">
                                    //     <Icon name="chevron right" />
                                    //     Konfirmasi deposit
                                    // </Button>
                                    }
                                </Feed.Content>
                            </Feed.Event>
                        )
                    })}
        </Feed>
    )
}

export default withRouter(DepositHistory)
