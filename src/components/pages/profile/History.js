import React from "react"
import { Grid, Header, Dropdown, Container } from "semantic-ui-react"
import { withRouter } from "react-router-dom"
import HistoryList from "./HistoryList"
import { connect } from "react-redux"
import { cancelOrder, saveOrderCode, fetchAirwayBills } from "../../state/actions/orderActions"
import { PosedContainer, Div, mobile } from "../../common"
import Timeout from "../../common/Timeout"

const filters = [
    { key: 1, value: 1, text: "menunggu pembayaran" },
    { key: 3, value: 3, text: "sedang memproses pembayaran" },
    { key: 4, value: 4, text: "pembayaran disetujui" },
    { key: 5, value: 5, text: "sedang diproses di gudang" },
    { key: 6, value: 6, text: "barang terkirim" },
    { key: 7, value: 7, text: "dibatalkan oleh admin" },
    { key: 8, value: 8, text: "pembayaran pake deposit" },
    { key: 9, value: 9, text: "pembayaran selesai" },
    { key: 10, value: 10, text: "dibatalkan oleh customer" },
    { key: "all", value: "all", text: "semua" }
]

class History extends React.Component {
    state = {
        active: false,
        showForm: false,
        prompt: false,
        reason: "",
        value: "all",
        page: 1
    }

    handleToggleDetail = () => {
        this.setState({ detailOpen: !this.state.detailOpen })
    }

    handleSelectOrderItem = (orderItem, resi, expeditionCode) => {
        this.setState({ orderItem: this.state.orderItem === orderItem ? null : orderItem })
        if (resi && resi !== null && expeditionCode) {
            this.props.fetchAirwayBills(resi, expeditionCode)
        }
    }

    handleFilterList = (e, { value }) => this.setState({ value })

    handleShowForm = () => this.setState({ showForm: true })
    handleCloseForm = () => this.setState({ showForm: false })
    handleShowPrompt = () => this.setState({ prompt: true })
    handleClosePrompt = () => this.setState({ prompt: false })
    handleChangeForm = (e, { name, value }) => this.setState({ [name]: value })
    handleSubmitForm = id => {
        this.setState({ prompt: false, reason: "", showForm: false })
        const data = { id, remark_cancel_order: this.state.reason }
        this.props.cancelOrder(data)
    }

    handleSaveOrderCode = data => {
        const newData = {
            orderId: data.order_id,
            orderCode: data.order_code
        }
        this.props.saveOrderCode(newData)
        this.props.history.push("/order-confirmation")
    }

    handleChangePage = condition => {
        const { page } = this.state
        if (condition === "next") {
            this.setState(
                prevState => ({ page: prevState.page + 1 }),
                () => {
                    this.props.onFetchOrderHistory(this.state.page)
                }
            )
        } else {
            if (page > 1) {
                this.setState(
                    prevState => ({ page: prevState.page - 1 }),
                    () => {
                        this.props.onFetchOrderHistory(this.state.page)
                    }
                )
            }
        }
    }

    render() {
        const {
            loading,
            orderHistory,
            status,
            awb,
            expeditionCode,
            orderSymbol,
            akunKoko,
            error,
            onFetchOrderHistory
        } = this.props

        return (
            <Grid stackable className="order-history">
                <Grid.Column as={PosedContainer}>
                    <Grid stackable>
                        <Timeout open={error && error === "timeout"} />
                        <Grid.Column floated="left" width={6}>
                            <Header as="h2" className="form-header">
                                <Header.Content as={Div}>
                                    History Pemesanan
                                    <Header.Subheader>
                                        Daftar orderan yang udah kamu lakukan sepanjang hidup.
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>
                        </Grid.Column>
                        <Grid.Column
                            as={Div}
                            floated={mobile ? undefined : "right"}
                            textAlign={mobile ? "center" : "right"}
                            width={6}
                            className="filterer"
                        >
                            {orderHistory && orderHistory.length > 0 && (
                                <Header as="h4">
                                    Tampilkan &nbsp;
                                    <Dropdown
                                        pointing
                                        inline
                                        options={filters}
                                        onChange={this.handleFilterList}
                                        defaultValue={filters[filters.length - 1].value}
                                    />
                                </Header>
                            )}
                        </Grid.Column>
                    </Grid>
                    <Container>
                        {orderHistory ? (
                            <HistoryList
                                loading={loading}
                                selectOrderItem={this.handleSelectOrderItem}
                                handleShowForm={this.handleShowForm}
                                states={this.state}
                                data={{ awb, status, orderHistory, expeditionCode, orderSymbol, akunKoko }}
                                onSaveOrderCode={this.handleSaveOrderCode}
                                handleFilterList={this.handleFilterList}
                                handleChangeForm={this.handleChangeForm}
                                handleShowPrompt={this.handleShowPrompt}
                                handleClosePrompt={this.handleClosePrompt}
                                handleSubmitForm={this.handleSubmitForm}
                                handleCloseForm={this.handleCloseForm}
                                onChangePage={this.handleChangePage}
                            />
                        ) : (
                            "Sabar, masih loading..."
                        )}
                    </Container>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ order }, ownProps) => {
    const expeditionCode = company => {
        if (company === "SiCepat Express") return "sicepat"
        else if (company === "J&T Express") return "jnt"
        else if (company === "JNE") return "jne"
        else if (company === "Jalur Nugraha Ek") return "jne"
        else return "lion"
    }

    const orderSymbol =
        ownProps.orderHistory &&
        ownProps.orderHistory.map(item => {
            switch (item.status_order_id) {
                case 1:
                    return { ...item, icon: "🤔", color: "orange" }
                    break
                case 3:
                    return { ...item, icon: "🤫", color: "orange" }
                    break
                case 4:
                    return { ...item, icon: "😘", color: "green" }
                    break
                case 5:
                    return { ...item, icon: "🏠", color: "blue" }
                    break
                case 6:
                    return { ...item, icon: "🚚", color: "green" }
                    break
                case 7:
                    return { ...item, icon: "😰", color: "red" }
                    break
                case 8:
                    return { ...item, icon: "😘", color: "green" }
                    break
                case 9:
                    return { ...item, icon: "😍", color: "green" }
                    break
                case 10:
                    return { ...item, icon: "😬", color: "red" }
                    break
                default:
                    return { ...item, icon: "", color: "green" }
            }
        })

    return {
        status: order.status,
        awb: order.awb,
        expeditionCode,
        orderSymbol
    }
}

export default connect(
    mapState,
    { cancelOrder, saveOrderCode, fetchAirwayBills }
)(withRouter(History))
