import React from "react"
import { Grid, Button, Container, Divider, Header, Form, Message } from "semantic-ui-react"
import { reduxForm, Field } from "redux-form"
import { connect } from "react-redux"
import TextInput from "../../form/TextInput"
import { changePassword } from "../../state/actions/userActions"
import { PosedContainer, Div, Formee } from "../../common"

class SettingsPage extends React.Component {
    state = {
        editing: false,
        editingEmail: false
    }

    handleChangeEditing = condition => {
        this.setState({ editing: condition })
    }

    handleSubmitForm = data => {
        const { new_password2, ...rest } = data
        this.props.changePassword(rest)
    }

    render() {
        const { editing } = this.state
        const { handleSubmit, loading, error, pristine } = this.props

        return (
            <Grid stackable className={`settings-page ${loading ? "loading" : null}`}>
                <Grid.Column as={PosedContainer} width={8}>
                    <Container>
                        <Header as="h2" className="form-header">
                            <Header.Content as={Div}>
                                Settings
                                <Header.Subheader>Tempat buat yang teknis-teknis ya.</Header.Subheader>
                            </Header.Content>
                        </Header>
                        <Form as={Formee} onSubmit={handleSubmit(this.handleSubmitForm)}>
                            {!editing ? (
                                <div className="field">
                                    <label>Ubah Password</label>
                                    <input
                                        disabled
                                        type="password"
                                        name="password"
                                        placeholder="Sembarang"
                                        value="Sembarang"
                                    />
                                </div>
                            ) : (
                                <React.Fragment>
                                    <Field
                                        type="password"
                                        name="new_password"
                                        component={TextInput}
                                        placeholder="Minimal 8 karakter ya"
                                        label="Ubah Password"
                                    />
                                    <Field
                                        type="password"
                                        name="new_password2"
                                        component={TextInput}
                                        placeholder="Harus sama persis yaa"
                                        label="Ulangi Password"
                                    />
                                    <Divider />
                                    <Field
                                        type="password"
                                        name="old_password"
                                        component={TextInput}
                                        placeholder="Masih inget kan password lama?"
                                        label="Masukin Password Lama"
                                    />
                                </React.Fragment>
                            )}

                            {error && <Message style={{ display: "block" }} error header="Wah!" content={error} />}

                            {editing ? (
                                <React.Fragment>
                                    <Button
                                        type="submit"
                                        className="btn-zigzag"
                                        content="Ok, Ubah!"
                                        disabled={pristine}
                                    />{" "}
                                    &nbsp;
                                    <Button
                                        content="Batal"
                                        className="link-btn"
                                        basic
                                        onClick={() => this.handleChangeEditing(false)}
                                    />
                                </React.Fragment>
                            ) : (
                                <Button
                                    className="btn-zigzag"
                                    icon="sun outline"
                                    content="Ubah password"
                                    onClick={() => this.handleChangeEditing(true)}
                                />
                            )}
                        </Form>
                    </Container>
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.new_password) errors.new_password = "Wah kok dikosongin nih?"
    else if (values.new_password.length < 8) errors.new_password = "Hayooo, belum 8 karakter nih"
    if (!values.new_password2) errors.new_password2 = "Ini juga nih, kok dikosongin?"
    else if (values.new_password2 !== values.new_password)
        errors.new_password2 = "Harus matching ya satu sama lainnya. Yuuuk"
    else if (values.new_password2.length < 8) errors.new_password2 = "Ini juga harus 8 karakter minimal loh"
    if (!values.old_password) errors.old_password = "Ini juga wajib loh, ah!"

    return errors
}

const mapState = ({ user }) => ({
    loading: user.loading,
    error: user.error
})

export default reduxForm({ form: "Settings", validate })(
    connect(
        mapState,
        { changePassword }
    )(SettingsPage)
)
