import React from "react"
import { Grid, Menu, Icon, Message, Button } from "semantic-ui-react"
import { Switch, NavLink, Route, Redirect, Link } from "react-router-dom"
import Posed, { PoseGroup } from "react-pose"
import { connect } from "react-redux"
import {
    fetchUser,
    fetchUserData,
    updateProfile,
    fetchCustomerServices,
    changeProfilePicture,
    fetchListUpgradeAccount
} from "../../state/actions/userActions"
import { fetchOrderHistory } from "../../state/actions/orderActions"
import { fetchCities, fetchProvinces, fetchSubdistricts } from "../../state/actions/rajaOngkirActions"
import Boundary from "../../common/Boundary"
import Wishlist from "./Wishlist"
import Basic from "./Basic"
import History from "./History"
import Deposit from "./Deposit"
import SettingsPage from "./SettingsPage"
import { mobile, idKoko } from "../../common"

const upgradeText = (
    <div>
        <p>
            Kamu udah mengajukan upgrade akun ke VIP, tapi kamu belum melakukan konfirmasi. <br /> Kalo kamu udah
            melakukan pembayaran, langsung konfirmasi aja ya.
        </p>
        <Link to="/upgrade/confirm">Konfirmasi sekarang</Link>
    </div>
)

const pendingText = (
    <div>
        <p>Kayaknya kamu masih ada tanggungan buat ngelunasin orderan kamu nih.</p>
        <Link to="/profile/history">Coba lihat</Link>
    </div>
)

const RouteContainer = Posed.div({
    enter: { opacity: 1, y: 0, delay: 200, beforeChildren: true },
    exit: { opacity: 0, y: 20 }
})

class Profile extends React.Component {
    state = {
        editing: false,
        userDetail: this.props.user,
        notice: true
    }

    componentDidMount() {
        this.props.fetchUser().then(() => {
            const { user } = this.props
            this.props.fetchSubdistricts(user && user.city)
        })
        this.props.fetchOrderHistory()
        this.props.fetchCustomerServices()
        this.props.fetchCities()
        this.props.fetchProvinces()
        this.props.fetchListUpgradeAccount()
    }

    renderProvinces = () => {
        const { provinces } = this.props

        const provinceList =
            provinces &&
            provinces.map(province => ({
                key: Number(province.province_id),
                value: Number(province.province_id),
                text: province.province
            }))

        return provinceList
    }

    renderCities = () => {
        const { cities } = this.props

        const cityList =
            cities &&
            cities.map(city => ({
                key: Number(city.city_id),
                value: Number(city.city_id),
                province: Number(city.province_id),
                zip: Number(city.postal_code),
                text: city.city_name
            }))

        return cityList
    }

    handlePopulateData = data => {
        this.props.fetchUserData(data)
    }

    handleRedirect = url => {
        this.props.history.push(url)
    }

    handleCloseNotice = () => this.setState({ notice: false })

    render() {
        const {
            loading,
            updateProfile,
            changeProfilePicture,
            orderHistory,
            loadingOrder,
            cs,
            userCity,
            location,
            match,
            csName,
            statusUpgrade,
            pending,
            akunKoko,
            subdistrict,
            error,
            userType
        } = this.props
        const { userDetail, notice } = this.state

        return (
            <React.Fragment>
                {statusUpgrade === 2 && notice && userType !== 2 && (
                    <Message
                        className="profile-notice"
                        icon="bell outline"
                        floating
                        info
                        compact
                        style={{ width: mobile ? "100%" : "60%" }}
                        content={upgradeText}
                        onDismiss={this.handleCloseNotice}
                    />
                )}
                {pending && notice && (
                    <Message
                        className="profile-notice"
                        icon="bell outline"
                        floating
                        info
                        compact={!mobile}
                        content={pendingText}
                        style={{ width: mobile ? "100%" : "60%" }}
                        onDismiss={this.handleCloseNotice}
                    />
                )}
                <Grid stackable padded>
                    <PoseGroup>
                        <Grid.Column as={RouteContainer} width={12} key={location.pathname}>
                            <Switch location={location}>
                                <Route
                                    path={`${match.url}/basic`}
                                    key="basic"
                                    render={({ match }) => (
                                        <Boundary>
                                            <Basic
                                                redirect={this.handleRedirect}
                                                match={match}
                                                cs={cs}
                                                csName={csName}
                                                user={this.props.user}
                                                provinces={this.renderProvinces()}
                                                cities={this.renderCities()}
                                                subdistrict={subdistrict}
                                                loading={loading}
                                                userCity={userCity}
                                                userData={userDetail}
                                                statusUpgrade={statusUpgrade}
                                                updateProfile={updateProfile}
                                                onChangeProfilePicture={changeProfilePicture}
                                                onFetchUser={this.props.fetchUser}
                                            />
                                        </Boundary>
                                    )}
                                />
                                <Route path={`${match.url}/wishlist`} key="wishlist" component={Wishlist} />
                                <Route
                                    path={`${match.url}/history`}
                                    key="history"
                                    render={() => (
                                        <Boundary>
                                            <History
                                                akunKoko={akunKoko}
                                                loading={loadingOrder}
                                                orderHistory={orderHistory}
                                                onFetchOrderHistory={this.props.fetchOrderHistory}
                                                error={error}
                                            />
                                        </Boundary>
                                    )}
                                />
                                <Route path={`${match.url}/deposit`} key="deposit" component={Deposit} />
                                {/* <Route path={`${match.url}/refund`} key="refund" component={Refund} /> */}
                                <Route path={`${match.url}/settings`} key="settings" component={SettingsPage} />
                                <Redirect exact from="/profile" to={`${match.url}/basic`} />
                            </Switch>
                        </Grid.Column>
                    </PoseGroup>
                    <Grid.Column width={4}>
                        <Menu fluid={mobile ? true : false} text vertical>
                            <Menu.Item header>
                                <Icon name="user outline" /> Profil ku
                            </Menu.Item>
                            <Menu.Item as={NavLink} to="/profile/basic">
                                Basic
                            </Menu.Item>
                            <Menu.Item as={NavLink} to="/profile/wishlist">
                                Wishlist
                            </Menu.Item>
                            <Menu.Item as={NavLink} to="/profile/history">
                                Histori Pemesanan
                            </Menu.Item>
                            {!akunKoko && (
                                <Menu.Item as={NavLink} to="/profile/deposit">
                                    Deposit
                                </Menu.Item>
                            )}
                            {/* <Menu.Item as={NavLink} to="/profile/refund">
                                Refund
                            </Menu.Item> */}
                            <Menu.Item as={NavLink} to="/profile/settings">
                                Settings
                            </Menu.Item>
                        </Menu>
                    </Grid.Column>
                </Grid>
            </React.Fragment>
        )
    }
}

const mapState = ({ user, order, rajaOngkir }) => {
    const csName =
        user.user &&
        user.user.acc_type !== 3 &&
        user.cs &&
        user.cs.filter(cs => cs.id === user.user.customer_service_id).map(item => item.name)[0]
    const userCity = user.user && user.user.city
    const userSubdistrict = user.user && user.user.subdistrict
    const subdistricts = rajaOngkir.subdistricts && rajaOngkir.subdistricts
    const subdistrict =
        subdistricts &&
        subdistricts
            .filter(city => Number(city.subdistrict_id) === userSubdistrict)
            .map(city => city.subdistrict_name)[0]
    const statusUpgrade = user.list && user.list.status_id
    const pending = order.orderHistory && order.orderHistory.some(item => item.status_order_id === 1)
    const orderHistory = order.orderHistory && order.orderHistory.sort((a, b) => b.order_id - a.order_id)
    const akunKoko = user.user && user.user.id === idKoko

    return {
        orderHistory,
        user: user.user,
        userType: user.user && user.user.acc_type,
        loading: user.loading,
        loadingOrder: order.loading,
        cs: user.cs,
        cities: rajaOngkir.cities,
        provinces: rajaOngkir.provinces,
        list: user.list,
        error: order.error,
        statusUpgrade,
        pending,
        csName,
        akunKoko,
        subdistrict,
        subdistricts,
        userCity
    }
}

const actionList = {
    fetchUser,
    fetchUserData,
    updateProfile,
    changeProfilePicture,
    fetchCities,
    fetchProvinces,
    fetchOrderHistory,
    fetchCustomerServices,
    fetchListUpgradeAccount,
    fetchSubdistricts
}

export default connect(
    mapState,
    actionList
)(Profile)
