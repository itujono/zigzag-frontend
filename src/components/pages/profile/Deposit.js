import React from "react"
import { Grid, Form, Header, Container, Button, Dropdown, Feed, Segment } from "semantic-ui-react"
import { Field, reduxForm } from "redux-form"
import { connect } from "react-redux"
import { saveDeposit, fetchDeposits } from "../../state/actions/depositActions"
import { fetchUser } from "../../state/actions/userActions"
import TextInput from "../../form/TextInput"
import DepositHistory from "./DepositHistory"
import { formatter, PosedContainer, Div, mobile } from "../../common"

const filters = [
    { key: 1, value: 1, text: "pending" },
    { key: 2, value: 2, text: "ditolak" },
    { key: 3, value: 3, text: "diterima" },
    { key: 4, value: 4, text: "dipake belanja" },
    { key: 5, value: 5, text: "semua" }
]

class Deposit extends React.Component {
    state = { edit: false, sent: false, value: 5 }

    componentDidMount() {
        this.props.fetchDeposits()
        this.props.fetchUser()
    }

    handleEditForm = () => this.setState({ edit: true })

    handlePostDeposit = data => {
        if (data) {
            this.props.saveDeposit(data, () => this.props.history.push("/deposit-sent"))
        }
    }

    handleFilterList = (e, { value }) => this.setState({ value })

    render() {
        const { handleSubmit, loading, deposits, total, user } = this.props
        const { edit, value } = this.state

        return (
            <Grid stackable className="profile-deposit">
                <Grid.Column as={PosedContainer}>
                    <Header as="h2" className="form-header">
                        <Header.Content as={Div}>
                            Deposit Page
                            <Header.Subheader>Data deposit kamu di Zigzag.</Header.Subheader>
                        </Header.Content>
                    </Header>
                    <Segment basic as={Div}>
                        <Header as="h3">
                            {user && user.deposit === 0
                                ? "Deposit kamu saat ini masih "
                                : "Deposit kamu saat ini sebesar"}{" "}
                            <span className="total">{formatter.format(user && user.deposit)}</span>
                        </Header>
                        {!edit && (
                            <Button
                                icon="plus"
                                className="btn-zigzag"
                                disabled={loading}
                                content="Tambah deposit"
                                onClick={this.handleEditForm}
                            />
                        )}
                        {edit && (
                            <React.Fragment>
                                <Form loading={loading} onSubmit={handleSubmit(this.handlePostDeposit)}>
                                    <Field
                                        name="total"
                                        type="text"
                                        placeholder="Minimal Rp 50.000,00"
                                        label="Jumlah deposit"
                                        component={TextInput}
                                        width={mobile ? 16 : 8}
                                    />
                                    <Button type="submit" icon="plus" className="btn-zigzag" content="Deposit" />
                                    <Button
                                        basic
                                        content="Batal"
                                        type="button"
                                        className="link-btn"
                                        onClick={() => this.setState({ edit: false })}
                                    />
                                </Form>
                                <Segment basic className="disclaimer">
                                    1. Setelah kamu berhasil menginput jumlah deposit yang diinginkan, silakan lakukan
                                    pembayaran ke nomor rekening yang diinginkan, dan kemudian kami akan segera
                                    melakukan proses verifikasi data.
                                </Segment>
                            </React.Fragment>
                        )}
                    </Segment>
                    <Grid stackable>
                        <Grid.Column as={Div} floated="left" width={6}>
                            <Header as="h4" content="Histori Deposit Kamu" />
                        </Grid.Column>
                        <Grid.Column as={Div} floated="right" textAlign="right" width={6}>
                            {deposits && deposits.length > 0 && (
                                <Header as="h4">
                                    {value === 5 ? "Tampilkan" : "Tampilkan yang"} &nbsp;
                                    <Dropdown
                                        inline
                                        options={filters}
                                        onChange={this.handleFilterList}
                                        defaultValue={filters[4].value}
                                    />
                                </Header>
                            )}
                        </Grid.Column>
                        <Container>
                            <DepositHistory
                                deposits={deposits}
                                value={value}
                                filters={filters}
                                total={total}
                                loading={loading}
                            />
                        </Container>
                    </Grid>
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.total) errors.total = "Wajib diisi ya, jangan sampe kosong"

    return errors
}

const mapState = ({ deposit, user }) => ({
    loading: deposit.loading,
    deposits: deposit.deposits && deposit.deposits.sort((a, b) => b.id - a.id),
    total: deposit.total,
    user: user.user
})

export default reduxForm({ form: "Deposit", validate })(
    connect(
        mapState,
        { saveDeposit, fetchDeposits, fetchUser }
    )(Deposit)
)
