import React from "react"
import { Grid, Feed, Header, Step } from "semantic-ui-react"
import Emoji from "../../common/Emoji"
import NoData from "../../common/NoData"

const Status = ({ awb, expeditionCode }) => (
    <Grid columns={1} className="status">
        <Grid.Column>
            <Header content="Status pesanan kamu sekarang" />
            {awb && awb.result && awb.result !== null ? (
                awb.result.manifest.map(item => (
                    <Feed key={item.manifest_description}>
                        <Feed.Event>
                            {/* <Feed.Label><Emoji label="lighting" symbol="😲" /></Feed.Label> */}
                            <Feed.Label image="https://react.semantic-ui.com/images/avatar/small/elliot.jpg" />
                            <Feed.Content>
                                <Feed.Summary>
                                    {/* Nyampe pos{" "} */}
                                    <Feed.Date>
                                        {item.manifest_date} - {item.manifest_time}
                                    </Feed.Date>
                                </Feed.Summary>
                                <Feed.Extra text>{item.manifest_description}</Feed.Extra>
                            </Feed.Content>
                        </Feed.Event>
                    </Feed>
                ))
            ) : (
                <NoData
                    basic
                    textAlign="left"
                    message="Nomor resi kamu belum tersedia ya. Silakan coba beberapa saat lagi."
                />
            )}
        </Grid.Column>
    </Grid>
)

export default Status
