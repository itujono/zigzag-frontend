import React from "react"
import { List, Header, Button, Grid, Form, Container, Transition, Segment, Tab, Icon } from "semantic-ui-react"
import moment from "moment"
import TableHistory from "./TableHistory"
import { tanggal, formatter } from "../../common"
import Prompt from "../../common/Prompt"
import Tag from "../../common/Tag"
import NoData from "../../common/NoData"
import Status from "./Status"

const teleCensored = tele => {
    const teleLength = tele.length
    const teleItem = tele && tele.slice(0, teleLength - 3)
    let teleLastThree = tele && tele.slice(-3)
    teleLastThree = "xxx"

    return teleItem + teleLastThree
}

const HistoryList = ({
    handleShowPrompt,
    handleClosePrompt,
    selectOrderItem,
    states: { showForm, prompt, reason, value, orderItem, page },
    data: { awb, status, orderHistory, expeditionCode, orderSymbol, akunKoko },
    handleChangeForm,
    handleShowForm,
    handleCloseForm,
    handleSubmitForm,
    onChangePage,
    onSaveOrderCode
}) => {
    if (orderHistory && orderHistory.length < 1) {
        return <NoData message="Kamu belum ada ngelakuin orderan sama sekali" image />
    }

    return (
        <React.Fragment>
            <List divided verticalAlign="middle" relaxed="very" size="large" className="history-list">
                {orderHistory &&
                    orderHistory
                        .filter(item => {
                            if (value === "all") return [...orderHistory]
                            else return item.status_order_id === value
                        })
                        .map(item => {
                            let itemName = item.order_detail.map(item => item.product_name)
                            if (itemName.includes(".")) itemName.slice(0, -1)

                            const discount = item.discount
                            const ongkir = item.ekspedition_total
                            const subtotal = item.subtotal_order
                            const grandtotal = item.grandtotal_order
                            const weight = item.total_weight
                            const uniqueCode = item.unique_code

                            const itemLength = item.order_detail.length

                            let texty = ""

                            if (itemLength > 2)
                                texty = `${itemName[0]}, ${itemName[1]}, dan ${itemLength - 2} item lainnya`
                            else if (itemLength === 2) texty = `${itemName[0]}, dan ${itemName[1]}`
                            else texty = `${itemName[0]}`

                            const orderStatus = item.status_order_id

                            const twoHours = moment(item.created_date)
                                .add(2, "hours")
                                .calendar()

                            const address =
                                item.shipping_address === "Jemput di gudang"
                                    ? "Jemput di gudang"
                                    : `${item.shipping_address}, ${item.city_name}, ${item.subdistrict_name}, ${
                                          item.province_name
                                      }, ${item.zip}`

                            const panes = [
                                {
                                    menuItem: "Detail",
                                    render: () => (
                                        <Tab.Pane>
                                            <Grid columns={3}>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <Header
                                                            content="Metode pembayaran"
                                                            subheader={item.payment_method}
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Header
                                                            content="Status orderan"
                                                            subheader={item.status_order}
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Header
                                                            content="Nomor resi"
                                                            subheader={
                                                                !item.resi_order || item.resi_order === ""
                                                                    ? "Belum tersedia"
                                                                    : item.resi_order
                                                            }
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Header
                                                            content="Penerima"
                                                            subheader={`${item.name} - ${teleCensored(item.tele)}`}
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Header content="Alamat penerima" subheader={address} />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Header
                                                            content="Ekspedisi"
                                                            subheader={`${item.ekspedition_company} (${
                                                                item.ekspedition_remark
                                                            })`}
                                                        />
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </Tab.Pane>
                                    )
                                },
                                {
                                    menuItem: "Items",
                                    render: () => (
                                        <Tab.Pane>
                                            <TableHistory
                                                otherDetails={{
                                                    ongkir,
                                                    discount,
                                                    subtotal,
                                                    grandtotal,
                                                    weight,
                                                    akunKoko,
                                                    uniqueCode
                                                }}
                                                orderDetail={item.order_detail}
                                            />
                                        </Tab.Pane>
                                    )
                                },
                                {
                                    menuItem: "Status barang",
                                    render: () => (
                                        <Tab.Pane>
                                            <Status awb={awb} expeditionCode={expeditionCode} />
                                        </Tab.Pane>
                                    )
                                }
                            ]

                            return (
                                <List.Item key={item.order_code}>
                                    <List.Content floated="right">
                                        <Button
                                            className="link-btn"
                                            basic
                                            icon={item.order_code === orderItem ? "chevron up" : "chevron down"}
                                            onClick={() =>
                                                selectOrderItem(
                                                    item.order_code,
                                                    item.resi_order,
                                                    expeditionCode(item.ekspedition_company)
                                                )
                                            }
                                            content={item.order_code === orderItem ? "Tutup detail" : "Detail"}
                                        />
                                    </List.Content>
                                    <List.Content>
                                        <List.Header> {texty} </List.Header>
                                        {orderSymbol &&
                                            orderSymbol
                                                .filter(symbol => symbol.status_order_id === item.status_order_id)
                                                .map(item => (
                                                    <Tag
                                                        key={item.status_order_id}
                                                        color={item.color}
                                                        text={item.status_order}
                                                        icon={item.icon}
                                                        className="status-order"
                                                    />
                                                ))[0]}
                                        <List.Description className="meta-title">
                                            <Container>
                                                <Icon name="file alternate outline" /> {item.order_code} <br />
                                                <Icon name="clock outline" />{" "}
                                                {tanggal(item.created_date, "dddd, DD MMMM YYYY - HH:mm")} (
                                                {moment(item.created_date).fromNow()}) <br />
                                                <Icon name="dollar sign" /> {formatter.format(item.grandtotal_order)}{" "}
                                                <br /> {item.order_code === orderItem && <br />}
                                                {orderStatus === 1 && (
                                                    <span>
                                                        Kalo belum melakukan pembayaran, orderan kamu akan dibatalkan{" "}
                                                        {twoHours.toLocaleLowerCase()} <br />
                                                    </span>
                                                )}{" "}
                                            </Container>
                                            {item.order_code === orderItem && <br />}

                                            <Transition
                                                as={Segment}
                                                basic
                                                visible={item.order_code === orderItem}
                                                animation="slide down"
                                                duration={250}
                                            >
                                                <Container>
                                                    <Tab menu={{ secondary: true }} panes={panes} />
                                                    {orderStatus === 1 && (
                                                        <Button
                                                            onClick={() => onSaveOrderCode(item)}
                                                            content="Konfirmasi pembayaran"
                                                            className="btn-zigzag"
                                                        />
                                                    )}{" "}
                                                    &nbsp;
                                                    {orderStatus > 0 && orderStatus < 4 && !showForm && (
                                                        <Button
                                                            basic
                                                            content={
                                                                orderStatus === 10
                                                                    ? "Order telah dibatalkan"
                                                                    : "Batalkan order"
                                                            }
                                                            disabled={orderStatus === 10}
                                                            icon={orderStatus === 10 ? null : "remove"}
                                                            onClick={handleShowForm}
                                                            className="link-btn"
                                                        />
                                                    )}
                                                    {showForm && (
                                                        <Segment basic className="cancel-order">
                                                            <Container style={{ marginBottom: "1.4em" }}>
                                                                <Header as="h4" content="Batalin orderan ini? 😲" />
                                                                Ya, kamu berhak kok ngebatalin orderan ini. Tapi ya
                                                                kenapa? <br /> Kamu harus tegas dong. Isi dulu deh
                                                                alasannya, baru ngomong lagi kita nya.
                                                            </Container>
                                                            <Form onSubmit={handleShowPrompt}>
                                                                <Form.TextArea
                                                                    rows={3}
                                                                    placeholder="Misalnya: Tas nya kurang greget"
                                                                    name="reason"
                                                                    value={reason}
                                                                    onChange={handleChangeForm}
                                                                />
                                                                <Button
                                                                    type="submit"
                                                                    content="Submit"
                                                                    icon="plus"
                                                                    disabled={!reason || reason.length < 3}
                                                                    className="btn-zigzag"
                                                                />
                                                                <Button
                                                                    content="Nggak jadi"
                                                                    className="link-btn"
                                                                    basic
                                                                    onClick={handleCloseForm}
                                                                />
                                                            </Form>
                                                            <Prompt
                                                                open={prompt}
                                                                onClose={handleClosePrompt}
                                                                header="Apa kamu yakin?"
                                                                yesText="Tentu saja"
                                                                confirm={() => handleSubmitForm(item.order_id)}
                                                            >
                                                                Kamu akan membatalkan orderan ini. Ini nggak bisa
                                                                di-undo. Apa kamu super yakin?
                                                            </Prompt>
                                                        </Segment>
                                                    )}
                                                </Container>
                                            </Transition>
                                        </List.Description>
                                    </List.Content>
                                </List.Item>
                            )
                        })}
            </List>
            {orderHistory && (
                <Segment basic textAlign="center">
                    <Button icon="chevron left" disabled={page < 2} basic onClick={() => onChangePage("prev")} />
                    <Button
                        icon="chevron right"
                        disabled={orderHistory.length < 10}
                        basic
                        onClick={() => onChangePage("next")}
                    />
                </Segment>
            )}
        </React.Fragment>
    )
}

export default HistoryList
