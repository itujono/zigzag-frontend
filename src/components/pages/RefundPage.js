import React from "react";
import { Grid, Segment, Form, Button, Input, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import TextInput from "../../components/form/TextInput";
import TextArea from "../../components/form/TextArea";
import DateInput from "../../components/form/DateInput";



class RefundPage extends React.Component {
    render() {
        return (
            <Grid centered columns={3} className="centered-grid main-form">
                <Grid.Column>
                    <Header as="h2" className="form-header">
                        <Header.Content>
                            Request refund
                            <Header.Subheader>
                                Jadi, kamu merasa ada yang janggal dengan barang dari kami? Refund saja!
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    <Form>
                        <Field
                            name="kode"
                            type="text"
                            placeholder="Masukkan kode order kamu..."
                            component={TextInput}
                            label="Kode Order"
                        />
                        <Field
                            name="date"
                            type="text"
                            placeholder="Pilih tanggal pembelian"
                            component={DateInput}
                            dateFormat="dddd, DD MMMM YYYY"
                            label="Tanggal Pembelian"
                        />
                        <Field
                            name="reason"
                            type="text"
                            placeholder="Jelaskan kenapa kamu ingin refund orderan ini..."
                            component={TextArea}
                            label="Alasan Refund"
                        />
                        <Button type="submit" className="btn-zigzag mt2em" content="Submit" fluid />
                    </Form>
                    <div className="switching">
                        <Button to="/profile/refund" as={Link} basic className="link-btn">Batal</Button>
                    </div>   
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.kode) errors.kode = "Kode order nya kok kosong?"
    if (!values.date) errors.date = "Tanggal nya kok gak dipilih?"
    if (!values.reason) errors.reason = "Alasan nya kok kosong? Ini paling penting, loh."

    return errors
}



export default reduxForm({ form: 'RefundForm', validate })(RefundPage)