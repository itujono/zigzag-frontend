import React from "react"
import { Grid, Segment, Header, Image, Container, Button, Radio, Responsive } from "semantic-ui-react"
import Prompt from "../../common/Prompt"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { formatter, PosedContainer, Div, tablet } from "../../common"
import { fetchDeposits } from "../../state/actions/depositActions"
import { fetchTempData, saveTempData } from "../../state/actions/orderActions"
import bca from "../../../images/bca.jpg"
import mandiri from "../../../images/mandiri.png"
import Emoji from "../../common/Emoji"
import CsSegment from "../../common/CsSegment"

class Payment extends React.Component {
    state = { selectedMethod: "transfer", prompt: false }

    componentDidMount() {
        this.props.fetchDeposits()

        const temp = localStorage.getItem("temp")

        if (!temp || temp === undefined || Object.keys(temp).length < 1) {
            this.props.history.push("/checkout")
        }
    }

    handleSelect = selectedMethod => {
        this.setState({ selectedMethod })
    }

    handleToNextPage = () => {
        const additional = { ...this.props.temp, payment_method: this.state.selectedMethod }

        if (this.state.selectedMethod === "deposit") {
            this.setState({ prompt: true })
        } else {
            this.props.saveTempData(additional)
            this.props.history.push("/checkout/summary")
        }
    }

    handleConfirmDeposit = () => {
        const additional = { ...this.props.temp, payment_method: this.state.selectedMethod }

        this.props.saveTempData(additional)
        this.props.history.push("/checkout/summary")
    }

    render() {
        const {
            totalCart,
            user,
            temp,
            csData: { cs, csState, onExpandCs }
        } = this.props
        const { selectedMethod } = this.state
        const ongkir = (temp && temp.ekspedition_total) || 0
        const userDeposit = user && user.deposit
        const totalOrder = totalCart !== 0 && totalCart + ongkir

        const textTransfer = (
            <div className="info">
                <div className="bank-list">
                    <Image.Group size="tiny">
                        <Image src={bca} style={{ marginRight: "1em" }} />
                        <Image src={mandiri} style={{ marginRight: "1em" }} />
                    </Image.Group>
                </div>
                Kamu memilih untuk menggunakan transfer bank:
                <ul>
                    <li>Hanya bank-bank di atas yang saat ini kami supoort</li>
                    <li>Nomor rekening akan diberitahukan di email dan di akhir proses order ini.</li>
                    <li>Dapat pahala dan amal jariyah</li>
                </ul>
            </div>
        )

        const textDeposit = (
            <div className="info">
                {userDeposit === 0 ? (
                    "Kamu belum punya balance deposit sama sekali "
                ) : (
                    <p>
                        Jumlah deposit di akun kamu sekarang adalah{" "}
                        <span className="tosca">{formatter.format(userDeposit)}</span>
                    </p>
                )}{" "}
                &nbsp;
                <Emoji label="angel" symbol="😇" />
                {userDeposit < totalOrder && (
                    <div className="info-insufficient">
                        <p>
                            {" "}
                            Well, karena jumlah deposit di akun kamu kurang, kamu harus tambah dulu deposit nya
                            sekarang.{" "}
                        </p>
                        <Button
                            icon="plus"
                            content="Tambah deposit"
                            className="btn-zigzag"
                            onClick={() => this.props.history.push("/deposit")}
                        />
                    </div>
                )}
            </div>
        )

        return (
            <Grid as={PosedContainer} stackable>
                <Grid.Column width={4}>
                    <CsSegment cs={cs} csState={csState} onExpandCs={onExpandCs} />
                </Grid.Column>
                <Grid.Column width={tablet ? 12 : 8} className="payment-method">
                    <Header as="h2">
                        <Header.Content as={Div}>
                            Metode Pembayaran
                            <Header.Subheader>
                                Silakan pilih metode pembayaran yang ingin kamu gunakan. Pilih lah yang paling familiar.
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    <Segment as={Div} padded="very" onClick={() => this.handleSelect("transfer")}>
                        <Radio
                            label="ATM / Transfer Bank"
                            onChange={this.handleSelect}
                            checked={selectedMethod === "transfer"}
                        />
                        {selectedMethod === "transfer" ? textTransfer : null}
                    </Segment>
                    <Segment as={Div} padded="very" onClick={() => this.handleSelect("deposit")}>
                        <Radio
                            label="Dari Deposit Kamu"
                            onChange={this.handleSelect}
                            checked={selectedMethod === "deposit"}
                        />
                        {selectedMethod === "deposit" ? textDeposit : null}
                    </Segment>
                    <Responsive minWidth={760}>
                        <Container as={Div} textAlign="right">
                            <Button
                                type="button"
                                basic
                                content="Kembali"
                                icon="angle left"
                                className="link-btn"
                                onClick={this.props.history.goBack}
                            />
                            <Button
                                content="Lanjut ke Summary"
                                className="btn-zigzag"
                                labelPosition="right"
                                icon="angle right"
                                disabled={selectedMethod === "deposit" && userDeposit < totalCart}
                                onClick={this.handleToNextPage}
                            />
                        </Container>
                    </Responsive>
                    <Responsive {...Responsive.onlyMobile}>
                        <Container as={Div} textAlign="right">
                            <Button
                                fluid
                                content="Lanjut ke Summary"
                                className="btn-zigzag"
                                labelPosition="right"
                                icon="angle right"
                                disabled={selectedMethod === "deposit" && userDeposit < totalCart}
                                onClick={this.handleToNextPage}
                            />{" "}
                            <br />
                            <Button
                                type="button"
                                fluid
                                basic
                                content="Kembali"
                                icon="angle left"
                                className="link-btn"
                                onClick={this.props.history.goBack}
                            />
                        </Container>
                    </Responsive>
                </Grid.Column>
                {this.state.prompt && (
                    <Prompt
                        open={this.state.prompt}
                        header="Apa kamu yakin?"
                        yesText="Pastinya"
                        onClose={() => this.setState({ prompt: false })}
                        confirm={this.handleConfirmDeposit}
                    >
                        Kamu milih menggunakan dana dari Deposit kamu. Dana di Deposit kamu akan dikurangin sebesar{" "}
                        <strong>{formatter.format(totalOrder)}</strong>. Apa kamu yakin?
                    </Prompt>
                )}
            </Grid>
        )
    }
}

const mapState = ({ deposit, order }) => {
    const localTemp = localStorage.getItem("temp")
    const temp = order.temp && Object.keys(order.temp).length > 0 ? order.temp : JSON.parse(localTemp)

    return {
        totalDeposit: deposit.total,
        temp
    }
}

export default withRouter(
    connect(
        mapState,
        { fetchDeposits, fetchTempData, saveTempData }
    )(Payment)
)
