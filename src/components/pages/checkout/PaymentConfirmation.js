import React from "react"
import { Link } from "react-router-dom"
import { Field, reduxForm } from "redux-form"
import TextInput from "../../form/TextInput"
import SelectInput from "../../form/SelectInput"
import DateInput from "../../form/DateInput"
import { banks, tanggal, mobile } from "../../common"
import {
    fetchOrderHistory,
    confirmOrder,
    saveOrderCode,
    fetchBankAccount,
    fetchOrderCode
} from "../../state/actions/orderActions"
import { Grid, Segment, Form, Button, Header, Checkbox, Container, Image, Message, Dropdown } from "semantic-ui-react"
import { connect } from "react-redux"
import moment from "moment"

class PaymentConfirmation extends React.Component {
    state = { received: false, file: null, imagePreview: "", order_code: this.props.orderCode }

    componentDidMount() {
        this.props.fetchOrderHistory()
        this.props.fetchBankAccount()
        this.props.fetchOrderCode()
    }

    handleImageChange = e => {
        this.setState({ file: e.target.files[0] })
    }

    handleSubmitForm = data => {
        const { file, order_code } = this.state
        let formData = new FormData()
        formData.append("evidence", file)
        formData.append("order_code", order_code)
        formData.append("bank_sender", data.bank_sender)
        formData.append("bank_receiver", data.bank_receiver)
        formData.append("total_transfer", data.total_transfer)
        formData.append("date", moment(data.date).format("Y-M-DD"))

        if (formData && formData !== null) {
            this.props.confirmOrder(formData, () => {
                this.setState({ received: true })
            })
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    render() {
        const { handleSubmit, loading, orderCode, error, bankOptions, historyOptions } = this.props
        const { file, imagePreview } = this.state

        const noOptions = (
            <span>
                Jika tidak ada pilihan kode order, kemungkinan kamu nggak punya orderan yang perlu dikonfirmasi. Coba
                silakan cek ke <Link to="/profile/history">History Pemesanan</Link>
            </span>
        )

        const err = error() && error().map(item => <li key={item}>{item}</li>)

        if (this.state.received) {
            return (
                <Grid stackable centered columns={mobile ? 1 : 3} className="centered-grid no-border">
                    <Grid.Column>
                        <Segment padded="very" centered className="wizard-success">
                            <svg
                                width="133px"
                                height="133px"
                                viewBox="0 0 133 133"
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                style={{ marginBottom: "2.5em" }}
                            >
                                <g id="check-group" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <circle id="filled-circle" fill="#8732ff" cx="66.5" cy="66.5" r="54.5" />
                                    <circle id="white-circle" fill="#FFFFFF" cx="66.5" cy="66.5" r="55.5" />
                                    <circle
                                        id="outline"
                                        stroke="#8732ff"
                                        strokeWidth="4"
                                        cx="66.5"
                                        cy="66.5"
                                        r="54.5"
                                    />
                                    <polyline id="check" stroke="#FFFFFF" strokeWidth="4" points="41 70 56 85 92 49" />
                                </g>
                            </svg>
                            <Container textAlign="center">
                                <Header as="h2" content="Konfirmasi pembayaran selesai! 😘" />
                                <p className="mb2em">
                                    Great! Konfirmasi pembayaran kamu telah kami terima, dan email berisi detail info
                                    orderan juga sudah ada di inbox email kamu. Silakan duduk tenang, dan kami akan
                                    melakukan verifikasi terhadap konfirmasi pembayaran kamu. Tunggu notifikasi
                                    berikutnya dari kami. Have a great day! <br />
                                    <br /> P.S. Kalo emailnya gak ada, jangan lupa cek folder spam nya juga ya.
                                </p>
                                <Button
                                    className="btn-zigzag"
                                    content="Kembali ke Home"
                                    onClick={() => window.location.replace("/")}
                                />
                            </Container>
                        </Segment>
                    </Grid.Column>
                </Grid>
            )
        }

        return (
            <Grid stackable centered columns={2} className="centered-grid main-form">
                <Grid.Column>
                    <Header as="h2" className="form-header">
                        <Header.Content>
                            Konfirmasi Pembayaran
                            <Header.Subheader>
                                Silakan masukkan data-data di bawah ini, agar orderan kamu bisa segera diproses.
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    {historyOptions && historyOptions.length < 1 && (
                        <Message
                            style={{ marginTop: "1.5em" }}
                            className="message-info"
                            color="green"
                            content={noOptions}
                            size="small"
                        />
                    )}
                    <Form
                        loading={loading}
                        encType="multipart/form-data"
                        onSubmit={handleSubmit(this.handleSubmitForm)}
                    >
                        <Form.Field>
                            <label>Kode order</label>
                            <Dropdown
                                name="order_code"
                                placeholder="Masukkan kode order kamu..."
                                options={historyOptions}
                                label="Kode order"
                                item={orderCode}
                                onChange={this.handleChange}
                                defaultValue={orderCode}
                                noResultsMessage="Tidak ada kode order"
                                fluid
                                search
                                selection
                            />
                        </Form.Field>
                        <Field
                            name="bank_sender"
                            type="text"
                            placeholder="Nama bank dan nomor rekening yang kamu gunakan sebagai pengirim"
                            component={TextInput}
                            label="Bank dan nomor rekening kamu"
                        />
                        <Field
                            name="bank_receiver"
                            type="text"
                            placeholder="Nomor rekening Zigzag yang kamu transfer"
                            component={SelectInput}
                            options={bankOptions}
                            label="Nomor rekening yang dituju"
                            search
                            selection
                        />
                        <Field
                            name="total_transfer"
                            type="text"
                            placeholder="Tidak perlu pakai titik/koma. Benar: 50000 - Salah: 50.000"
                            component={TextInput}
                            label="Nominal yang ditransfer (Rp)"
                        />
                        <Field
                            name="date"
                            todayButton="Hari ini"
                            type="text"
                            placeholder="Ditransfer pada tanggal..."
                            component={DateInput}
                            label="Tanggal transfer"
                        />
                        <Form.Field>
                            <label>Upload Bukti</label>

                            {file && imagePreview !== "" ? (
                                <React.Fragment>
                                    <Image src={imagePreview} size="medium" style={{ marginBottom: "1em" }} />
                                    <Button
                                        icon="picture"
                                        className="btn-zigzag"
                                        content="Ganti"
                                        onClick={() => this.setState({ file: null, imagePreview: "" })}
                                    />
                                </React.Fragment>
                            ) : (
                                <div className="upload-btn-wrapper edit-img-wrapper wizard">
                                    <input
                                        type="file"
                                        name="evidence"
                                        style={{ height: "100%", cursor: "pointer", zIndex: 1000 }}
                                        // accept=".png, .jpg, .jpeg"
                                        onChange={this.handleImageChange}
                                    />
                                </div>
                            )}
                        </Form.Field>

                        {err && err.length > 0 && (
                            <Message style={{ display: "block" }} error header="Wah!" content={err} />
                        )}

                        <Button
                            type="submit"
                            loading={loading}
                            disabled={loading}
                            content="Submit"
                            fluid
                            className="mt2em btn-zigzag"
                        />
                    </Form>
                    <div className="switching">
                        <Button
                            basic
                            icon="remove"
                            onClick={this.props.history.goBack}
                            className="link-btn mb2em"
                            content="Batal"
                        />
                    </div>
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.order_code) errors.order_code = "Jangan sampe kosong ya"
    if (!values.bank_sender) errors.bank_sender = "Eits, ini kok kosong?"
    if (!values.bank_number_sender) errors.bank_number_sender = "Nomer rekening nya kok kosong?"
    if (!values.bank_receiver) errors.bank_receiver = "Ini juga wajib loh ya"
    if (!values.total_transfer) errors.total_transfer = "Total transfer nya kok kosong?"
    else if (values.total_transfer.includes(".") || values.total_transfer.includes(","))
        errors.total_transfer = "Gak perlu pake titik atau koma ya"
    if (!values.date) errors.date = "Tanggal nya kenapa? Kok gak diisi?"

    return errors
}

const actionList = { fetchOrderHistory, confirmOrder, saveOrderCode, fetchBankAccount, fetchOrderCode }

const mapState = ({ order }) => {
    const error = () => {
        let err = order.error
        if (err) {
            for (let i in err) {
                return err[i]
            }
        }
    }
    const orderHistory = order.orderHistory
    const orderHistorySorted =
        orderHistory &&
        orderHistory.map(item => ({
            ...item,
            date: Number(
                item.created_date
                    .split(" ")[0]
                    .split("-")
                    .join("")
            )
        }))
    const historyOptions =
        orderHistorySorted &&
        orderHistorySorted
            .filter(item => item.status_order_id === 1)
            .sort((a, b) => b.date - a.date)
            .map(item => ({
                key: item.order_code,
                value: item.order_code,
                text: item.order_code,
                description: tanggal(item.created_date, "dddd, DD MMMM YYYY")
            }))
    const bankOptions =
        order.bankAccount &&
        order.bankAccount.map(item => ({
            key: item.id,
            text: item.bank_account + " " + item.number + " " + "a.n." + " " + item.under_name,
            value: item.bank_account + " " + item.number
        }))

    return {
        orderCode: order.orderCode,
        loading: order.loading,
        bankAccount: order.bankAccount,
        bankOptions,
        historyOptions,
        orderHistory,
        error
    }
}

export default reduxForm({
    form: "PaymentConfirmation",
    validate,
    destroyOnUnmount: false,
    enableReinitialize: true
})(
    connect(
        mapState,
        actionList
    )(PaymentConfirmation)
)
