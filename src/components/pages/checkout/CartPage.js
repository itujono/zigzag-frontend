import React from "react"
import {
    Header,
    Table,
    Image,
    Dropdown,
    Button,
    Container,
    Grid,
    Popup,
    Responsive,
    Item,
    List
} from "semantic-ui-react"
import NoData from "../../common/NoData"
import Emoji from "../../common/Emoji"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import Posed from "react-pose"
import { fetchCarts, updateCartItem, getTotalCarts, deleteCart } from "../../state/actions/cartActions"
import { formatter, imageURL, priceType, mobile, idKoko, tablet } from "../../common"
import Timeout from "../../common/Timeout"

const { promo, vip, member, partner } = priceType

const TableBody = Posed.tbody({
    enter: { staggerChildren: 100 },
    exit: { staggerChildren: 100, transition: { duration: 150 } }
})

const TableItem = Posed.tr({
    enter: { opacity: 1 },
    exit: { opacity: 0.4 }
})

class CartPage extends React.Component {
    state = { actualPrice: 0, portal: false }

    componentDidMount() {
        if (this.props.user) this.props.fetchCarts()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.total !== this.props.total) {
            this.props.fetchCarts()
        }
    }

    handleDeleteCart = cartId => {
        this.props.deleteCart(cartId)
    }

    handleChangeQuantity = (e, data) => {
        const { user } = this.props
        const selected = this.props.carts.filter(cart => cart.id === data.id)[0]
        const selectedPrice = JSON.parse(selected.price)
        const qty = Number(selected.qty)
        const productWeight = selected.weight / qty

        const actualPrice = selectedPrice
            .filter(price => {
                if (user) {
                    if (user.id === idKoko) return price.price_type === member
                    else {
                        if (user.acc_type === 1) return price.price_type === member
                        else if (user.acc_type === 2) return price.price_type === vip
                        else return price.price_type === partner
                    }
                } else return price.price_type === member
            })
            .map(price => price.price)[0]

        if (selected) {
            const dataKeren = {
                id: data.id,
                qty: data.value,
                weight: data.value * productWeight,
                total_price: data.value * actualPrice
            }

            this.props.updateCartItem(dataKeren)
        }
    }

    handleShowPortal = () => this.setState({ portal: true })
    handleHidePortal = () => this.setState({ portal: false })

    render() {
        const {
            user,
            carts,
            discount,
            weight,
            loadingCart: loading,
            initial: initialPrice,
            totaludin,
            dynamicOptions,
            error
        } = this.props
        const discountNumber = discount ? discount : 0
        const nully = carts && carts.some(cart => cart.total_price === null)
        const subTotal = nully ? initialPrice : totaludin
        const generalTotal = totaludin - discountNumber

        return (
            <Grid stackable centered className="cart-page">
                <Grid.Column width={tablet ? 16 : 12}>
                    <Timeout open={error && error === "timeout"} />
                    <Container>
                        <Header as="h2">
                            <Header.Content>
                                Keranjang belanja
                                <Header.Subheader>
                                    Ini item-item yang udah kamu tambahin ke keranjang belanja
                                </Header.Subheader>
                            </Header.Content>
                        </Header>

                        {carts && carts.length > 0 ? (
                            <React.Fragment>
                                <Responsive
                                    as={Table}
                                    minWidth={760}
                                    stackable
                                    padded="very"
                                    className={loading ? "loading" : null}
                                >
                                    <Table.Header>
                                        <Table.Row textAlign="center">
                                            <Table.HeaderCell singleLine textAlign="left">
                                                Item
                                            </Table.HeaderCell>
                                            <Table.HeaderCell>Harga</Table.HeaderCell>
                                            <Table.HeaderCell>Kuantitas</Table.HeaderCell>
                                            <Table.HeaderCell>Berat</Table.HeaderCell>
                                            <Table.HeaderCell>Total</Table.HeaderCell>
                                            <Table.HeaderCell>Actions</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>

                                    <TableBody pose={loading ? "exit" : "enter"}>
                                        {carts &&
                                            carts.map(cart => {
                                                const priceTemp = JSON.parse(cart.price)
                                                const picParsed = cart.picture && cart.picture
                                                const picture = imageURL + "/" + picParsed

                                                const promoPrice =
                                                    priceTemp &&
                                                    priceTemp
                                                        .filter(price => {
                                                            if (cart.promo && cart.promo === 1)
                                                                return price.price_type === promo
                                                        })
                                                        .map(price => price.price)[0]

                                                const priceAll =
                                                    cart &&
                                                    priceTemp
                                                        .filter(price => {
                                                            if (user) {
                                                                if (user.id === idKoko)
                                                                    return price.price_type === member
                                                                else {
                                                                    if (user.acc_type === 1)
                                                                        return price.price_type === member
                                                                    else if (user.acc_type === 2)
                                                                        return price.price_type === vip
                                                                    else return price.price_type === partner
                                                                }
                                                            } else return price.price_type === member
                                                        })
                                                        .map(price => price.price)[0]

                                                const actualPrice = promoPrice ? promoPrice : priceAll

                                                return (
                                                    <TableItem textAlign="center" key={cart.id}>
                                                        <Table.Cell textAlign="left" singleLine>
                                                            <Popup
                                                                trigger={
                                                                    <Header as="h5" image>
                                                                        <Image
                                                                            src={
                                                                                picture ||
                                                                                "http://source.unsplash.com/random/"
                                                                            }
                                                                            rounded
                                                                            size="medium"
                                                                        />
                                                                        <Header.Content>
                                                                            {cart.name}
                                                                            <Header.Subheader>
                                                                                Warna: {cart.color}
                                                                                {cart.size !== 0 && (
                                                                                    <p>Size: {cart.size}</p>
                                                                                )}
                                                                            </Header.Subheader>
                                                                        </Header.Content>
                                                                    </Header>
                                                                }
                                                                verticalOffset={-5}
                                                                wide="very"
                                                                className="product-detail-hover"
                                                            >
                                                                <Grid stackable>
                                                                    <Grid.Column width={6}>
                                                                        <Image src={picture} fluid />
                                                                    </Grid.Column>
                                                                    <Grid.Column width={10}>
                                                                        <Header as="h4" content={cart.name} />
                                                                        {cart.product_description}
                                                                    </Grid.Column>
                                                                </Grid>
                                                            </Popup>
                                                        </Table.Cell>
                                                        <Table.Cell textAlign={mobile ? "center" : ""} singleLine>
                                                            Rp {actualPrice}
                                                        </Table.Cell>
                                                        <Table.Cell textAlign="center">
                                                            <Dropdown
                                                                name={cart.name}
                                                                id={cart.id}
                                                                onChange={this.handleChangeQuantity}
                                                                selectOnBlur={false}
                                                                defaultValue={Number(cart.qty)}
                                                                options={dynamicOptions(cart.stock)}
                                                                selection
                                                                compact
                                                            />
                                                        </Table.Cell>
                                                        <Table.Cell textAlign="center">{cart.weight} gram</Table.Cell>
                                                        <Table.Cell textAlign="center">
                                                            Rp {Number(cart.qty) * actualPrice}
                                                            {/* Rp {cart.total_price !== null ? cart.total_price : actualPrice} */}
                                                        </Table.Cell>
                                                        <Table.Cell textAlign="center" singleLine>
                                                            <Button
                                                                onClick={() => this.handleDeleteCart(cart.id)}
                                                                content="Hapus"
                                                                icon="trash alternate outline"
                                                                basic
                                                                className="delete-btn"
                                                                size="small"
                                                            />
                                                        </Table.Cell>
                                                    </TableItem>
                                                )
                                            })}
                                    </TableBody>

                                    <Table.Footer>
                                        <Table.Row textAlign="center">
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell>
                                                <strong>Total berat</strong> <br />
                                                <Popup
                                                    inverted
                                                    trigger={
                                                        <span className="dashed">
                                                            {weight} gram / {weight / 1000} kg
                                                        </span>
                                                    }
                                                    content="Total berat di sini adalah hasil pembulatan, sesuai dengan ketentuan ekspedisi-ekspedisi yang kami gunakan"
                                                />
                                            </Table.HeaderCell>
                                            <Table.HeaderCell>Subtotal</Table.HeaderCell>
                                            <Table.HeaderCell>{formatter.format(totaludin)}</Table.HeaderCell>
                                        </Table.Row>
                                        <Table.Row textAlign="center">
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell>
                                                Discount&nbsp;{" "}
                                                {discountNumber !== 0 && <Emoji label="lightening" symbol="⚡" />}
                                            </Table.HeaderCell>
                                            <Table.HeaderCell>- {formatter.format(discountNumber)}</Table.HeaderCell>
                                        </Table.Row>
                                        <Table.Row textAlign="center">
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell />
                                            <Table.HeaderCell className="total">Estimasi Total</Table.HeaderCell>
                                            <Table.HeaderCell className="total">
                                                {formatter.format(generalTotal)}
                                            </Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Footer>
                                </Responsive>
                                <Responsive
                                    as={Grid.Row}
                                    columns={1}
                                    {...Responsive.onlyMobile}
                                    style={{ marginBottom: "1em", marginTop: "2em" }}
                                >
                                    <Grid.Column>
                                        <Item.Group divided unstackable>
                                            {carts &&
                                                carts.map(cart => {
                                                    const priceTemp = JSON.parse(cart.price)
                                                    const picParsed = cart.picture && cart.picture
                                                    const picture = imageURL + "/" + picParsed

                                                    const promoPrice =
                                                        priceTemp &&
                                                        priceTemp
                                                            .filter(price => {
                                                                if (cart.promo && cart.promo === 1)
                                                                    return price.price_type === promo
                                                            })
                                                            .map(price => price.price)[0]

                                                    const priceAll =
                                                        cart &&
                                                        priceTemp
                                                            .filter(price => {
                                                                if (user) {
                                                                    if (user.id === idKoko)
                                                                        return price.price_type === member
                                                                    else {
                                                                        if (user.acc_type === 1)
                                                                            return price.price_type === member
                                                                        else if (user.acc_type === 2)
                                                                            return price.price_type === vip
                                                                        else return price.price_type === partner
                                                                    }
                                                                } else return price.price_type === member
                                                            })
                                                            .map(price => price.price)[0]

                                                    const actualPrice = promoPrice ? promoPrice : priceAll
                                                    return (
                                                        <Item className="cart-item" key={cart.id}>
                                                            <Item.Image size="tiny" src={picture} />

                                                            <Item.Content>
                                                                <Item.Header>
                                                                    {cart.name} <br /> <span>Rp {actualPrice}</span>
                                                                </Item.Header>
                                                                <Item.Meta>{cart.product_description}</Item.Meta>
                                                                <Item.Extra>
                                                                    <Dropdown
                                                                        name={cart.name}
                                                                        id={cart.id}
                                                                        onChange={this.handleChangeQuantity}
                                                                        selectOnBlur={false}
                                                                        defaultValue={Number(cart.qty)}
                                                                        options={dynamicOptions(cart.stock)}
                                                                        style={{ marginBottom: "1em" }}
                                                                        selection
                                                                        compact
                                                                    />
                                                                    <p>
                                                                        Warna: <strong>{cart.color}</strong>
                                                                    </p>{" "}
                                                                    {cart && cart.size !== 0 && (
                                                                        <p>
                                                                            Size: <strong>{cart.size}</strong>
                                                                        </p>
                                                                    )}
                                                                    <p>
                                                                        Total berat: <strong>{cart.weight} gram</strong>
                                                                    </p>
                                                                    <p>
                                                                        Total:{" "}
                                                                        <strong>
                                                                            Rp {Number(cart.qty * actualPrice)}
                                                                        </strong>
                                                                    </p>
                                                                    <Button
                                                                        basic
                                                                        onClick={() => this.handleDeleteCart(cart.id)}
                                                                        content="Hapus"
                                                                        icon="trash alternate outline"
                                                                        className="delete-btn"
                                                                        size="small"
                                                                        color="red"
                                                                    />
                                                                </Item.Extra>
                                                            </Item.Content>
                                                        </Item>
                                                    )
                                                })}
                                        </Item.Group>
                                    </Grid.Column>
                                    <Grid.Row columns={1} style={{ marginBottom: "2em" }}>
                                        <Grid.Column>
                                            <List horizontal>
                                                <List.Item>
                                                    <List.Header content="Total berat" />
                                                    {weight} gram / {weight / 1000} kg
                                                </List.Item>
                                                <List.Item>
                                                    <List.Header content="Subtotal" />
                                                    {formatter.format(totaludin)}
                                                </List.Item>
                                                <List.Item>
                                                    <List.Header content={<span>Discount ⚡</span>} />
                                                    {discountNumber !== 0 ? formatter.format(discountNumber) : "-"}
                                                </List.Item>
                                            </List>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row columns={1} centered textAlign="center" style={{ marginBottom: "3em" }}>
                                        <Grid.Column>
                                            <Header as="h3" className="total">
                                                Estimasi Total
                                                <Header.Subheader>{formatter.format(generalTotal)}</Header.Subheader>
                                            </Header>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Responsive>
                            </React.Fragment>
                        ) : (
                            <NoData message="Belum ada apa-apanya nih di cart kamu" icon="shopping bag" image />
                        )}

                        <Responsive as={Container} minWidth={760} textAlign="right">
                            <Button
                                basic
                                content="Kembali"
                                className="link-btn"
                                icon="angle left"
                                onClick={this.props.history.goBack}
                            />
                            <Button
                                content="Lanjut ke Checkout"
                                className="btn-zigzag"
                                labelPosition="right"
                                icon="angle right"
                                onClick={() => this.props.history.push("/checkout")}
                                disabled={(carts && carts.length < 1) || loading}
                            />
                        </Responsive>
                        <Responsive {...Responsive.onlyMobile}>
                            <Button
                                fluid
                                content="Lanjut ke Checkout"
                                className="btn-zigzag"
                                labelPosition="right"
                                icon="angle right"
                                onClick={() => this.props.history.push("/checkout")}
                                disabled={(carts && carts.length < 1) || loading}
                            />{" "}
                            <br />
                            <Button
                                fluid
                                basic
                                content="Kembali"
                                className="link-btn"
                                icon="angle left"
                                onClick={this.props.history.goBack}
                            />
                        </Responsive>
                    </Container>

                    <Container className="disclaimer">
                        1. Harga <strong>Total</strong> di atas belum termasuk ongkos kirim, yang akan dikalkulasikan di
                        halaman selanjutnya.
                    </Container>
                </Grid.Column>
            </Grid>
        )
    }
}

const actionList = {
    fetchCarts,
    updateCartItem,
    deleteCart,
    getTotalCarts
}

// prettier-ignore
const mapState = ({ cart, user }) => {
    const qty = cart.carts && cart.carts.map(cart => Number(cart.qty)).reduce((acc, curr) => acc + curr, 0);
    const qtyDisc = cart.carts && cart.carts
        .filter(item => item.promo === 0)
        .map(cart => Number(cart.qty))
        .reduce((acc, curr) => acc + curr, 0);
    const reguler = user.user && user.user.acc_type === 1;

    const dynamicOptions = (stock) => {
        if (stock < 11) return Array.from({length: stock}, (v, i) => ({ key: i + 1, value: i + 1, text: i + 1 }))
        else {
            return Array.from({length: 50}, (v, i) => {
                if (i === 0) {
                    return { key: 1, value: 1, text: 1, selected: true }
                }

                return { key: i + 1, value: i + 1, text: i + 1 }
            })
        }
    }

    const total = cart.carts && cart.carts.map(item => item.total_price).reduce((acc, curr) => acc + curr, 0);
    const totalWeight = cart.carts && cart.carts.map(item => item.weight).reduce((acc, curr) => acc + curr, 0) / 1000;
    const tempWeight =
        totalWeight && totalWeight.toString().split(".")[1] && totalWeight.toString().split(".")[1].length > 1
        ? totalWeight.toString().split(".")[1].split("")[0]
        : totalWeight.toString().split(".")[1];
    const weight = tempWeight > 3 ? Math.ceil(Number(totalWeight)) * 1000 : Math.floor(Number(totalWeight)) * 1000;

    const discount = reguler && cart.carts && cart.carts
        .filter(item => item.promo === 0)
        .map(item => {
            if (qty && qty > 2 && qty < 10) return 10000 * qtyDisc;
            else if (qty && qty > 9 && qty < 51) return 20000 * qtyDisc;
            else return 0;
        })[0];

    return {
        total: cart.total,
        totaludin: total,
        carts: cart.carts,
        user: user.user,
        loadingCart: cart.loading,
        error: cart.error,
        weight: weight === 0 ? 1000 : weight,
        discount: discount ? discount : 0,
        qty,
        tempWeight,
        dynamicOptions
    };
};

// prettier-ignore
export default connect( mapState, actionList )(withRouter(CartPage));
