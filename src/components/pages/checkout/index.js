import React from "react"
import { Grid } from "semantic-ui-react"
import { connect } from "react-redux"
import { withRouter, Switch, Route, Redirect } from "react-router-dom"
import { fetchCarts, getTotalCarts } from "../../state/actions/cartActions"
import { fetchUserData, fetchUser, fetchCustomerServices } from "../../state/actions/userActions"
import { fetchBankAccount, saveTempExpedition } from "../../state/actions/orderActions"
import { fetchSubdistricts } from "../../state/actions/rajaOngkirActions"
import ShippingAddress from "./ShippingAddress"
import Payment from "./Payment"
import SummaryPage from "./Summary"
import OrderReceived from "./OrderReceived"
import TotalOrder from "./TotalOrder"
import Posed, { PoseGroup } from "react-pose"

const RoutesContainer = Posed.div({
    enter: { opacity: 1, delay: 150, beforeChildren: true },
    exit: { opacity: 0 }
})

class Checkout extends React.Component {
    state = { page: 2, value: 1, quantity: 1, cartId: 0, totalin: 0, shipping: false, cs: false, pickup: false }

    componentDidMount() {
        this.props.fetchCarts()
        this.props.getTotalCarts()
        this.props.fetchUser().then(() => {
            const { user } = this.props
            this.props.fetchSubdistricts(user && user.city)
        })
        this.props.fetchBankAccount()
        this.props.fetchCustomerServices()
    }

    nextPage = (page = 1) => {
        this.setState({ page: this.state.page + page })
    }

    prevPage = (page = 1) => {
        this.setState({ page: this.state.page - page })
    }

    handleShippingTrue = () => localStorage.setItem("shipping", true)

    handleChangeExpedition = (e, { value }) => this.setState({ value })

    handleExpandCs = () => this.setState(prevState => ({ cs: !prevState.cs }))

    handleSubdistrictOptions = city => this.props.fetchSubdistricts(city)

    handleTogglePickup = () => this.setState(prevState => ({ pickup: !prevState.pickup }))

    render() {
        const { page, value, shipping } = this.state
        const {
            carts,
            total,
            fetchUserData,
            user,
            location,
            discount,
            weight,
            bankAccount,
            cs,
            subtotal,
            subdistricts,
            expedition,
            loading,
            saveTempExpedition
        } = this.props

        return (
            <Grid stackable centered className="checkout">
                <PoseGroup>
                    <Grid.Column
                        as={RoutesContainer}
                        key={location.pathname}
                        width={page !== 2 && page !== 3 ? 10 : 16}
                    >
                        <Switch location={location}>
                            <Route
                                exact
                                path="/checkout/shipping"
                                key="shipping"
                                render={() => (
                                    <ShippingAddress
                                        fetchUserData={fetchUserData}
                                        value={value}
                                        discount={discount}
                                        weight={weight}
                                        shipping={shipping}
                                        subtotal={subtotal}
                                        subdistricts={subdistricts}
                                        onTogglePickup={this.handleTogglePickup}
                                        pickup={this.state.pickup}
                                        onSubdistrictOptions={this.handleSubdistrictOptions}
                                        handleShippingTrue={this.handleShippingTrue}
                                        handleChangeExpedition={this.handleChangeExpedition}
                                        csData={{ cs, csState: this.state.cs, onExpandCs: this.handleExpandCs }}
                                    />
                                )}
                            />
                            <Route
                                exact
                                path="/checkout/expedition"
                                key="expedition"
                                render={() => (
                                    <TotalOrder
                                        user={user}
                                        carts={carts}
                                        loading={loading}
                                        shipping={shipping}
                                        onSaveTempExpedition={saveTempExpedition}
                                        csData={{ cs, csState: this.state.cs, onExpandCs: this.handleExpandCs }}
                                    />
                                )}
                            />
                            <Route
                                exact
                                path="/checkout/payment"
                                key="payment"
                                render={() => (
                                    <Payment
                                        totalCart={total}
                                        user={user}
                                        csData={{ cs, csState: this.state.cs, onExpandCs: this.handleExpandCs }}
                                    />
                                )}
                            />
                            <Route
                                exact
                                path="/checkout/summary"
                                key="summary"
                                render={() => (
                                    <SummaryPage
                                        carts={carts}
                                        total={total}
                                        user={user}
                                        discount={discount}
                                        pickup={this.state.pickup}
                                        expedition={expedition}
                                        cs={cs}
                                        csData={{ cs, csState: this.state.cs, onExpandCs: this.handleExpandCs }}
                                    />
                                )}
                            />
                            <Route
                                exact
                                path="/checkout/order-received"
                                key="order-received"
                                render={() => <OrderReceived bankAccount={bankAccount} />}
                            />
                            <Redirect exact from="/checkout" to="/checkout/shipping" />
                        </Switch>
                    </Grid.Column>
                </PoseGroup>
            </Grid>
        )
    }
}

const mapState = ({ cart, user, order, rajaOngkir }) => {
    const qty = cart.carts && cart.carts.map(cart => Number(cart.qty)).reduce((acc, curr) => acc + curr, 0)
    const qtyDisc =
        cart.carts &&
        cart.carts
            .filter(item => item.promo === 0)
            .map(cart => Number(cart.qty))
            .reduce((acc, curr) => acc + curr, 0)
    const reguler = user.user && user.user.acc_type === 1
    const totalWeight = cart.carts && cart.carts.map(item => item.weight).reduce((acc, curr) => acc + curr, 0) / 1000
    const tempWeight =
        totalWeight &&
        totalWeight !== null &&
        totalWeight.toString().split(".")[1] &&
        totalWeight.toString().split(".")[1].length > 1
            ? totalWeight
                  .toString()
                  .split(".")[1]
                  .split("")[0]
            : totalWeight.toString().split(".")[1]
    const weight =
        totalWeight < 1
            ? 1000
            : tempWeight > 3
            ? Math.ceil(Number(totalWeight)) * 1000
            : Math.floor(Number(totalWeight)) * 1000
    const userCs = user.user && user.user.customer_service_id
    const cs =
        user.cs &&
        user.cs
            .filter(item => item.id === userCs)
            .map(item => ({ name: item.name, wa: item.whatsapp, line: item.line }))[0]
    const subdistricts = rajaOngkir.subdistricts && rajaOngkir.subdistricts

    // const subtotal =
    //     cart.carts &&
    //     cart.carts
    //         .map(item => ({ price: JSON.parse(item.price), qty: item.qty }))
    //         .flat()
    //         .map(item => {
    //             const pricey = item.price
    //                 .filter(price => {
    //                     if (user.user) {
    //                         if (user.user.id === idKoko) return price.price_type === member
    //                         else {
    //                             if (user.user.acc_type === 1) return price.price_type === member
    //                             else if (user.user.acc_type === 2) return price.price_type === vip
    //                             else return price.price_type === partner
    //                         }
    //                     } else return price.price_type === member
    //                 })
    //                 .map(item => item.price)
    //             return pricey * item.qty
    //         })
    //         .reduce((acc, curr) => acc + curr, 0)

    const subtotal = cart.carts && cart.carts.map(item => item.total_price).reduce((acc, curr) => acc + curr, 0)

    const discount =
        reguler &&
        cart.carts &&
        cart.carts
            .filter(item => item.promo === 0)
            .map(item => {
                if (qty && qty > 2 && qty < 10) return 10000 * qtyDisc
                else if (qty && qty > 9 && qty < 51) return 20000 * qtyDisc
                else return 0
            })[0]

    return {
        total: cart.total,
        carts: cart.carts,
        user: user.user,
        bankAccount: order.bankAccount,
        expedition: order.expedition,
        discount: discount ? discount : 0,
        loading: order.loading,
        subtotal,
        tempWeight,
        totalWeight,
        weight,
        subdistricts,
        cs
    }
}

const actionList = {
    fetchCarts,
    getTotalCarts,
    fetchUserData,
    fetchUser,
    fetchBankAccount,
    fetchCustomerServices,
    fetchSubdistricts,
    saveTempExpedition
}

export default connect(
    mapState,
    actionList
)(withRouter(Checkout))
