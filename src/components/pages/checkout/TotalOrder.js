import React from "react"
import { Grid, Header, Segment, Divider, Button, Container, Image, Responsive, Label } from "semantic-ui-react"
import Emoji from "../../common/Emoji"
import { withRouter } from "react-router-dom"
import { fetchExpeditions, saveTempData, fetchCosts } from "../../state/actions/orderActions"
import { getTotalCarts, fetchCarts } from "../../state/actions/cartActions"
import { connect } from "react-redux"
import jneImage from "../../../images/jne.svg"
import lionImage from "../../../images/lion-parcel.svg"
import siCepat from "../../../images/sicepat.png"
import jntImage from "../../../images/jnt.jpg"
import { mobile, PosedContainer, Div, formatter, idKoko } from "../../common"
import CsSegment from "../../common/CsSegment"

const relatively = {
    position: "relative",
    top: 30
}

class TotalOrder extends React.Component {
    state = { service: null, expedition: null, est: "", ongkir: 0, active: false }

    componentDidMount() {
        const { temp } = this.props

        this.props.fetchExpeditions()
        this.handleGetCosts()
        this.props.getTotalCarts()
        this.props.fetchCarts()

        if (!temp || temp === undefined || Object.keys(temp).length < 1) {
            this.props.history.push("/checkout")
        }
    }

    handleSelectCost = (selectedCost, company, code) => {
        this.setState({ selectedCost, company, code })
    }

    handleGetCosts = () => {
        const {
            temp: { subdistrict, total_weight }
        } = this.props

        const data = {
            origin: 673,
            destination: subdistrict,
            weight: total_weight
        }

        if (data) {
            this.props.fetchCosts(data)
        }
    }

    handleUpdateTemp = () => {
        const {
            company,
            code,
            selectedCost: { service, description, cost }
        } = this.state
        const { user } = this.props

        const ongkirData = {
            ekspedition_company: company,
            ekspedition_remark: service + " " + "-" + " " + description,
            ekspedition_code: code,
            ekspedition_total: cost[0].value,
            estimation: cost[0].etd
        }

        const go = user && user.id === idKoko ? "/checkout/summary" : "/checkout/payment"

        if (ongkirData && ongkirData !== null) {
            const additional = { ...this.props.temp, ...ongkirData }
            this.props.saveTempData(additional)
            this.props.onSaveTempExpedition(ongkirData).then(() => {
                this.props.history.push(go)
            })
        }
    }

    renderClassNames = (service, courier, expStatus) => {
        const { active, expedition } = this.state

        if (active === service && expedition === courier) return "service-item active"
        else if (expStatus === 0) return "service-item inactive"
        else return "service-item"
    }

    render() {
        const {
            temp,
            costs: costy,
            expeditionLength,
            imageUrl,
            selected,
            user,
            selectedCompany,
            loading,
            csData: { cs, csState, onExpandCs }
        } = this.props
        const { selectedCost, company } = this.state

        return (
            <Grid as={PosedContainer} stackable className="total-order">
                <Grid.Column width={4}>
                    {/* <FloatingSummary temp={temp} carts={carts} /> */}
                    {user && user.acc_type !== 3 && <CsSegment cs={cs} csState={csState} onExpandCs={onExpandCs} />}
                </Grid.Column>
                <Grid.Column width={12}>
                    <Grid stackable>
                        <Grid.Row className="from-to">
                            <Grid.Column width={4}>
                                <Segment as={Div} textAlign="center">
                                    <div style={{ fontSize: "2em" }}>
                                        <Emoji label="package" symbol="📦" />
                                    </div>
                                    Dikirim dari
                                    <Header as="h4" content="Batam, Kepulauan Riau" />
                                </Segment>
                            </Grid.Column>
                            <Grid.Column width={8}>
                                <Div style={mobile ? null : relatively}>
                                    <Responsive {...Responsive.onlyMobile} as={Divider} vertical>
                                        Ke
                                    </Responsive>
                                    <Responsive minWidth={760} as={Divider} horizontal>
                                        <Emoji label="airplane" symbol="🛫" style={{ fontSize: "2em" }} /> &nbsp;
                                        ngueeeng!
                                    </Responsive>
                                </Div>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Segment as={Div} textAlign="center">
                                    <div style={{ fontSize: "2em" }}>
                                        <Emoji label="mail" symbol="📭" />
                                    </div>
                                    Dikirim ke
                                    <Header as="h4" content={temp.cityName + ", " + temp.provinceName} />
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                        <Divider />

                        <Grid.Row>
                            <Grid.Column>
                                <Header as="h3">
                                    <Header.Content as={Div}>
                                        Pilih kurir nya (ketemu {expeditionLength} ekspedisi)
                                        <Header.Subheader>
                                            Kurir pilihan kamu supaya tau ongkir sebenarnya
                                        </Header.Subheader>
                                    </Header.Content>
                                </Header>
                                <Grid className="expeditions" columns={mobile ? 2 : 4}>
                                    {costy &&
                                        costy.map(exp => {
                                            if (exp.costs && exp.costs.length > 0) {
                                                return (
                                                    <Grid.Column key={exp.code}>
                                                        <Segment.Group className="expedition-list">
                                                            <Segment basic className="logo">
                                                                <Image src={imageUrl(exp.code)} fluid />
                                                            </Segment>
                                                            {exp.costs &&
                                                                exp.costs.map(item => {
                                                                    if (
                                                                        item.service !== "Cargo" &&
                                                                        item.service !== "BEST"
                                                                    ) {
                                                                        return (
                                                                            <Segment
                                                                                textAlign="center"
                                                                                key={
                                                                                    item.cost[0].value +
                                                                                    item.cost[0].etd
                                                                                }
                                                                                basic
                                                                                vertical
                                                                                onClick={() =>
                                                                                    this.handleSelectCost(
                                                                                        item,
                                                                                        exp.name,
                                                                                        exp.code
                                                                                    )
                                                                                }
                                                                                className={
                                                                                    selected(item, selectedCost) &&
                                                                                    selectedCompany(exp, company)
                                                                                        ? "active"
                                                                                        : ""
                                                                                }
                                                                            >
                                                                                {selected(item, selectedCost) &&
                                                                                    selectedCompany(exp, company) && (
                                                                                        <Label
                                                                                            size="mini"
                                                                                            attached="bottom right"
                                                                                            icon="checkmark"
                                                                                        />
                                                                                    )}
                                                                                <Header
                                                                                    as="h4"
                                                                                    content={item.service}
                                                                                    subheader={item.description}
                                                                                />
                                                                                <p>
                                                                                    {formatter.format(
                                                                                        item.cost[0].value
                                                                                    )}
                                                                                </p>
                                                                            </Segment>
                                                                        )
                                                                    }
                                                                })}
                                                        </Segment.Group>
                                                    </Grid.Column>
                                                )
                                            }
                                        })}
                                </Grid>
                            </Grid.Column>
                        </Grid.Row>

                        <Grid.Row>
                            <Grid.Column>
                                <Responsive minWidth={760}>
                                    <Container as={Div} textAlign="right" style={{ marginTop: "2em" }}>
                                        <Button
                                            type="button"
                                            basic
                                            content="Kembali"
                                            icon="angle left"
                                            className="link-btn"
                                            onClick={this.props.history.goBack}
                                        />
                                        <Button
                                            content="Lanjut ke Pembayaran"
                                            disabled={!selectedCost || loading}
                                            type="submit"
                                            labelPosition="right"
                                            icon="angle right"
                                            className="btn-zigzag"
                                            loading={loading}
                                            onClick={this.handleUpdateTemp}
                                        />
                                    </Container>
                                </Responsive>
                                <Responsive {...Responsive.onlyMobile} as={Container}>
                                    <Button
                                        fluid
                                        content="Lanjut ke Pembayaran"
                                        disabled={!selectedCost || loading}
                                        type="submit"
                                        labelPosition="right"
                                        icon="angle right"
                                        className="btn-zigzag"
                                        loading={loading}
                                        onClick={this.handleUpdateTemp}
                                    />{" "}
                                    <br />
                                    <Button
                                        type="button"
                                        fluid
                                        basic
                                        content="Kembali"
                                        icon="angle left"
                                        className="link-btn"
                                        onClick={this.props.history.goBack}
                                    />
                                </Responsive>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ order, cart }) => {
    const expeditionLength = order.costs && order.costs.filter(exp => exp.costs && exp.costs.length > 0).length
    const selected = (item, state) => state && item.service === state.service && item.description === state.description
    const selectedCompany = (item, company) => company && item.name === company
    const localTemp = localStorage.getItem("temp")
    const temp = order.temp && Object.keys(order.temp).length > 0 ? order.temp : JSON.parse(localTemp)
    const imageUrl = code => {
        let url = ""

        if (code === "jne") url = jneImage
        else if (code === "sicepat") url = siCepat
        else if (code === "lion") url = lionImage
        else url = jntImage

        return url
    }

    return {
        expeditions: order.expeditions,
        weight: cart.weight,
        carts: cart.carts,
        costs: order.costs,
        selected,
        selectedCompany,
        expeditionLength,
        imageUrl,
        temp
    }
}

const actionList = { fetchExpeditions, getTotalCarts, fetchCarts, saveTempData, fetchCosts }

export default connect(
    mapState,
    actionList
)(withRouter(TotalOrder))
