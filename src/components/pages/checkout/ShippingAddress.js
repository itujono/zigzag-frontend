import React from "react"
import {
    Form,
    Container,
    Button,
    Header,
    Checkbox,
    Divider,
    Grid,
    Segment,
    Icon,
    Popup,
    Responsive,
    Input,
    TextArea as TextAreaInput
} from "semantic-ui-react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import TextInput from "../../form/TextInput"
import SelectInput from "../../form/SelectInput"
import TextArea from "../../form/TextArea"
import Emoji from "../../common/Emoji"
import { provinces, cities } from "../../state/common"
import { getTotalCarts } from "../../state/actions/cartActions"
import { saveTempData } from "../../state/actions/orderActions"
import { Field, reduxForm, formValueSelector } from "redux-form"
import FloatingSummary from "./FloatingSummary"
import NoData from "../../common/NoData"
import { mobile, PosedContainer, Div, idKoko } from "../../common"
import CsSegment from "../../common/CsSegment"

const provinceList = provinces.map(province => {
    return {
        key: Number(province.province_id),
        value: Number(province.province_id),
        text: province.province
    }
})

const cityList = cities.map(city => {
    return {
        key: Number(city.city_id),
        value: Number(city.city_id),
        province: Number(city.province_id),
        zip: Number(city.postal_code),
        text: city.city_name
    }
})

class ShippingAddress extends React.Component {
    state = {
        zip: 0,
        defaulted: false,
        selectedProvince: null,
        selectedCity: null,
        helpText: false,
        helpDropshipper: false,
        dropshipper: false,
        partner: false
    }

    componentDidMount() {
        this.props.initialize()
        this.props.getTotalCarts()
    }

    componentDidUpdate(prevState) {
        if (prevState.dropshipper !== this.state.dropshipper) {
            if (!this.state.dropshipper) {
                this.props.change("dropshipper_name", "")
                this.props.change("dropshipper_tele", "")
                this.props.change("dropshipper_address", "")
            }
        }
    }

    handleHelpTextDropshipper = () => this.setState({ helpDropshipper: true })

    handleSelectProvince = (e, data) => {
        this.setState({ selectedProvince: data })
    }

    handleSelectSubdistrict = (e, data) => {
        this.setState({ selectedSubdistrict: data })
    }

    handleChangeAddress = (e, data) => {
        this.setState({ shipping_address: data })
    }

    handleSelectCity = (e, data) => {
        this.setState({ selectedCity: data, helpText: true })
        this.props.onSubdistrictOptions(data)
    }

    handleSaveTempData = data => {
        const {
            selectedCity,
            selectedProvince,
            defaulted,
            dropshipper: dropState,
            dropshipper_name,
            dropshipper_tele,
            selectedSubdistrict,
            jne_online_booking,
            notes
        } = this.state
        const { initialValues, user, weight, subdistrictName, initialSubdistrictName, pickup } = this.props
        let prov = initialValues && provinces && provinces.find(p => p.province_id === initialValues.province)
        let city = initialValues && cities && cities.find(c => Number(c.city_id) === initialValues.city)
        const cityName =
            cities && cities.filter(city => Number(city.city_id) === data.city).map(city => city.city_name)[0]
        const provinceName =
            provinces && provinces.filter(prov => prov.province_id === data.province).map(prov => prov.province)[0]
        const selectedCities = cityList && cityList.filter(city => city.province === selectedProvince)
        const zipCode =
            selectedCities && selectedCities.filter(city => city.value === selectedCity).map(city => city.zip)[0]
        const dropshipper = {
            name: data.dropshipper_name && data.dropshipper_name !== "" ? data.dropshipper_name : "",
            tele: data.dropshipper_tele && data.dropshipper_tele !== "" ? data.dropshipper_tele : "",
            address: data.dropshipper_address && data.dropshipper_address !== "" ? data.dropshipper_address : ""
        }
        const isNotKoko = user && user.id !== idKoko
        const urlKoko = pickup ? "/checkout/summary" : "/checkout/expedition"
        const random = Math.floor(Math.random() * (100 - 10)) + 10

        const actual = {
            ...data,
            unique_code: random,
            cityName,
            provinceName,
            pickup,
            zip: zipCode,
            total_weight: weight,
            notes,
            subdistrict_name: subdistrictName
        }
        const defaultData = {
            unique_code: random,
            name: initialValues.name,
            tele: initialValues.tele,
            province: initialValues.province,
            city: initialValues.city,
            provinceName: prov.province,
            cityName: city.city_name,
            subdistrict: initialValues.subdistrict,
            subdistrict_name: initialSubdistrictName,
            zip: initialValues.zip,
            shipping_address: initialValues.address,
            dropshipper: dropState && data.dropshipper_name !== "" ? true : false,
            dropshipper_name: dropshipper.name || dropshipper_name,
            dropshipper_tele: dropshipper.tele || dropshipper_tele,
            jne_online_booking: data.jne_online_booking,
            total_weight: weight,
            pickup,
            notes
        }

        const buatKoko = {
            unique_code: random,
            name: this.props.nameValue,
            tele: this.props.teleValue,
            total_weight: weight,
            shipping_address: this.state.shipping_address,
            city: selectedCity,
            cityName: selectedCity && cityList.find(item => item.value === selectedCity).text,
            province: selectedProvince,
            provinceName: selectedProvince && provinceList.find(item => item.value === selectedProvince).text,
            subdistrict: Number(selectedSubdistrict),
            subdistrict_name: subdistrictName,
            dropshipper: dropState && data.dropshipper_name !== "" ? true : false,
            dropshipper_name,
            dropshipper_tele,
            jne_online_booking,
            zip: zipCode,
            pickup,
            notes
        }

        const realData = !isNotKoko ? buatKoko : defaulted ? defaultData : actual

        if (user) {
            this.props.saveTempData(realData)
            this.props.history.push(
                user.acc_type === 3 && !isNotKoko
                    ? urlKoko
                    : user.acc_type === 3
                    ? "/checkout/summary"
                    : user.acc_type === 2 && pickup
                    ? "/checkout/payment"
                    : "/checkout/expedition"
            )
        }
    }

    handleChangeDropshipper = () => {
        this.setState(prevState => ({ dropshipper: !prevState.dropshipper }))
    }

    handleDefaultToggle = () => {
        this.setState(prevState => ({ defaulted: !prevState.defaulted }))
    }

    handleChange = (e, { name, value }) => {
        this.setState({ [name]: value })
    }

    handleTogglePartner = () => {
        this.setState(prevState => ({ partner: !prevState.partner, defaulted: !prevState.defaulted }))
        this.props.onTogglePickup()
    }

    handleTogglePickup = () => {
        this.setState(prevState => ({ defaulted: !prevState.defaulted }))
        this.props.onTogglePickup()
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    render() {
        const {
            initialValues,
            handleSubmit,
            carts,
            total,
            user,
            nameValue,
            teleValue,
            provinceValue,
            cityValue,
            jobValue,
            addressValue,
            subtotal,
            subdistrictName,
            initialSubdistrictName,
            dropName,
            dropTele,
            weight,
            discount,
            subdistrictOptions,
            pickup,
            csData: { cs, csState, onExpandCs }
        } = this.props
        const { selectedProvince, selectedCity, helpText, defaulted, dropshipper, partner } = this.state

        let prov = initialValues && provinces && provinces.find(p => p.province_id === initialValues.province)
        let city = initialValues && cities && cities.find(c => Number(c.city_id) === initialValues.city)
        const isFromBatam = user.city && user.city === 48

        const selectedCities = cityList && cityList.filter(city => city.province === selectedProvince)
        const zipCode =
            selectedCities && selectedCities.filter(city => city.value === selectedCity).map(city => city.zip)
        const defaultButton = defaulted ? (
            <span>
                {" "}
                Eh isi sendiri aja deh <Emoji label="cat laughing" symbol="😹" />{" "}
            </span>
        ) : (
            <span>
                {" "}
                Pake detail dari Profile saya aja <Emoji label="lightning" symbol="⚡" />{" "}
            </span>
        )

        const cityNameSummaries = cities
            .filter(city => Number(city.city_id) === cityValue)
            .map(city => city.city_name)[0]
        const provinceNameSummaries = cities
            .filter(province => Number(province.province_id) === provinceValue)
            .map(province => province.province)[0]
        const initializeCity =
            initialValues &&
            cities.filter(city => Number(city.city_id) === initialValues.city).map(city => city.city_name)[0]
        const initialProvince =
            initialValues &&
            cities
                .filter(province => Number(province.province_id) === initialValues.province)
                .map(province => province.province)[0]

        const nameSummary = defaulted && initialValues ? initialValues.name : nameValue
        const teleSummary = defaulted && initialValues ? initialValues.tele : teleValue
        const provinceSummary = defaulted && initialValues ? initialProvince : provinceNameSummaries
        const citySummary = defaulted && initialValues ? initializeCity : cityNameSummaries
        const zipSummary = defaulted && initialValues ? initialValues.zip : zipCode
        const addressSummary = defaulted && initialValues ? initialValues.address : addressValue
        const subdistrictSummary = defaulted && initialValues ? initialSubdistrictName : subdistrictName

        const summaries = {
            name: nameSummary,
            tele: teleSummary,
            province: provinceSummary,
            city: citySummary,
            zip: zipSummary,
            subdistrict: subdistrictSummary,
            address: addressSummary
        }

        const drop = {
            name: dropName,
            tele: dropTele,
            jne_online_booking: jobValue
        }

        return (
            <Grid as={PosedContainer} stackable padded="vertically">
                <Grid.Column width={4}>
                    <Div basic padded>
                        <Header as="h3" content="Ringkasan" />
                        <FloatingSummary
                            summaries={summaries}
                            dropshipper={dropshipper}
                            states={this.state}
                            drop={drop}
                            subtotal={subtotal}
                            user={user}
                            carts={carts}
                            total={total}
                            weight={weight}
                            discount={discount}
                        />
                    </Div>
                    {user && user.acc_type !== 3 && <CsSegment cs={cs} csState={csState} onExpandCs={onExpandCs} />}
                </Grid.Column>
                <Grid.Column width={12} className="shipping-address">
                    {carts && carts.length < 1 ? (
                        <NoData
                            message="Cart nya aja masih kosong. Cepat cari produk nya dulu!"
                            className="mt3em"
                            image
                        />
                    ) : user && user.acc_type === 3 ? (
                        <Segment basic padded>
                            <Header as="h2">
                                <Header.Content as={Div}>
                                    Kamu Adalah Partner!
                                    <Header.Subheader>
                                        Karena kamu adalah partner Zigzag, jadi kamu hanya bisa memilih shipping address
                                        dengan cara jemput langsung di gudang Zigzag
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>
                            <Container as={Div} style={{ marginBottom: "1.5em" }}>
                                {isFromBatam && (
                                    <React.Fragment>
                                        <Checkbox
                                            name="partner"
                                            defaultChecked={pickup}
                                            label="Ya, saya bersedia ambil langsung barang nya di gudang Zigzag"
                                            onChange={this.handleTogglePartner}
                                        />{" "}
                                        <br />
                                        <br />
                                        {pickup && (
                                            <Form>
                                                <label>Notes</label>
                                                <TextAreaInput
                                                    name="notes"
                                                    placeholder="Biasanya untuk copy paste alamat Shopee kamu"
                                                    onChange={this.handleChange}
                                                />
                                            </Form>
                                        )}
                                    </React.Fragment>
                                )}
                                <br />
                                <br />
                                {user.id === idKoko && !pickup && (
                                    <React.Fragment>
                                        <Form onSubmit={handleSubmit(this.handleSaveTempData)}>
                                            <Form.Group as={Div} widths="equal">
                                                <Field
                                                    name="name"
                                                    type="text"
                                                    placeholder="Misal: Agustina..."
                                                    label="Nama"
                                                    component={TextInput}
                                                    onChange={this.handleChange}
                                                />
                                                <Field
                                                    name="tele"
                                                    type="number"
                                                    placeholder="Misal: 0812222222..."
                                                    label="Nomor HP"
                                                    component={TextInput}
                                                    onChange={this.handleChange}
                                                />
                                            </Form.Group>
                                            <Form.Group widths="equal">
                                                <Field
                                                    name="province"
                                                    type="text"
                                                    placeholder="Misal: Jawa Barat..."
                                                    label="Provinsi"
                                                    component={SelectInput}
                                                    value={selectedProvince}
                                                    options={provinceList}
                                                    onChange={this.handleSelectProvince}
                                                    search
                                                    selection
                                                />
                                                <Field
                                                    name="city"
                                                    type="text"
                                                    placeholder="Misal: Bandung..."
                                                    label="Kota"
                                                    component={SelectInput}
                                                    options={selectedCities}
                                                    onChange={this.handleSelectCity}
                                                    search
                                                    selection
                                                />
                                            </Form.Group>
                                            <Form.Group widths="equal">
                                                <Field
                                                    name="subdistrict"
                                                    type="text"
                                                    placeholder="Misal: Ciwidey..."
                                                    label="Kecamatan"
                                                    component={SelectInput}
                                                    options={subdistrictOptions}
                                                    onChange={this.handleSelectSubdistrict}
                                                    search
                                                    selection
                                                />
                                                {helpText ? (
                                                    <div className="field">
                                                        <label>Kode Pos</label>
                                                        <input
                                                            disabled
                                                            type="number"
                                                            name="zip"
                                                            placeholder="Kode pos kota kamu"
                                                            value={zipCode ? zipCode : undefined}
                                                        />
                                                        <span className="help-text">
                                                            Kode pos terisi otomatis sesuai kota mu
                                                        </span>
                                                    </div>
                                                ) : (
                                                    <Field
                                                        name="zip"
                                                        type="number"
                                                        placeholder="Misal: 39433"
                                                        // value={null}
                                                        label="Kode pos"
                                                        component={TextInput}
                                                    />
                                                )}
                                            </Form.Group>
                                            <Form.Group as={Div}>
                                                <Field
                                                    name="shipping_address"
                                                    placeholder="Alamat pengiriman kamu..."
                                                    rows={4}
                                                    width={16}
                                                    onChange={this.handleChangeAddress}
                                                    label="Alamat pengiriman"
                                                    component={TextArea}
                                                />
                                            </Form.Group>
                                        </Form>
                                        <Checkbox
                                            tabIndex="1"
                                            toggle
                                            label="Saya adalah dropshipper"
                                            onChange={this.handleChangeDropshipper}
                                            className={`checkbox-radio ${this.state.dropshipper ? "activated" : ""}`}
                                        />{" "}
                                        &nbsp;{" "}
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            inverted
                                            content="Kamu bisa sok-sokan ngirim barang ini ke customer kamu dengan pake data diri kamu loh. Keren gak sih? Keren pastinya dong"
                                        />
                                    </React.Fragment>
                                )}
                                <br />
                                {dropshipper && user.id === idKoko && (
                                    <Grid className="dropshipper-partner">
                                        <Grid.Row columns="equal">
                                            <Grid.Column>
                                                <label>Nama saya sebagai pengirim</label>
                                                <Input
                                                    as={Div}
                                                    name="dropshipper_name"
                                                    type="text"
                                                    fluid
                                                    onChange={this.handleChange}
                                                    placeholder="Misal: Zainab Abidin"
                                                />
                                            </Grid.Column>
                                            <Grid.Column>
                                                <label>Nomor HP saya sebagai pengirim</label>
                                                <Input
                                                    as={Div}
                                                    name="dropshipper_tele"
                                                    type="number"
                                                    fluid
                                                    onChange={this.handleChange}
                                                    placeholder="Misal: 08199999999"
                                                />
                                            </Grid.Column>
                                            <Grid.Column>
                                                <label>JNE Online Booking (JOB)</label>
                                                <Input
                                                    as={Div}
                                                    name="jne_online_booking"
                                                    type="text"
                                                    fluid
                                                    onChange={this.handleChange}
                                                    placeholder="Masukkan kode JOB kamu di sini, jika ada"
                                                />
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                )}
                                <br />
                                <div>
                                    <Button
                                        fluid={mobile}
                                        disabled={
                                            dropshipper && !this.state.dropshipper_name && !this.state.dropshipper_tele
                                        }
                                        className="btn-zigzag"
                                        labelPosition="right"
                                        onClick={this.handleSaveTempData}
                                        icon
                                    >
                                        <Icon name="chevron right" />
                                        Lanjut
                                    </Button>
                                </div>
                            </Container>
                        </Segment>
                    ) : (
                        <React.Fragment>
                            {user && user.acc_type === 2 && (
                                <Segment basic padded>
                                    {isFromBatam && (
                                        <React.Fragment>
                                            <Header
                                                as="h2"
                                                content="Ambil langsung di Zigzag"
                                                subheader="Kamu bisa ambil langsung barang kamu di gudang Zigzag, silakan centang pilihan di bawah ya"
                                            />
                                            <Checkbox
                                                name="pickup"
                                                label="Ya, saya bersedia ambil langsung barang nya di gudang Zigzag"
                                                onChange={this.handleTogglePickup}
                                                style={{ marginBottom: "1em" }}
                                            />
                                        </React.Fragment>
                                    )}
                                    {!pickup && isFromBatam && <Divider horizontal>Atau</Divider>}
                                    {pickup && (
                                        <React.Fragment>
                                            <Segment basic padded>
                                                <Form>
                                                    <label>Notes</label>
                                                    <TextAreaInput
                                                        name="notes"
                                                        placeholder="Biasanya untuk copy paste alamat Shopee kamu"
                                                        onChange={this.handleChange}
                                                    />
                                                </Form>
                                            </Segment>
                                            <Segment basic padded textAlign="right">
                                                <Button
                                                    type="button"
                                                    basic
                                                    content="Kembali"
                                                    className="link-btn"
                                                    icon="angle left"
                                                    onClick={this.props.history.goBack}
                                                />{" "}
                                                &nbsp; &nbsp;
                                                <Button
                                                    content="Lanjut ke Metode Pembayaran"
                                                    type="submit"
                                                    labelPosition="right"
                                                    icon="angle right"
                                                    disabled={carts && carts.length < 1}
                                                    className="btn-zigzag"
                                                    onClick={this.handleSaveTempData}
                                                />
                                            </Segment>
                                        </React.Fragment>
                                    )}
                                </Segment>
                            )}
                            {!pickup && (
                                <Segment basic padded>
                                    <Header as="h2">
                                        <Header.Content as={Div}>
                                            Alamat Pengiriman
                                            <Header.Subheader>
                                                Silakan isi detail alamat ke mana orderan kamu akan dikirim.
                                            </Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                    <Container as={Div} style={{ marginBottom: "1.5em" }}>
                                        <Button
                                            fluid={mobile}
                                            content={defaultButton}
                                            className="btn-zigzag"
                                            onClick={this.handleDefaultToggle}
                                        />
                                    </Container>
                                    <Form onSubmit={handleSubmit(this.handleSaveTempData)}>
                                        {!this.state.defaulted ? (
                                            <React.Fragment>
                                                <Form.Group as={Div} widths="equal">
                                                    <Field
                                                        name="name"
                                                        type="text"
                                                        placeholder="Misal: Agustina..."
                                                        label="Nama"
                                                        component={TextInput}
                                                    />
                                                    <Field
                                                        name="tele"
                                                        type="number"
                                                        placeholder="Misal: 0812222222..."
                                                        label="Nomor HP"
                                                        component={TextInput}
                                                    />
                                                </Form.Group>
                                                <Form.Group as={Div} widths="equal">
                                                    <Field
                                                        name="province"
                                                        type="text"
                                                        placeholder="Misal: Jawa Barat..."
                                                        label="Provinsi"
                                                        component={SelectInput}
                                                        value={selectedProvince}
                                                        options={provinceList}
                                                        onChange={this.handleSelectProvince}
                                                        search
                                                        selection
                                                    />
                                                    <Field
                                                        name="city"
                                                        type="text"
                                                        placeholder="Misal: Bandung..."
                                                        label="Kota"
                                                        component={SelectInput}
                                                        options={selectedCities}
                                                        onChange={this.handleSelectCity}
                                                        search
                                                        selection
                                                    />
                                                </Form.Group>

                                                <Form.Group as={Div} widths="equal">
                                                    <Field
                                                        name="subdistrict"
                                                        type="text"
                                                        placeholder="Misal: Ciwidey..."
                                                        label="Kecamatan"
                                                        component={SelectInput}
                                                        options={subdistrictOptions}
                                                        onChange={this.handleChange}
                                                        search
                                                        selection
                                                    />

                                                    {helpText ? (
                                                        <div className="field">
                                                            <label>Kode Pos</label>
                                                            <input
                                                                disabled
                                                                type="number"
                                                                name="zip"
                                                                placeholder="Kode pos kota kamu"
                                                                value={zipCode ? zipCode : undefined}
                                                            />
                                                            <span className="help-text">
                                                                Kode pos terisi otomatis sesuai kota mu
                                                            </span>
                                                        </div>
                                                    ) : (
                                                        <Field
                                                            name="zip"
                                                            type="number"
                                                            placeholder="Misal: 39433"
                                                            // value={null}
                                                            label="Kode pos"
                                                            component={TextInput}
                                                        />
                                                    )}
                                                </Form.Group>
                                                <Form.Group as={Div}>
                                                    <Field
                                                        name="shipping_address"
                                                        placeholder="Alamat pengiriman kamu..."
                                                        rows={4}
                                                        width={16}
                                                        label="Alamat pengiriman"
                                                        component={TextArea}
                                                    />
                                                </Form.Group>
                                            </React.Fragment>
                                        ) : (
                                            <React.Fragment>
                                                <Form.Group as={Div} widths="equal">
                                                    <div className="field">
                                                        <label>Nama</label>
                                                        <input
                                                            type="text"
                                                            readOnly
                                                            name="name"
                                                            placeholder="Misal: Agustina Pujianto..."
                                                            value={initialValues ? initialValues.name : ""}
                                                        />
                                                    </div>
                                                    <div className="field">
                                                        <label>Nomor HP</label>
                                                        <input
                                                            type="number"
                                                            readOnly
                                                            name="tele"
                                                            placeholder="Misal: 0812222222..."
                                                            value={initialValues ? initialValues.tele : ""}
                                                        />
                                                    </div>
                                                </Form.Group>
                                                <Form.Group as={Div} widths="equal">
                                                    <div className="field">
                                                        <label>Provinsi</label>
                                                        <input
                                                            type="text"
                                                            readOnly
                                                            name="province"
                                                            placeholder="Misal: Jawa Barat"
                                                            value={prov ? prov.province : ""}
                                                        />
                                                    </div>
                                                    <div className="field">
                                                        <label>Kota</label>
                                                        <input
                                                            type="text"
                                                            readOnly
                                                            name="city"
                                                            placeholder="Misal: Bandung"
                                                            value={city ? city.city_name : ""}
                                                        />
                                                    </div>
                                                </Form.Group>
                                                <Form.Group as={Div} widths="equal">
                                                    <div className="field">
                                                        <label>Kecamatan</label>
                                                        <input
                                                            type="text"
                                                            readOnly
                                                            name="subdistrict"
                                                            placeholder="Misal: Ciwidey"
                                                            value={
                                                                initialValues &&
                                                                initialValues.subdistrict &&
                                                                initialSubdistrictName
                                                            }
                                                        />
                                                    </div>
                                                    <div className="field">
                                                        <label>Kode Pos</label>
                                                        <input
                                                            type="number"
                                                            readOnly
                                                            name="zip"
                                                            placeholder="Misal: 68888"
                                                            value={initialValues && initialValues.zip}
                                                        />
                                                    </div>
                                                </Form.Group>
                                                <Div className="field">
                                                    <label>Alamat Pengiriman</label>
                                                    <textarea
                                                        name="address"
                                                        readOnly
                                                        placeholder="Alamat pengiriman kamu..."
                                                        rows="4"
                                                        value={initialValues ? initialValues.address : ""}
                                                    />
                                                </Div>
                                            </React.Fragment>
                                        )}
                                        <Divider />
                                        <Checkbox
                                            tabIndex="1"
                                            toggle
                                            label="Saya adalah dropshipper"
                                            onChange={this.handleChangeDropshipper}
                                            className={`checkbox-radio ${this.state.dropshipper ? "activated" : null}`}
                                        />{" "}
                                        &nbsp;{" "}
                                        <Popup
                                            trigger={<Icon name="question circle outline" />}
                                            inverted
                                            content="Kamu bisa (ceritanya) ngirim barang ini ke customer kamu dengan pake data diri kamu loh. Keren gak sih? Keren pastinya dong"
                                        />
                                        {dropshipper && (
                                            <Form.Group widths="equal">
                                                <Field
                                                    as={Div}
                                                    name="dropshipper_name"
                                                    type="text"
                                                    label="Nama saya sebagai pengirim"
                                                    component={TextInput}
                                                    placeholder="Misal: Zainab Abidin"
                                                />
                                                <Field
                                                    as={Div}
                                                    name="dropshipper_tele"
                                                    type="number"
                                                    label="Nomor HP saya sebagai pengirim"
                                                    component={TextInput}
                                                    placeholder="Misal: 08199999999"
                                                />
                                                <Field
                                                    as={Div}
                                                    name="jne_online_booking"
                                                    type="text"
                                                    label="JNE Online Booking (JOB)"
                                                    component={TextInput}
                                                    placeholder="Masukkan kode JOB kamu di sini, jika ada"
                                                />
                                            </Form.Group>
                                        )}
                                        <Divider />
                                        <Responsive minWidth={760} as={Container} textAlign="right">
                                            <Button
                                                type="button"
                                                basic
                                                content="Kembali"
                                                className="link-btn"
                                                icon="angle left"
                                                onClick={this.props.history.goBack}
                                            />{" "}
                                            &nbsp; &nbsp;
                                            <Button
                                                content="Lanjut ke Pilihan Ekspedisi"
                                                color="black"
                                                type="submit"
                                                labelPosition="right"
                                                icon="angle right"
                                                disabled={carts && carts.length < 1}
                                                className="btn-zigzag"
                                            />
                                        </Responsive>
                                        <Responsive {...Responsive.onlyMobile} as={Container} textAlign="center">
                                            <Button
                                                fluid
                                                content="Lanjut ke Pilihan Ekspedisi"
                                                color="black"
                                                type="submit"
                                                labelPosition="right"
                                                icon="angle right"
                                                disabled={carts && carts.length < 1}
                                                className="btn-zigzag"
                                            />{" "}
                                            <br />
                                            <Button
                                                type="button"
                                                fluid
                                                basic
                                                content="Kembali"
                                                icon="angle left"
                                                className="link-btn"
                                                onClick={this.props.history.goBack}
                                            />{" "}
                                            &nbsp; &nbsp;
                                        </Responsive>
                                    </Form>

                                    <Container className="disclaimer">
                                        1. Harga <strong>Total</strong> terbaru orderan kamu (include ongkos kirim) akan
                                        dikalkulasikan di halaman selanjutnya.
                                    </Container>
                                </Segment>
                            )}
                        </React.Fragment>
                    )}
                </Grid.Column>
            </Grid>
        )
    }
}

const selector = formValueSelector("Checkout")

const mapState = (state, ownProps) => {
    const initialValues = state.user.user
    const subdistricts = ownProps.subdistricts
    const subdistrictOptions =
        subdistricts &&
        subdistricts.map(item => ({
            key: item.subdistrict_id,
            value: item.subdistrict_id,
            text: item.subdistrict_name
        }))
    const subdistrictValue = selector(state, "subdistrict")
    const subdistrictName =
        subdistricts &&
        subdistricts
            .filter(item => Number(item.subdistrict_id) === Number(subdistrictValue))
            .map(item => item.subdistrict_name)[0]
    const initialSubdistrictName =
        subdistricts &&
        initialValues &&
        subdistricts
            .filter(item => Number(item.subdistrict_id) === Number(initialValues.subdistrict))
            .map(item => item.subdistrict_name)[0]

    return {
        initialValues,
        nameValue: selector(state, "name"),
        teleValue: selector(state, "tele"),
        provinceValue: selector(state, "province"),
        cityValue: selector(state, "city"),
        zipValue: selector(state, "zip"),
        addressValue: selector(state, "shipping_address"),
        remarksValue: selector(state, "remarks"),
        dropName: selector(state, "dropshipper_name"),
        dropTele: selector(state, "dropshipper_tele"),
        dropAddress: selector(state, "dropshipper_address"),
        jobValue: selector(state, "jne_online_booking"),
        carts: state.cart.carts,
        total: state.cart.total,
        user: state.user.user || {},
        temp: state.order.temp,
        subdistrictOptions,
        subdistrictValue,
        subdistrictName,
        subdistricts,
        initialSubdistrictName
    }
}

const validate = values => {
    let errors = {}
    const phoneRegex = /\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/

    if (!values.name) errors.name = "Wah kok kosong nih namanya?"
    if (!values.tele) errors.tele = "Nomor telepon juga nggak boleh kosong ya"
    else if (values.tele < 99999999) errors.tele = "Ini beneran nomor hape?"
    if (!values.province) errors.province = "Provinsi tolong diisi ya"
    if (!values.city) errors.city = "Kota mu tolong diisi ya"
    if (!values.subdistrict) errors.subdistrict = "Kecamatan mu tolong diisi ya"
    if (!values.dropshipper_name) errors.dropshipper_name = "Kalo mau dropshipping, nama kamu wajib diisi, ya"
    if (!values.dropshipper_tele) errors.dropshipper_tele = "Kalo mau dropshipping, nomor HP kamu wajib diisi, ya"
    else if (values.dropshipper_tele < 1) errors.dropshipper_tele = "Kalo gak mau pake nomor HP nya, isi aja 1"
    if (!values.dropshipper_address) errors.dropshipper_address = "Kalo mau dropshipping, alamat kamu wajib diisi, ya"
    if (!values.shipping_address) errors.shipping_address = "Nah alamat rumah ini justru yang paling penting"

    return errors
}

export default reduxForm({
    form: "Checkout",
    enableReinitialize: true,
    destroyOnUnmount: false,
    keepDirtyOnReinitialize: true,
    validate
})(
    withRouter(
        connect(
            mapState,
            { getTotalCarts, saveTempData }
        )(ShippingAddress)
    )
)
