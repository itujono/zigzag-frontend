import React from "react"
import { Segment, List, Icon, Header, Divider, Item, Container } from "semantic-ui-react"
import Emoji from "../../common/Emoji"
import { fetchTempData } from "../../state/actions/orderActions"
import { connect } from "react-redux"
import { imageURL, formatter, priceType, idKoko } from "../../common"

const { promo, vip, member, partner } = priceType

class FloatingSummary extends React.Component {
    render() {
        const {
            summaries: { name, tele, province, city, subdistrict, address, expedition },
            summaries,
            carts,
            dropshipper,
            user,
            drop,
            total,
            discount,
            weight,
            subtotal
        } = this.props
        const { zip, ...allSummaries } = summaries
        const undef = summaries && Object.entries(allSummaries).every(item => item[1] === undefined)
        const dropUndef = drop && Object.entries(drop).every(item => item[1] === undefined)

        if (undef && zip.length < 1 && dropUndef)
            return (
                <Segment textAlign="center" stacked padded="very">
                    <div style={{ fontSize: "3em" }}>
                        <Emoji label="farmer" symbol="👨‍🌾" />
                    </div>
                    <Header as="h4" content="Semua ringkasan kamu akan muncul di sini" />
                </Segment>
            )

        return (
            <React.Fragment>
                <Segment stacked padded className="floating-summary">
                    <List relaxed>
                        {name && name !== "" && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Nama </List.Header>
                                    <List.Description>{name}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {tele && tele !== "" && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Telepon </List.Header>
                                    <List.Description>{tele}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {province && province !== "" && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Provinsi </List.Header>
                                    <List.Description>{province}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {city && city !== "" && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Kota </List.Header>
                                    <List.Description>{city}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {subdistrict && subdistrict !== "" && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Kecamatan </List.Header>
                                    <List.Description>{subdistrict}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {zip && zip !== "" && zip.length > 0 && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Kode Pos </List.Header>
                                    <List.Description>{zip}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {address && address !== "" && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Alamat </List.Header>
                                    <List.Description>{address}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {expedition && expedition !== "" && (
                            <List.Item>
                                <Icon name="angle right" />
                                <List.Content>
                                    <List.Header> Ekspedisi </List.Header>
                                    <List.Description>{expedition}</List.Description>
                                </List.Content>
                            </List.Item>
                        )}
                        {dropshipper && (
                            <React.Fragment>
                                <Divider />
                                <List.Item>
                                    <Icon name="angle right" />
                                    <List.Content>
                                        <List.Header> Dropshipper </List.Header>
                                        <List.Description>
                                            <div>Nama: {drop.name}</div>
                                            <div>Nomor HP: {drop.tele}</div>
                                            <div>JOB: {drop.jne_online_booking}</div>
                                        </List.Description>
                                    </List.Content>
                                </List.Item>
                            </React.Fragment>
                        )}
                    </List>
                </Segment>

                <Header as="h3" content={carts && carts.length > 1 ? "Item-item Kamu" : "Item Kamu"} />
                <Segment stacked padded className="floating-summary">
                    <Item.Group unstackable divided>
                        {carts &&
                            carts.map(cart => {
                                const priceTemp = JSON.parse(cart.price)
                                const parsedPic = cart.picture
                                const picture = imageURL + "/" + parsedPic

                                const promoPrice =
                                    cart &&
                                    priceTemp
                                        .filter(price => {
                                            if (cart.promo && cart.promo === 1) return price.price_type === promo
                                        })
                                        .map(price => price.price)[0]

                                const priceAll =
                                    cart &&
                                    priceTemp
                                        .filter(price => {
                                            if (user) {
                                                if (user.id === idKoko) return price.price_type === member
                                                else {
                                                    if (user.acc_type === 1) return price.price_type === member
                                                    else if (user.acc_type === 2) return price.price_type === vip
                                                    else return price.price_type === partner
                                                }
                                            } else {
                                                return price.price_type === member
                                            }
                                        })
                                        .map(price => price.price)[0]

                                const actualPrice = promoPrice ? promoPrice : priceAll

                                return (
                                    <Item key={cart.id}>
                                        <Item.Image size="mini" src={picture} />
                                        <Item.Content verticalAlign="middle">
                                            <Header as="h4" content={cart.name} />
                                            <p className="price">
                                                Harga: {formatter.format(actualPrice)} x {cart.qty}
                                            </p>
                                            <Item.Meta>Warna: {cart.color}</Item.Meta>
                                            <Item.Meta>Berat total: {cart.weight} gram</Item.Meta>
                                            <p>
                                                <strong>{formatter.format(actualPrice * cart.qty)}</strong>
                                            </p>
                                        </Item.Content>
                                    </Item>
                                )
                            })}
                    </Item.Group>
                    <Container className="total">
                        <p className="discount">
                            Diskon: <span>{`- ${formatter.format(discount) || 0}`}</span>
                        </p>
                        Total berat:
                        <Header as="h4" content={`${weight} gram`} /> <br />
                        Estimasi total:
                        <Header as="h3" content={formatter.format(subtotal - discount)} />
                    </Container>
                </Segment>
            </React.Fragment>
        )
    }
}

const mapState = ({ order }) => ({
    temp: order.temp
})

export default connect(
    mapState,
    { fetchTempData }
)(FloatingSummary)
