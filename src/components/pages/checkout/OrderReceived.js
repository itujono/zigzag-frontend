import React from "react"
import { Link } from "react-router-dom"
import { Grid, Segment, Header, Container, Button, List } from "semantic-ui-react"
import Emoji from "../../common/Emoji"
import { connect } from "react-redux"
import { fetchTempData } from "../../state/actions/orderActions"
import { PosedContainer, H2, Par, Div } from "../../common"

const backToHome = (
    <div>
        <span>Kembali ke Home </span> <Emoji label="Home" symbol="🏠" />
    </div>
)

const receivedText = (
    <Container>
        <span>Orderan Diterima! </span> <Emoji label="love" symbol="💖" />
    </Container>
)

class OrderReceived extends React.Component {
    componentDidMount() {
        const { temp } = this.props

        if (!temp || temp === undefined || Object.keys(temp).length < 1) {
            window.location.replace("/checkout")
        }
    }

    render() {
        const { temp, bankAccount } = this.props

        return (
            <Grid as={PosedContainer} stackable centered columns={3} className="centered-grid no-border order-received">
                <Grid.Column>
                    <Segment padded="very" centered className="wizard-success">
                        <svg
                            width="133px"
                            height="133px"
                            viewBox="0 0 133 133"
                            version="1.1"
                            xmlns="http://www.w3.org/2000/svg"
                            style={{ marginBottom: "2.5em" }}
                        >
                            <g id="check-group" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <circle id="filled-circle" fill="#8732ff" cx="66.5" cy="66.5" r="54.5" />
                                <circle id="white-circle" fill="#FFFFFF" cx="66.5" cy="66.5" r="55.5" />
                                <circle id="outline" stroke="#8732ff" strokeWidth="4" cx="66.5" cy="66.5" r="54.5" />
                                <polyline id="check" stroke="#FFFFFF" strokeWidth="4" points="41 70 56 85 92 49" />
                            </g>
                        </svg>
                        <Container textAlign="center">
                            <Header as={H2} content={receivedText} />
                            {temp && temp.payment_method === "transfer" ? (
                                <p as={Par} style={{ marginBottom: "3em" }}>
                                    Great! Orderan kamu telah kami terima, dan email berisi detail info orderan juga
                                    sudah ada di inbox email kamu. Silakan. Namun sebelumnya, silakan lakukan pembayaran
                                    terlebih dahulu ke rekening yang tertulis di bawah ini. <br />
                                    <br />
                                    <p style={{ fontSize: 11 }}>
                                        P.S. Jangan lupa include-kan <strong>kode unik transfer</strong> nya ketika mau
                                        transfer pembayaran, ya!
                                    </p>{" "}
                                    <br />
                                    <p style={{ fontSize: 11 }}>
                                        P.P.S. Kalo kamu belum melakukan konfirmasi order lebih dari 2 jam, maka orderan
                                        kamu akan dibatalkan otomatis oleh sistem.
                                    </p>{" "}
                                    <br />
                                </p>
                            ) : (
                                <p as={Par} className="mb2em">
                                    Great! Orderan kamu telah kami terima, dan email berisi detail info orderan juga
                                    sudah ada di inbox email kamu. Silakan dicek. Detail orderan kamu bisa dilihat di
                                    menu History Order di Profile kamu. Thank you! :)
                                </p>
                            )}
                            {temp && temp.payment_method === "transfer" && (
                                <React.Fragment>
                                    <Container textAlign="center" className="bank-info">
                                        <List relaxed="very">
                                            {bankAccount &&
                                                bankAccount.map(item => (
                                                    <List.Item key={item.id}>
                                                        <List.Header>Bank {item.bank_account}</List.Header>
                                                        {item.number} a.n. {item.under_name}
                                                    </List.Item>
                                                ))}
                                        </List>
                                    </Container>
                                    <Container
                                        as={Div}
                                        textAlign="center"
                                        className="cta"
                                        style={{ marginBottom: "3em" }}
                                    >
                                        <Header
                                            as="h3"
                                            content="Sudah melakukan pembayaran?"
                                            style={{ marginBottom: "1em" }}
                                        />
                                        <Button
                                            as={Link}
                                            to="/order-confirmation"
                                            className="btn-zigzag"
                                            content="Konfirmasi Pembayaran"
                                        />
                                    </Container>
                                </React.Fragment>
                            )}
                            <Button
                                className="link-btn"
                                basic
                                content={backToHome}
                                onClick={() => window.location.replace("/")}
                            />
                        </Container>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ order }) => {
    const localTemp = localStorage.getItem("temp")
    const temp = order.temp && Object.keys(order.temp).length > 0 ? order.temp : JSON.parse(localTemp)

    return {
        temp
    }
}

export default connect(
    mapState,
    { fetchTempData }
)(OrderReceived)
