import React from "react"
import { Header, Table, Image, Button, Container, List, Icon, Grid, Responsive, Segment, Item } from "semantic-ui-react"
import { withRouter, Link } from "react-router-dom"
import Prompt from "../../common/Prompt"
import { removeEntireCarts } from "../../state/actions/cartActions"
import { fetchTempData, saveTempData, submitOrder } from "../../state/actions/orderActions"
import { imageURL, formatter, priceType, PosedContainer, PosedTable, Div, idKoko, mobile, tablet } from "../../common"
import { connect } from "react-redux"
import CsSegment from "../../common/CsSegment"

const { promo, member, vip, partner } = priceType

class SummaryPage extends React.Component {
    state = { received: false, prompt: false }

    componentDidMount() {
        const temp = localStorage.getItem("temp")

        if (!temp || temp === undefined || Object.keys(temp).length < 1) {
            this.props.history.push("/checkout")
        }
    }

    completeOrder = () => this.setState({ received: true })

    handleOpenPrompt = () => this.setState({ prompt: true })

    handleSubmitData = () => {
        const { actual, discount, user, partnerType, expedition = {} } = this.props
        const random = actual.unique_code || 0

        const products =
            this.props.carts &&
            this.props.carts.map(product => {
                const priceTemp = JSON.parse(product.price)

                const promoPrice = priceTemp
                    .filter(price => {
                        if (product.promo && product.promo === 1) return price.price_type === promo
                    })
                    .map(price => price.price)[0]

                const priceAll = priceTemp
                    .filter(price => {
                        if (user) {
                            if (user.id === idKoko) return price.price_type === partner
                            else {
                                if (user && user.acc_type === 1) return price.price_type === member
                                else if (user && user.acc_type === 2) return price.price_type === vip
                                else return price.price_type === partner
                            }
                        } else {
                            return price.price_type === member
                        }
                    })
                    .map(price => price.price)[0]

                const actualPrice = promoPrice ? promoPrice : priceAll
                let nyo = "NYO"
                const productName =
                    user && user.id === idKoko && product.name.includes(nyo)
                        ? "DIS" + product.name.slice(3)
                        : product.name

                return {
                    product_id: product.product_id,
                    product_name: productName,
                    product_qty: product.qty,
                    product_price: actualPrice,
                    product_color: product.color,
                    product_size: product.size,
                    product_total_price: actualPrice * product.qty,
                    product_more_detail_id: product.product_more_detail_id
                }
            })

        const data = {
            name: actual.name,
            email: user.email,
            tele: actual.tele,
            customer_service_id: user.customer_service_id,
            province: actual.province,
            city: actual.city,
            province_name: actual.provinceName,
            city_name: actual.cityName,
            zip: actual.zip,
            shipping_address: actual.pickup ? "Jemput di gudang" : actual.shipping_address,
            subdistrict_name: actual.subdistrict_name || "-",
            payment_method: actual.payment_method,
            dropshipper_name: actual.dropshipper_name || "-",
            dropshipper_tele: actual.dropshipper_tele || 1,
            order_detail: JSON.stringify(products),
            jne_online_booking: actual.jne_online_booking || "-",
            discount: discount || 0,
            total_weight: actual.total_weight,
            order_id: actual.pickup ? 0 : expedition.id,
            unique_code: actual.pickup ? 0 : random,
            notes: actual.pickup ? actual.notes : ""
        }

        const partnerData = {
            name: actual.name || user.name,
            email: user.email,
            tele: actual.tele || user.tele,
            customer_service_id: user.customer_service_id,
            province: actual.province || "-",
            city: actual.city || "-",
            province_name: actual.provinceName || "-",
            city_name: actual.cityName || "-",
            zip: actual.zip || 0,
            shipping_address: actual.pickup ? "Jemput di gudang" : actual.shipping_address || "-",
            subdistrict_name: actual.subdistrict_name || "-",
            payment_method: "deposit",
            dropshipper_name: actual.dropshipper_name || "-",
            dropshipper_tele: actual.dropshipper_tele || 1,
            order_detail: JSON.stringify(products),
            jne_online_booking: actual.jne_online_booking || "-",
            discount: discount || 0,
            total_weight: actual.total_weight,
            order_id: actual.pickup ? 0 : expedition.id,
            unique_code: actual.pickup ? 0 : random,
            notes: actual.pickup ? actual.notes : ""
        }

        const actualData = partnerType ? partnerData : data

        if (actualData && actualData !== null) {
            this.setState({ prompt: false })
            this.props.saveTempData(actualData)
            this.props.submitOrder(actualData, () => {
                this.setState({ prompt: false })
                localStorage.removeItem("expedition")
                localStorage.removeItem("temp")
                this.props.history.push("/checkout/order-received")
            })
        }
    }

    render() {
        const {
            carts,
            total,
            user,
            actual,
            discount,
            csData: { cs, csState, onExpandCs },
            partnerType,
            deposit,
            loading
        } = this.props
        const pickup = actual.pickup
        const disc = discount ? discount : 0
        const random = actual.unique_code
        const ongkir = actual.ekspedition_total ? actual.ekspedition_total : 0
        const generalTotal = total && total !== null && total
        const partnerNotSufficient = (partnerType && deposit < generalTotal) || (partnerType && deposit === 0)

        return (
            <Grid as={PosedContainer} stackable className="summary">
                {!tablet && (
                    <Grid.Column width={4}>
                        {!partnerType && <CsSegment cs={cs} csState={csState} onExpandCs={onExpandCs} />}
                    </Grid.Column>
                )}
                <Grid.Column width={tablet ? 12 : 10}>
                    <Header as="h2">
                        <Header.Content as={Div}>
                            Ringkasan Pemesanan
                            <Header.Subheader>Silakan review dulu sebelum klik tombol All good.</Header.Subheader>
                        </Header.Content>
                    </Header>

                    <Responsive as={Segment} basic minWidth={760}>
                        <Table as={PosedTable} singleLine padded="very" className="summary-table">
                            <Table.Header>
                                <Table.Row textAlign="center">
                                    <Table.HeaderCell singleLine textAlign="left">
                                        Item
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>Harga</Table.HeaderCell>
                                    <Table.HeaderCell>Kuantitas</Table.HeaderCell>
                                    <Table.HeaderCell>Total</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>
                                {carts &&
                                    carts.map(cart => {
                                        const priceTemp = JSON.parse(cart.price)
                                        const picture = imageURL + "/" + cart.picture

                                        const priceAll =
                                            cart &&
                                            priceTemp
                                                .filter(price => {
                                                    if (user) {
                                                        if (user.id === idKoko) return price.price_type === member
                                                        else {
                                                            if (user.acc_type === 1) return price.price_type === member
                                                            else if (user.acc_type === 2)
                                                                return price.price_type === vip
                                                            else return price.price_type === partner
                                                        }
                                                    } else {
                                                        return price.price_type === member
                                                    }
                                                })
                                                .map(price => price.price)[0]

                                        return (
                                            <Table.Row textAlign="center" key={cart.id}>
                                                <Table.Cell textAlign="left">
                                                    <Header as="h5" image>
                                                        <Image src={picture} rounded size="medium" />
                                                        <Header.Content>
                                                            {cart.name}
                                                            <Header.Subheader>Warna: {cart.color}</Header.Subheader>
                                                            {cart && cart.size !== 0 && (
                                                                <Header.Subheader>Size: {cart.size}</Header.Subheader>
                                                            )}
                                                        </Header.Content>
                                                    </Header>
                                                </Table.Cell>
                                                <Table.Cell singleLine>{formatter.format(priceAll)}</Table.Cell>
                                                <Table.Cell singleLine>{cart.qty}</Table.Cell>
                                                <Table.Cell>{formatter.format(cart.total_price)}</Table.Cell>
                                            </Table.Row>
                                        )
                                    })}
                            </Table.Body>

                            <Table.Footer>
                                <Table.Row textAlign="center">
                                    <Table.HeaderCell />
                                    <Table.HeaderCell />
                                    <Table.HeaderCell disabled>Subtotal</Table.HeaderCell>
                                    <Table.HeaderCell disabled>{formatter.format(generalTotal)}</Table.HeaderCell>
                                </Table.Row>
                                <Table.Row textAlign="center">
                                    <Table.HeaderCell />
                                    <Table.HeaderCell />
                                    <Table.HeaderCell>Ongkos kirim</Table.HeaderCell>
                                    <Table.HeaderCell>{formatter.format(ongkir)}</Table.HeaderCell>
                                </Table.Row>
                                <Table.Row textAlign="center">
                                    <Table.HeaderCell />
                                    <Table.HeaderCell />
                                    <Table.HeaderCell>Discount</Table.HeaderCell>
                                    <Table.HeaderCell>- {formatter.format(disc)}</Table.HeaderCell>
                                </Table.Row>
                                {!pickup && (
                                    <Table.Row textAlign="center">
                                        <Table.HeaderCell />
                                        <Table.HeaderCell />
                                        <Table.HeaderCell>Kode unik transfer</Table.HeaderCell>
                                        <Table.HeaderCell>{random}</Table.HeaderCell>
                                    </Table.Row>
                                )}
                                <Table.Row textAlign="center">
                                    <Table.HeaderCell />
                                    <Table.HeaderCell />
                                    <Table.HeaderCell>
                                        <strong>General Total</strong>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                        <strong>
                                            {pickup
                                                ? formatter.format(generalTotal + ongkir - disc)
                                                : formatter.format(generalTotal + ongkir - disc + random)}
                                        </strong>
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>
                    </Responsive>

                    <Responsive as={Grid.Row} {...Responsive.onlyMobile} columns={1}>
                        <Grid.Column>
                            <Item.Group divided unstackable>
                                {carts &&
                                    carts.map(cart => {
                                        const priceTemp = JSON.parse(cart.price)
                                        const picture = imageURL + "/" + cart.picture

                                        const priceAll =
                                            cart &&
                                            priceTemp
                                                .filter(price => {
                                                    if (user) {
                                                        if (user.id === idKoko) return price.price_type === member
                                                        else {
                                                            if (user.acc_type === 1) return price.price_type === member
                                                            else if (user.acc_type === 2)
                                                                return price.price_type === vip
                                                            else return price.price_type === partner
                                                        }
                                                    } else {
                                                        return price.price_type === member
                                                    }
                                                })
                                                .map(price => price.price)[0]

                                        return (
                                            <Item className="cart-item" key={cart.id}>
                                                <Item.Image size="tiny" src={picture} />

                                                <Item.Content>
                                                    <Item.Header>
                                                        {cart.name} <br />{" "}
                                                        <span>
                                                            {cart.qty} x Rp {priceAll}
                                                        </span>
                                                    </Item.Header>
                                                    <Item.Meta>{cart.product_description}</Item.Meta>
                                                    <Item.Extra>
                                                        <p>
                                                            Warna: <strong>{cart.color}</strong>
                                                        </p>{" "}
                                                        {cart && cart.size !== 0 && (
                                                            <p>
                                                                Size: <strong>{cart.size}</strong>
                                                            </p>
                                                        )}
                                                        <p>
                                                            Total berat: <strong>{cart.weight} gram</strong>
                                                        </p>
                                                        <p>
                                                            Total: <strong>Rp {cart.total_price}</strong>
                                                        </p>
                                                    </Item.Extra>
                                                </Item.Content>
                                            </Item>
                                        )
                                    })}
                            </Item.Group>
                            <List unstackable relaxed="very" style={{ marginBottom: "3em" }}>
                                <List.Item>
                                    <List.Header content="Subtotal" />
                                    {formatter.format(generalTotal)}
                                </List.Item>
                                <List.Item>
                                    <List.Header content="Ongkos kirim" />
                                    {formatter.format(ongkir)}
                                </List.Item>
                                <List.Item>
                                    <List.Header content="Discount" />- {formatter.format(disc)}
                                </List.Item>
                                {!pickup && (
                                    <List.Item>
                                        <List.Header content="Kode unik transfer" />
                                        {random}
                                    </List.Item>
                                )}
                                <List.Item>
                                    <List.Header content="General total" />
                                    <strong>
                                        {pickup
                                            ? formatter.format(generalTotal + ongkir - disc)
                                            : formatter.format(generalTotal + ongkir - disc + random)}
                                    </strong>
                                </List.Item>
                            </List>
                        </Grid.Column>
                    </Responsive>

                    {partnerType ? (
                        <List relaxed="very" className="summary-list">
                            <List.Item as={Div}>
                                <Icon name="right triangle" />
                                <List.Content>
                                    <List.Header>Metode Pembayaran</List.Header>
                                    <List.Description>
                                        Deposit{" "}
                                        {user && user.id !== idKoko && (
                                            <span>
                                                - jumlah deposit kamu saat ini{" "}
                                                {user.deposit !== 0 ? "sebesar " : "masih "}{" "}
                                                {formatter.format(user.deposit)}
                                            </span>
                                        )}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                            <List.Item as={Div}>
                                <Icon name="right triangle" />
                                <List.Content>
                                    <List.Header>Notes</List.Header>
                                    <List.Description>{actual.notes || "-"}</List.Description>
                                </List.Content>
                            </List.Item>
                            {user && user.id === idKoko && (
                                <React.Fragment>
                                    <List.Item as={Div}>
                                        <Icon name="right triangle" />
                                        <List.Content>
                                            <List.Header>Alamat Pengiriman</List.Header>
                                            <List.Description>
                                                {actual && !pickup ? (
                                                    <span>
                                                        {actual.shipping_address}, {actual.subdistrict_name},{" "}
                                                        {actual.cityName}, Provinsi {actual.provinceName} - {actual.zip}
                                                    </span>
                                                ) : (
                                                    "Jemput di gudang"
                                                )}
                                            </List.Description>
                                        </List.Content>
                                    </List.Item>
                                    <List.Item as={Div}>
                                        <Icon name="right triangle" />
                                        <List.Content>
                                            <List.Header>Detail Penerima</List.Header>
                                            <List.Description>
                                                {actual && actual.name && (
                                                    <span>
                                                        {actual.name} ({actual.name.split(" ")[0]})
                                                    </span>
                                                )}{" "}
                                                - {actual.tele}
                                            </List.Description>
                                        </List.Content>
                                    </List.Item>
                                    <List.Item as={Div}>
                                        <Icon name="right triangle" />
                                        <List.Content>
                                            <List.Header>Ekspedisi</List.Header>
                                            <List.Description>
                                                {actual && actual.ekspedition_company && (
                                                    <span>
                                                        {actual.ekspedition_company === "pos"
                                                            ? actual.ekspedition_company
                                                            : actual.ekspedition_company.toUpperCase()}
                                                    </span>
                                                )}{" "}
                                                ({actual.ekspedition_remark})
                                            </List.Description>
                                        </List.Content>
                                    </List.Item>
                                    {actual && actual.dropshipper && (
                                        <List.Item as={Div}>
                                            <Icon name="right triangle" />
                                            <List.Content>
                                                <List.Header>Saya Sebagai Dropshipper</List.Header>
                                                <List.Description>
                                                    Nama: {actual.dropshipper_name} <br />
                                                    Nomor HP: {actual.dropshipper_tele} <br />
                                                    {actual.jne_online_booking &&
                                                        `JNE Online Booking: ${actual.jne_online_booking}`}{" "}
                                                    <br />
                                                </List.Description>
                                            </List.Content>
                                        </List.Item>
                                    )}
                                </React.Fragment>
                            )}
                        </List>
                    ) : (
                        <List relaxed="very" className="summary-list">
                            <List.Item as={Div}>
                                <Icon name="right triangle" />
                                <List.Content>
                                    <List.Header>Alamat Pengiriman</List.Header>
                                    <List.Description>
                                        {actual && !pickup ? (
                                            <span>
                                                {actual.shipping_address}, {actual.subdistrict_name}, {actual.cityName},
                                                Provinsi {actual.provinceName} - {actual.zip}
                                            </span>
                                        ) : (
                                            "Jemput di gudang"
                                        )}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                            <List.Item as={Div}>
                                <Icon name="right triangle" />
                                <List.Content>
                                    <List.Header>Detail Penerima</List.Header>
                                    <List.Description>
                                        {actual && actual.name && (
                                            <span>
                                                {actual.name} ({actual.name.split(" ")[0]})
                                            </span>
                                        )}{" "}
                                        - {actual.tele}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                            <List.Item as={Div}>
                                <Icon name="right triangle" />
                                <List.Content>
                                    <List.Header>Ekspedisi</List.Header>
                                    <List.Description>
                                        {actual && actual.ekspedition_company && (
                                            <span>
                                                {actual.ekspedition_company === "pos"
                                                    ? actual.ekspedition_company
                                                    : actual.ekspedition_company.toUpperCase()}
                                            </span>
                                        )}{" "}
                                        {(actual.ekspedition_remark && actual.ekspedition_remark) || "-"}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                            {actual && actual.dropshipper && (
                                <List.Item as={Div}>
                                    <Icon name="right triangle" />
                                    <List.Content>
                                        <List.Header>Saya Sebagai Dropshipper</List.Header>
                                        <List.Description>
                                            Nama: {actual.dropshipper_name} <br />
                                            Nomor HP: {actual.dropshipper_tele} <br />
                                            {actual.jne_online_booking &&
                                                `JNE Online Booking: ${actual.jne_online_booking}`}{" "}
                                            <br />
                                        </List.Description>
                                    </List.Content>
                                </List.Item>
                            )}
                            <List.Item as={Div}>
                                <Icon name="right triangle" />
                                <List.Content>
                                    <List.Header>Metode Pembayaran</List.Header>
                                    <List.Description>
                                        {actual.payment_method && actual.payment_method === "transfer" ? (
                                            "Transfer Bank (Mandiri/BCA)"
                                        ) : (
                                            <span>
                                                Deposit{" "}
                                                {user &&
                                                    user.id !== idKoko &&
                                                    `- jumlah deposit kamu saat ini sebesar Rp ${user.deposit}`}
                                            </span>
                                        )}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                            <List.Item as={Div}>
                                <Icon name="right triangle" />
                                <List.Content>
                                    <List.Header>Notes</List.Header>
                                    <List.Description>{actual.notes || "-"}</List.Description>
                                </List.Content>
                            </List.Item>
                        </List>
                    )}
                    <Responsive minWidth={760} as={Container} textAlign="right">
                        <Button
                            basic
                            content="Kembali"
                            icon="angle left"
                            className="link-btn"
                            onClick={this.props.history.goBack}
                        />
                        <Button
                            content="All good!"
                            className="btn-zigzag"
                            labelPosition="right"
                            icon="angle right"
                            disabled={
                                (partnerType && deposit < generalTotal) || (partnerType && deposit === 0) || loading
                            }
                            onClick={this.handleOpenPrompt}
                        />
                    </Responsive>
                    <Responsive {...Responsive.onlyMobile} as={Container}>
                        <Button
                            fluid
                            content="All good!"
                            className="btn-zigzag"
                            labelPosition="right"
                            icon="angle right"
                            disabled={
                                (partnerType && deposit < generalTotal) || (partnerType && deposit === 0) || loading
                            }
                            onClick={this.handleOpenPrompt}
                        />{" "}
                        <br />
                        <Button
                            fluid
                            basic
                            content="Kembali"
                            className="link-btn"
                            icon="angle left"
                            onClick={this.props.history.goBack}
                        />
                    </Responsive>

                    {partnerNotSufficient && (
                        <p style={{ textAlign: mobile ? "center" : "right", marginTop: "1em", color: "#999" }}>
                            <Icon name="exclamation triangle" /> Oops! Jumlah deposit kamu masih kurang.{" "}
                            <Link to="/deposit">Deposit dulu</Link>
                        </p>
                    )}

                    <Prompt
                        open={this.state.prompt}
                        header="Apa kamu yakin?"
                        yesText="Pastinya"
                        loading={loading}
                        onClose={() => this.setState({ prompt: false })}
                        confirm={this.handleSubmitData}
                    >
                        Dengan ngeklik tombol di bawah ini, kamu akan placing order di Zigzag. Proses?
                    </Prompt>
                    <Container className="disclaimer">
                        1. Harap diperiksa dengan seksama data-data yang tertera di tabel sebelum placing order. <br />
                        2. <strong>General Total</strong> adalah jumlah yang harus kamu transfer untuk orderan ini.{" "}
                        <br /> 3. Pastikan kamu tidak melewati <strong>batas pembayaran selama 2 jam</strong> setelah
                        placing order, untuk mencegah orderan kamu dibatalkan otomatis oleh sistem.
                    </Container>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ order }, ownProps) => {
    const partnerType = ownProps.user && ownProps.user.acc_type === 3
    const temp = order.temp
    const localTemp = localStorage.getItem("temp")
    const actual = temp && Object.keys(temp).length > 0 ? temp : JSON.parse(localTemp)
    const deposit = ownProps.user && ownProps.user.deposit
    const tempExpedition = localStorage.getItem("expedition")

    return {
        submitResponse: order.response,
        loading: order.loading,
        expedition: ownProps.expedition ? ownProps.expedition : JSON.parse(tempExpedition),
        actual,
        partnerType,
        deposit
    }
}

export default connect(
    mapState,
    { submitOrder, fetchTempData, saveTempData, removeEntireCarts }
)(withRouter(SummaryPage))
