import React from "react"
import { Grid, Segment, Header, Card } from "semantic-ui-react"
import Emoji from "../common/Emoji"
import { connect } from "react-redux"
import { fetchBannerPromoProducts } from "../state/actions/productActions"
import ProductItem from "../common/ProductItem";
import { mobile } from "../common";



class BannerPage extends React.Component {

    componentDidMount() {
        this.props.fetchBannerPromoProducts()
    }

    render() {
        const { bannerProduct, productList, bannerMeta } = this.props

        return (
            <Grid stackable>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Header as="h1" className="section-header category-page">
                            <Header.Content>
                                {bannerProduct && bannerProduct.title} &nbsp; <Emoji {...bannerMeta} />
                                <Header.Subheader>
                                    {bannerProduct && bannerProduct.description}
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Card.Group itemsPerRow={mobile ? 2 : 5}>
                            { productList && productList.map(item => (
                                <ProductItem key={item.product_id} product={item} noLazy />
                                )) }
                        </Card.Group>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}


const mapState = ({ products }, ownProps) => {
    const title = ownProps.match.params.title && ownProps.match.params.title.split(" ").join("-").toLowerCase()
    const bannerProduct = products.bannerProducts && products.bannerProducts
        .filter(item => item.title.split(" ").join("-").toLowerCase() === title)[0]
    const productList = bannerProduct && bannerProduct.products
    const bannerTitle = bannerProduct && bannerProduct.title.toLowerCase()
    const bannerMeta = bannerTitle.includes("Christmas".toLowerCase()) ? { label: "santa", symbol: "🎅"} : { label: "lightning", symbol: "⚡"}

    return {
        title,
        bannerProduct,
        productList,
        bannerMeta
    }
}

export default connect(mapState, { fetchBannerPromoProducts })(BannerPage)