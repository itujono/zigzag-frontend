import React from "react"
import { Grid, Header, Form, Dropdown, Button, Segment, Image, List } from "semantic-ui-react"
import { connect } from "react-redux"
import { fetchCities, fetchSubdistricts } from "../state/actions/rajaOngkirActions"
import { fetchCosts } from "../state/actions/orderActions"
import { formatter } from "../common"
import jne from "../../images/jne.svg"
import jnt from "../../images/jnt.jpg"
import lion from "../../images/lion-parcel.svg"
import sicepat from "../../images/sicepat.png"
import Ball from "../common/Ball";


const weightOptions = [
    { key: 1, text: 1, value: 1000 },
    { key: 2, text: 2, value: 2000 },
    { key: 3, text: 3, value: 3000 },
    { key: 4, text: 4, value: 4000 },
    { key: 5, text: 5, value: 5000 },
    { key: 6, text: 6, value: 6000 },
    { key: 7, text: 7, value: 7000 },
    { key: 8, text: 8, value: 8000 },
    { key: 9, text: 9, value: 9000 },
    { key: 10, text: 10, value: 10000 }
]

const courierOptions = [
    { key: 'all', text: "Semua", value: 'all' },
    // { key: 'jne', text: "JNE", value: 'jne' },
    // { key: 'tiki', text: "TIKI", value: 'tiki' },
    // { key: 'pos', text: "Pos Indonesia", value: 'pos' }
]


class Ongkir extends React.Component {
    state = { origin: 48, destination: 0, weight: 3, result: false }

    componentDidMount() {
        this.props.fetchCities()
    }

    handleSubmit = () => {
        const { destination, weight, subdistrict } = this.state
        if (destination && weight) {
            this.setState({ result: true })
            this.props.fetchCosts({ origin: 673, destination: subdistrict, weight })
        }
    }

    handleCloseResult = () => this.setState({ result: false })

    handleChange = (e, { name, value }) => {
        if (name === 'destination') {
            this.props.fetchSubdistricts(value)
        }
        this.setState({ [name]: value })
    }

    render() {
        const { cityOptions, costs, imageUrl, details, destinationDetails, subdistrictOptions, loading } = this.props
        const { result } = this.state

        return (
            <Grid stackable centered verticalAlign="middle" className="cek-ongkir" style={{ minHeight: '100vh' }}>
                <Grid.Column width={result ? 10 : 6}>
                    <Header as="h3" content="Cek Ongkir" subheader="Supaya kamu bisa tidur nyenyak malam ini" />
                    <span className="ball big"><Ball type="big" /></span>
                    <span className="ball medium"><Ball type="medium" /></span>
                    <span className="ball small"><Ball type="small" /></span>
                    { !result && <Form className="main-form" onSubmit={this.handleSubmit}>
                        <Header as="h4" content="Dikirim dari: Batam, Kepulauan Riau" />
                        <Form.Field>
                            <Dropdown
                                name="destination"
                                placeholder="Kota tujuan nya..."
                                onChange={this.handleChange}
                                options={cityOptions}
                                search
                                selection
                            />
                        </Form.Field>
                        <Form.Field>
                            <Dropdown
                                name="subdistrict"
                                placeholder="Dan kecamatan nya..."
                                onChange={this.handleChange}
                                options={subdistrictOptions}
                                search
                                selection
                            />
                        </Form.Field>
                        <Form.Group>
                            <Form.Select width={10} name="courier" onChange={this.handleChange} options={courierOptions} placeholder="Pilih kurir nya..." search selection />
                            <Form.Select width={6} name="weight" onChange={this.handleChange} options={weightOptions} placeholder="Berat (kg)" search selection />
                        </Form.Group>
                        <Button className="btn-zigzag" content="Cek sekarang" icon="plus" />
                    </Form>}
                    { result && <Button className="link-btn" style={{ marginBottom: '2em' }} basic content="Kembali" icon="chevron left" onClick={this.handleCloseResult} /> }
                    <Segment basic style={{ padding: 0 }}>
                        { result && <List relaxed>
                            <List.Item>
                                <List.Header as="h5" content="Dikirim dari" />
                                Kecamatan Batam Kota, kota Batam, Kepulauan Riau
                            </List.Item>
                            <List.Item>
                                <List.Header as="h5" content="Tujuan" />
                                Kecamatan { destinationDetails && destinationDetails.subdistrict_name }, { destinationDetails && destinationDetails.type.toLowerCase() } { destinationDetails && destinationDetails.city }, 
                                provinsi { destinationDetails && destinationDetails.province }
                            </List.Item>
                            <List.Item>
                                <List.Header as="h5" content="Berat" />
                                { details && details.weight } gram / { details && details.weight / 1000 } kg
                            </List.Item>
                        </List>}
                        { result && costs && costs.map(exp => (
                            <Segment.Group horizontal key={exp.code}>
                                <Segment loading={loading} className="logo">
                                    <Image size="tiny" src={imageUrl(exp.code)} />
                                </Segment>
                                { exp && exp.costs.map(item => {
                                    const etd = item.cost[0].etd === '' ? '-' : item.cost[0].etd + " " + "hari"

                                    return (
                                        <Segment loading={loading} key={item.service + item.description}>
                                            <Header as="h4" content={item.service} subheader={item.description} />
                                            <strong>{ formatter.format(item.cost[0].value) }</strong>
                                            <p>Est: {etd}</p>
                                        </Segment>
                                    )
                                }) }
                            </Segment.Group>
                        )) }
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}


const mapState = ({ rajaOngkir, order }) => {
    const cityOptions = rajaOngkir.cities && rajaOngkir.cities.map(city => ({
        key: city.city_id, text: `${city.city_name}, Provinsi ${city.province}`, value: city.city_id
    }))
    const subdistrictOptions = rajaOngkir.subdistricts && rajaOngkir.subdistricts.map(item => ({
        key: item.subdistrict_id, value: item.subdistrict_id, text: item.subdistrict_name, city: item.city_id
    }))

    const costs = order.costs && order.costs.sort((a, b) => b.costs.length - a.costs.length)
    const destinationDetails = order.details && order.details.destination

    const imageUrl = (code) => {
        if (code === 'jne') return jne
        else if (code === 'lion') return lion
        else if (code === 'sicepat') return sicepat
        else return jnt
    }

    return {
        cities: rajaOngkir.cities,
        details: order.details,
        destinationDetails,
        loading: order.loading,
        costs,
        cityOptions,
        subdistrictOptions,
        imageUrl
    }
}

export default connect(mapState, { fetchCities, fetchCosts, fetchSubdistricts })(Ongkir)