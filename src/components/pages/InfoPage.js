import React from "react"
import { Grid, Header, Card, Menu } from "semantic-ui-react"
import { mobile } from '../common'
import ProductItem from "../common/ProductItem"
import Emoji from "../common/Emoji";
import NoData from "../common/NoData"
import { Link, Switch, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { fetchLatestProducts, fetchRestockProducts } from "../state/actions/productActions"
import { fetchUser } from "../state/actions/userActions";



class InfoPage extends React.Component {

	componentDidMount() {
        this.props.fetchLatestProducts()
        this.props.fetchRestockProducts()
	}


    render() {
		const { products, user, loading, page, catName } = this.props

        return (
			<Grid stackable>
				<Grid.Column width={3}>
					<Header as="h1" className="section-header category-page">
						<Header.Content>
							{catName}&nbsp; <Emoji label="firefighter" symbol="👩‍🚒" />
							<Header.Subheader>
								Waktunya baca {catName.toLowerCase()} supaya lebih ngerti tentang kami.
							</Header.Subheader>
						</Header.Content>
					</Header>
				</Grid.Column>
				<Grid.Column width={13}>
					<Card.Group itemsPerRow={mobile ? 2 : 4}>
					{
						products && products.length > 0 ? products.map(product => {
                            return <ProductItem
                                as={Card}	
                                productId={product.product_id}
                                loading={loading}
                                key={product.product_id}
                                user={user ? user : ''}
                                product={product}
                                noLazy
                            />
                        }) : <NoData basic message={`Masih belum ada ${page} buat sekarang`} />
					}
					</Card.Group>
				</Grid.Column>
			</Grid>
		)
    }
}


const mapState = ({ products, user }, ownProps) => {
    const page = ownProps.match.params.name
    const latests = products.latests
	const restock = products.restock
	const catName = page && page.split("-").join(" ")	

	return {
		user: user.user,
		categories: products.categories,
        loading: products.loading,
        products: page === 'produk-terbaru' ? latests : restock,
		page,
		catName
	}
}

export default connect(mapState, { fetchRestockProducts, fetchLatestProducts, fetchUser })(withRouter(InfoPage));
