import React from "react"
import { Grid, Header, Card } from "semantic-ui-react"
import { mobile } from "../common"
import ProductItem from "../common/ProductItem"
import Emoji from "../common/Emoji"
import NoData from "../common/NoData"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { fetchLatestProducts, fetchRestockProducts } from "../state/actions/productActions"
import { fetchUser } from "../state/actions/userActions"

class ProductsPage extends React.Component {
    componentDidMount() {
        const { page } = this.props
        page && page === "produk-terbaru" ? this.props.fetchLatestProducts() : this.props.fetchRestockProducts()
    }

    render() {
        const { products, user, loading, page, description } = this.props
        const catName = page && page.split("-").join(" ")

        return (
            <Grid stackable className="category-page">
                <Grid.Column width={3}>
                    <Header as="h1" className="section-header">
                        <Header.Content>
                            {catName}
                            <Header.Subheader>
                                {description}
                                <div style={{ marginTop: ".5em" }}>
                                    <Emoji
                                        label={page === "produk-terbaru" ? "gown" : "bikini"}
                                        symbol={page === "produk-terbaru" ? "👗" : "👙"}
                                    />
                                </div>
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                </Grid.Column>
                <Grid.Column width={13}>
                    <Card.Group itemsPerRow={mobile ? 2 : 4}>
                        {products && products.length < 1 ? (
                            <NoData basic message={`Masih belum ada ${page} buat sekarang`} />
                        ) : (
                            products &&
                            products.map(product => (
                                <ProductItem
                                    key={product.product_id}
                                    once={product.once}
                                    as={Card}
                                    productId={product.product_id}
                                    loading={loading}
                                    user={user ? user : ""}
                                    product={product}
                                    noLazy
                                />
                            ))
                        )}
                    </Card.Group>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ products, user }, ownProps) => {
    const page = ownProps.match.params.name
    const latests = products.latests
    const restock = products.restock
    const description =
        page === "produk-terbaru" ? "Baru masuk! Baru masuk! Baru masuk!" : "Produk laris yang baru aja diupdate"

    return {
        user: user.user,
        categories: products.categories,
        loading: products.loading,
        products: page === "produk-terbaru" ? latests : restock,
        page,
        description
    }
}

export default connect(
    mapState,
    { fetchRestockProducts, fetchLatestProducts, fetchUser }
)(withRouter(ProductsPage))
