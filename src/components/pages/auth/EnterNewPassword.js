import React from "react"
import { Link, withRouter } from "react-router-dom"
import { Grid, Form, Button, Header, Message } from "semantic-ui-react"
import { Field, reduxForm } from "redux-form"
import TextInput from "../../form/TextInput"
import { connect } from "react-redux"
import { saveNewPassword } from "../../state/actions/authActions"
import { mobile } from "../../common"

class EnterNewPassword extends React.Component {
    handleEnterNewPassword = data => {
        const { search } = this.props.location
        const params = new URLSearchParams(search)
        const email = params.get("email")
        const code = params.get("code")

        const newData = {
            email,
            code,
            password: data.password,
            password_confirmation: data.password_confirmation
        }

        if (newData) {
            this.props.saveNewPassword(newData).then(() => {
                this.props.history.push("/password-changed")
            })
        }
    }

    render() {
        const { handleSubmit, error } = this.props
        const err = error()

        return (
            <Grid centered stackable columns={mobile ? 1 : 3} className="centered-grid main-form">
                <Grid.Column>
                    <Header as="h2" className="form-header">
                        <Header.Content>
                            Masukkan password baru kamu
                            <Header.Subheader>
                                Oke, silakan masukkan password baru kamu. Jangan pake nama kucing kesayanganmu, ya.
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    <Form onSubmit={handleSubmit(this.handleEnterNewPassword)}>
                        <Field
                            name="password"
                            type="password"
                            placeholder="Masukkan password baru kamu..."
                            component={TextInput}
                            label="Password"
                        />
                        <Field
                            name="password_confirmation"
                            type="password"
                            placeholder="Ulangi password baru kamu tadi..."
                            component={TextInput}
                            label="Ulangi Password"
                        />
                        {err && err.length > 0 && <Message error header="Oops!" content={err[0]} />}
                        <Form.Button type="submit" content="Submit" fluid className="mt2em" />
                    </Form>
                    <div className="switching">
                        <Button to="/login" as={Link} basic className="link-btn mb2em">
                            Sudah punya akun?
                        </Button>
                    </div>
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.password) errors.password = "Password nya kok kosong?"
    else if (values.password.length < 6) errors.password = "Minimal 6 karakter, ya"

    if (values.password_confirmation !== values.password)
        errors.password_confirmation = "Kok password nya nggak match, ya?"

    return errors
}

const mapState = ({ auth }) => {
    const error = () => {
        if (auth.message) {
            for (let i in auth.message) {
                return auth.message[i]
            }
        }
    }

    return {
        error,
        errorType: auth.errorType
    }
}

export default reduxForm({ form: "EnterNewPassword", validate })(
    connect(
        mapState,
        { saveNewPassword }
    )(withRouter(EnterNewPassword))
)
