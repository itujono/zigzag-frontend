import React from "react"
import { Link } from "react-router-dom"
import { Grid, Form, Button, Message, Header } from "semantic-ui-react"
import { Field, reduxForm } from "redux-form"
import { forgotPassword } from "../../state/actions/authActions"
import { connect } from "react-redux"
import TextInput from "../../form/TextInput"
import Logo from "../../common/Logo"
import { mobile } from "../../common"

class Forgot extends React.Component {
    handleSubmitForgot = email => {
        this.props.forgotPassword(email, () => {
            this.props.history.push("/forgot-sent")
        })
    }

    render() {
        const { handleSubmit, loading, errorMessage, errorType } = this.props

        return (
            <React.Fragment>
                <Link to="/">
                    <Logo size="tiny" />
                </Link>
                <Grid stackable centered columns={mobile ? 1 : 3} className="centered-grid main-form">
                    <Grid.Column>
                        <Header as="h2" className="form-header">
                            <Header.Content>
                                Lupa Password?
                                <Header.Subheader>
                                    Jangan sedih. Semua orang bisa aja lupa. Reset aja password kamu di sini.
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                        <Form loading={loading} onSubmit={handleSubmit(this.handleSubmitForgot)}>
                            <Field
                                name="email"
                                type="text"
                                placeholder="Masukkan email yang biasa kamu pake buat login..."
                                component={TextInput}
                                label="Email"
                            />
                            {errorMessage && errorType && errorType === "forgot" && (
                                <Message style={{ display: "block" }} error header="Oops!" content={errorMessage} />
                            )}
                            <Form.Button type="submit" content="Submit" fluid />
                        </Form>
                        <div className="switching">
                            <Button to="/login" as={Link} basic className="link-btn mb2em">
                                Sudah punya akun?
                            </Button>
                        </div>
                    </Grid.Column>
                </Grid>
            </React.Fragment>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.email) errors.email = "Email nya kok kosong?"
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) errors.email = "Ini sih bukan email ya"

    return errors
}

const mapState = ({ auth }) => ({
    loading: auth.loading,
    errorType: auth.errorType,
    errorMessage: auth.message
})

export default reduxForm({ form: "Forgot", validate })(
    connect(
        mapState,
        { forgotPassword }
    )(Forgot)
)
