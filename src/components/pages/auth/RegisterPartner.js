import React from "react"
import { Link } from "react-router-dom"
import { Field, reduxForm } from "redux-form"
import TextInput from "../../form/TextInput"
import SelectInput from "../../form/SelectInput"
import { withRouter } from "react-router-dom"
import { Grid, Form, Button, Message, Header, Checkbox, Segment, Container } from "semantic-ui-react"
import { connect } from "react-redux"
import { fetchCities, fetchProvinces, fetchProvince, fetchSubdistricts } from "../../state/actions/rajaOngkirActions"
import { signupUser } from "../../state/actions/authActions"
import TextArea from "../../form/TextArea"
import Emoji from "../../common/Emoji"
import Logo from "../../common/Logo"
import { mobile, Div, Par } from "../../common"

const existed = "Isian email sudah ada sebelumnya."

class RegisterPartner extends React.Component {
    state = {
        helpText: false,
        selectedProvince: "",
        selectedCity: [],
        type: 0,
        done: false,
        passwordVisible: false
    }

    componentDidMount() {
        this.props.fetchProvinces()
        this.props.fetchCities()
    }

    handleSignup = data => {
        const { cityList } = this.props
        const { selectedCity } = this.state
        const zipCode = cityList && cityList.filter(city => city.value === selectedCity).map(city => city.zip)[0]
        const additional = { ...data, deposit: 0, zip: zipCode, acc_type: 3, customer_service_id: 6 }

        if (additional) {
            this.setState({ done: true }, () => {
                this.props.signupUser(additional, () => {
                    this.props.reset()
                })
            })
        }
    }

    handleChangeProvince = (e, data) => {
        this.setState({ selectedProvince: data || null })
    }

    handleChangeCity = (e, data) => {
        this.setState({ selectedCity: data, helpText: true })
        this.props.fetchSubdistricts(data)
    }

    handleShowPassword = e => {
        if (e || e.keyCode === 32) {
            this.setState(prevState => ({ passwordVisible: !prevState.passwordVisible }))
        }
    }

    render() {
        const { handleSubmit, errorMessage, errorType, subdistricts, cityList, provinceList } = this.props
        const { selectedProvince, selectedCity, helpText, done, passwordVisible } = this.state
        const city = cityList && cityList.filter(city => city.province === selectedProvince)
        const zipCode = cityList && cityList.filter(city => city.value === selectedCity).map(city => city.zip)

        if (done)
            return (
                <Grid centered columns={mobile ? 1 : 3} className="centered-grid no-border">
                    <Grid.Column>
                        <Segment padded="very" centered className="wizard-success">
                            <svg
                                width="133px"
                                height="133px"
                                viewBox="0 0 133 133"
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                style={{ marginBottom: "2.5em" }}
                            >
                                <g id="check-group" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <circle id="filled-circle" fill="#8732ff" cx="66.5" cy="66.5" r="54.5" />
                                    <circle id="white-circle" fill="#FFFFFF" cx="66.5" cy="66.5" r="55.5" />
                                    <circle
                                        id="outline"
                                        stroke="#8732ff"
                                        strokeWidth="4"
                                        cx="66.5"
                                        cy="66.5"
                                        r="54.5"
                                    />
                                    <polyline id="check" stroke="#FFFFFF" strokeWidth="4" points="41 70 56 85 92 49" />
                                </g>
                            </svg>
                            <Container textAlign="center">
                                <Header as="h2" content="Terima kasih sudah mendaftar! 🌞" />
                                <p className="mb2em">
                                    Kamu sudah berhasil mendaftar. Great! Sekarang, silakan cek inbox kamu untuk
                                    mengaktifkan akun kamu. <br /> <br />
                                    P.S. Kalo emailnya gak masuk, jangan lupa cek folder spam nya juga ya.
                                </p>
                                <Button
                                    icon="shopping bag"
                                    as={Link}
                                    to="/register"
                                    content="Kembali ke Register"
                                    className="btn-zigzag"
                                />
                            </Container>
                        </Segment>
                    </Grid.Column>
                </Grid>
            )

        return (
            <Grid stackable centered>
                <Grid.Column width={16}>
                    <Link to="/">
                        <Logo size="tiny" />
                    </Link>
                    <Grid stackable centered columns={mobile ? 1 : 2} className="centered-grid main-form">
                        <Grid.Column>
                            <Header as="h2" className="form-header">
                                <Header.Content as={Div}>
                                    Register buat Partner <Emoji label="clouds" symbol="⛅" />
                                    <Header.Subheader>
                                        Ayo jadi partner Zigzag dan dapatkan kecupan hangat nya!
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>
                            <Form onSubmit={handleSubmit(this.handleSignup)}>
                                <Form.Group widths="equal">
                                    <Field
                                        name="email"
                                        type="text"
                                        placeholder="Masukkan email kamu..."
                                        component={TextInput}
                                        label="Email"
                                    />
                                    <Field
                                        name="name"
                                        type="text"
                                        placeholder="Masukkan nama lengkap kamu..."
                                        component={TextInput}
                                        label="Nama Lengkap"
                                    />
                                </Form.Group>
                                <Field
                                    name="password"
                                    type={passwordVisible ? "text" : "password"}
                                    placeholder="Masukkan password pilihan kamu..."
                                    component={TextInput}
                                    label="Password"
                                />
                                <Form.Field>
                                    <Checkbox
                                        label="Tunjukkan password"
                                        tabIndex="-1"
                                        onChange={this.handleShowPassword}
                                    />
                                </Form.Field>
                                <Field
                                    name="password2"
                                    type={passwordVisible ? "text" : "password"}
                                    placeholder="Ulangi password kamu tadi..."
                                    component={TextInput}
                                    label="Ulangi Password"
                                />
                                <Form.Group widths="equal">
                                    <Field
                                        name="province"
                                        type="text"
                                        placeholder="Pilih provinsi kamu..."
                                        component={SelectInput}
                                        value={selectedProvince}
                                        options={provinceList}
                                        onChange={this.handleChangeProvince}
                                        label="Provinsi"
                                        search
                                        selection
                                    />
                                    <Field
                                        name="city"
                                        type="text"
                                        placeholder="Pilih kota kamu..."
                                        component={SelectInput}
                                        value={selectedCity}
                                        options={city}
                                        onChange={this.handleChangeCity}
                                        label="Kota/Kabupaten"
                                        search
                                        selection
                                    />
                                </Form.Group>
                                <Form.Group widths="equal">
                                    <Field
                                        name="subdistrict"
                                        type="text"
                                        placeholder="Pilih kecamatan kamu..."
                                        component={SelectInput}
                                        options={subdistricts}
                                        label="Kecamatan"
                                        search
                                        selection
                                    />
                                    {helpText ? (
                                        <div className="field">
                                            <label>Kode Pos</label>
                                            <input
                                                readOnly
                                                type="number"
                                                name="zip"
                                                placeholder="Kode pos kota kamu"
                                                value={zipCode ? zipCode : ""}
                                            />
                                            <span className="help-text">Kode pos terisi otomatis sesuai kota mu</span>
                                        </div>
                                    ) : (
                                        <Field
                                            name="zip"
                                            type="number"
                                            placeholder="Kode pos daerah mu..."
                                            value={zipCode}
                                            label="Kode pos"
                                            readOnly
                                            component={TextInput}
                                        />
                                    )}
                                </Form.Group>
                                <Field
                                    name="address"
                                    placeholder="Alamat rumah/pengiriman kamu..."
                                    component={TextArea}
                                    rows={3}
                                    label="Alamat"
                                />
                                <Field
                                    name="tele"
                                    type="number"
                                    placeholder="Nomor telepon kamu..."
                                    component={TextInput}
                                    label="Telepon"
                                />
                                {errorMessage && errorType && errorType === "register" && (
                                    <Message style={{ display: "block" }} error header="Oops!" content={errorMessage} />
                                )}
                                <Par className="sk-agreement">
                                    Dengan mendaftar di Zigzag, kamu dianggap telah menyetujui{" "}
                                    <Link to="/info/syarat-dan-ketentuan">Syarat & Ketentuan</Link> yang ada di Zigzag.
                                </Par>
                                <Form.Button type="submit" content="Submit" fluid className="mt2em" />
                            </Form>
                            <Div className="switching">
                                <Button to="/login" as={Link} basic className="link-btn mb2em">
                                    Sudah punya akun? Login di sini &nbsp; <Emoji label="palette" symbol="🎨" />
                                </Button>
                            </Div>
                        </Grid.Column>
                    </Grid>
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.email) errors.email = "Email nya kok kosong?"
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) errors.email = "Ini sih bukan email ya"
    if (!values.name) errors.name = "Nama nya kok kosong?"
    else if (values.name.length < 4) errors.name = "Yakin ini nama lengkap kamu, nih?"
    if (!values.password) errors.password = "Password nya kok kosong?"
    else if (values.password.length < 8) errors.password = "Minimal 8 karakter ya"
    if (!values.address) errors.address = "Alamat nya kok kosong?"
    if (values.password !== values.password2) errors.password2 = "Password nya kok gak match?"
    if (!values.tele) errors.tele = "Nomor telepon juga wajib diisi, ya"

    return errors
}

const mapState = ({ rajaOngkir, auth }) => {
    const errorMessage =
        auth.message && auth.message === existed ? "Kayak kenal emailnya, nih. Udah punya akun ya?" : auth.message
    const provinceList =
        rajaOngkir.provinces &&
        rajaOngkir.provinces.map(item => ({
            key: Number(item.province_id),
            value: Number(item.province_id),
            text: item.province
        }))
    const cityList =
        rajaOngkir.cities &&
        rajaOngkir.cities.map(city => ({
            key: Number(city.city_id),
            value: Number(city.city_id),
            province: Number(city.province_id),
            zip: Number(city.postal_code),
            text: city.city_name,
            description: city.type
        }))
    const subdistricts =
        rajaOngkir.subdistricts &&
        rajaOngkir.subdistricts.map(item => ({
            key: item.subdistrict_id,
            value: item.subdistrict_id,
            text: item.subdistrict_name,
            city: item.city_id
        }))

    return {
        loading: auth.loading,
        errorType: auth.errorType,
        authed: auth.authenticated,
        errorMessage,
        subdistricts,
        provinceList,
        cityList
    }
}

const actionList = { fetchCities, fetchProvinces, fetchProvince, signupUser, fetchSubdistricts }

export default reduxForm({
    form: "RegisterPartner",
    validate,
    enableReinitialize: true,
    destroyOnUnmount: false
})(
    connect(
        mapState,
        actionList
    )(withRouter(RegisterPartner))
)
