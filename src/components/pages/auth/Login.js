import React from "react"
import { Link } from "react-router-dom"
import { Grid, Form, Button, Header, Message, Divider, Container, Checkbox } from "semantic-ui-react"
import { Field, reduxForm } from "redux-form"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import { signinUser, signinUserViaFacebook } from "../../state/actions/authActions"
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props"
import TextInput from "../../form/TextInput"
import Emoji from "../../common/Emoji"
import Logo from "../../common/Logo"
import { api } from "../../../services/config"
import { mobile } from "../../common"

class Login extends React.Component {
    state = {
        fbToken: null,
        passwordVisible: false
    }

    responseFacebook = res => {
        // if (res && res.accessToken) {
        //     localStorage.setItem('token', res.accessToken)
        //     this.props.history.push('/')
        // }
        if (res) {
            console.log(res)
            localStorage.setItem("token", res.accessToken)
        }
    }

    handleSigninUser = data => {
        this.props.signinUser(data).then(() => {
            const token = localStorage.getItem("token")

            if (token && token !== undefined) {
                this.props.history.push("/")
            }
        })
    }

    handleShowPassword = () => {
        this.setState(prevState => ({ passwordVisible: !prevState.passwordVisible }))
    }

    render() {
        const { handleSubmit, loading, errorMessage, errorType } = this.props
        const passwordVisible = this.state.passwordVisible

        return (
            <React.Fragment>
                <Link to="/">
                    <Logo size="tiny" />
                </Link>
                <Grid stackable centered className="centered-grid main-form">
                    <Grid.Column width={mobile ? 14 : 8}>
                        <Header as="h2" className="form-header">
                            <Header.Content>
                                Login &nbsp; <Emoji label="thunder" symbol="⚡" />
                                <Header.Subheader>Kamu harus login dulu kalo mau juara kelas.</Header.Subheader>
                            </Header.Content>
                        </Header>
                        <Form loading={loading} onSubmit={handleSubmit(this.handleSigninUser)}>
                            <Field
                                name="email"
                                type="text"
                                placeholder="Masukkan email kamu..."
                                component={TextInput}
                                label="Email"
                                error={errorMessage}
                            />
                            <Field
                                name="password"
                                type={passwordVisible ? "text" : "password"}
                                placeholder="Masukkan password kamu..."
                                component={TextInput}
                                label="Password"
                                error={errorMessage}
                            />
                            <Form.Field>
                                <Checkbox label="Tampilkan password" tabIndex="-1" onChange={this.handleShowPassword} />
                            </Form.Field>
                            {errorMessage && errorMessage.length > 0 && (
                                <Message style={{ display: "block" }} error header="Oops!" content={errorMessage[0]} />
                            )}
                            <Button to="/forgot" as={Link} floated="right" basic className="link-btn mb2em">
                                Lupa password?
                            </Button>
                            <Form.Button type="submit" content="Submit" fluid />
                        </Form>
                        {/* <Divider horizontal> atau </Divider>
                        <Container textAlign="center">
                            <FacebookLogin
                                appId={api.FB}
                                fields="name,email,picture"
                                callback={this.responseFacebook}
                                version="3.1"
                                render={props => (
                                    <Button
                                        icon="facebook f"
                                        onClick={props.onClick}
                                        color="facebook"
                                        content="Login pake Facebook"
                                    />
                                )}
                            />
                        </Container> */}
                        <div className="switching">
                            <Button to="/register" as={Link} basic className="link-btn">
                                Belum punya akun? Daftar sekarang &nbsp;
                                <Emoji label="clouds" symbol="⛅" />
                            </Button>
                        </div>
                    </Grid.Column>
                </Grid>
            </React.Fragment>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.email) errors.email = "Email nya kok kosong?"
    // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) errors.email = "Ini sih bukan email ya"

    if (!values.password) errors.password = "Password nya kok kosong?"

    return errors
}

const mapState = ({ auth }) => ({
    loading: auth.loading,
    errorMessage: auth.message,
    errorType: auth.errorType,
    authenticated: auth.authenticated
})

const actionList = { signinUser, signinUserViaFacebook }

// prettier-ignore
export default reduxForm({ form: "Login", validate })(
    connect( mapState, actionList )(withRouter(Login))
)
