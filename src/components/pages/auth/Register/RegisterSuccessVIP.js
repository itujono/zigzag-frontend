import React from "react"
import { Link } from "react-router-dom"
import { Grid, Segment, Header, Container, Button } from "semantic-ui-react"
import { mobile } from "../../../common"

class RegisterSuccessVIP extends React.Component {
    componentDidMount() {
        if (!this.props.done) window.location.replace("/login")
    }

    render() {
        return (
            <Grid centered columns={mobile ? 1 : 3} className="centered-grid no-border">
                <Grid.Column>
                    <Segment padded="very" centered className="wizard-success">
                        <svg
                            width="133px"
                            height="133px"
                            viewBox="0 0 133 133"
                            version="1.1"
                            xmlns="http://www.w3.org/2000/svg"
                            style={{ marginBottom: "2.5em" }}
                        >
                            <g id="check-group" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <circle id="filled-circle" fill="#8732ff" cx="66.5" cy="66.5" r="54.5" />
                                <circle id="white-circle" fill="#FFFFFF" cx="66.5" cy="66.5" r="55.5" />
                                <circle id="outline" stroke="#8732ff" strokeWidth="4" cx="66.5" cy="66.5" r="54.5" />
                                <polyline id="check" stroke="#FFFFFF" strokeWidth="4" points="41 70 56 85 92 49" />
                            </g>
                        </svg>
                        <Container textAlign="center">
                            <Header as="h2" content="Terima kasih sudah mendaftar! 🌞" />
                            <p>
                                Kamu sudah berhasil mendaftar. Great! Silakan cek inbox kamu untuk mengaktifkan akun
                                kamu.
                            </p>
                            <p className="mb2em">
                                Namun, karena kamu milih untuk jadi member VIP Zigzag, kamu diwajibkan untuk membayar
                                biaya pendaftaran VIP member sebesar <span className="tosca">Rp 50.000,00</span> +{" "}
                                <strong>sejumlah kode unik</strong> yg dapat kamu lihat di email yang baru saja kami
                                kirim. <br />
                                <br /> P.S. Kalo emailnya gak masuk, jangan lupa cek folder spam nya juga ya.
                            </p>
                            <Button
                                icon="shopping bag"
                                as={Link}
                                to="/"
                                content="Balik ke Home"
                                className="btn-zigzag"
                            />
                        </Container>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

export default RegisterSuccessVIP
