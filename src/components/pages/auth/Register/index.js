import React from "react"
import { Link } from "react-router-dom"
import { Field, reduxForm } from "redux-form"
import TextInput from "../../../form/TextInput"
import SelectInput from "../../../form/SelectInput"
import RegisterSuccessVIP from "./RegisterSuccessVIP"
import RegisterSuccess from "./RegisterSuccess"
import UpgradeFirst from "./UpgradeFirst"
import NotFound from "../../../common/NotFound"
import { Switch, Route, withRouter } from "react-router-dom"
import { Grid, Form, Button, Message, Header, Segment, Radio, Container, Transition, Checkbox } from "semantic-ui-react"
import { connect } from "react-redux"
import { fetchCities, fetchProvinces, fetchProvince, fetchSubdistricts } from "../../../state/actions/rajaOngkirActions"
import { signupUser } from "../../../state/actions/authActions"
import { fetchCustomerServices } from "../../../state/actions/userActions"
import TextArea from "../../../form/TextArea"
import Emoji from "../../../common/Emoji"
import Logo from "../../../common/Logo"
import Posed, { PoseGroup } from "react-pose"
import { mobile, Div, PosedContainer, Formee, Par, Img } from "../../../common"

const textReguler = (
    <Container className="info" fluid>
        Dengan jadi Reguler member, kamu dapat:
        <ul>
            <li>Dapat belanja di sini</li>
            <li>Dapat update barang terbaru pertama kali</li>
            <li>Dapat pahala dan amal jariyah</li>
        </ul>
    </Container>
)

const textVIP = (
    <Container className="info" fluid>
        Dengan jadi VIP member, kamu dapat:
        <ul>
            <li>Semua kelebihan di Reguler</li>
            <li>Dapat diskon melimpah</li>
            <li>Dapat update barang terbaru pertama kali</li>
            <li>Dapat pahala dan amal jariyah</li>
        </ul>
    </Container>
)

const RouteContainer = Posed.div({
    enter: { opacity: 1, y: 0, delay: 200, beforeChildren: true },
    exit: { opacity: 0, y: 20 }
})

const existed = "Isian email sudah ada sebelumnya."

class Register extends React.Component {
    state = {
        helpText: false,
        selectedProvince: "",
        selectedCity: [],
        type: 0,
        selectedAccount: "reguler",
        done: false,
        passwordVisible: false
    }

    componentDidMount() {
        this.props.fetchProvinces()
        this.props.fetchCities()
        this.props.fetchCustomerServices()
    }

    handleSignup = data => {
        const cityList = this.renderCities()
        const { selectedCity, selectedProvince, selectedAccount } = this.state
        const zipCode = cityList && cityList.filter(city => city.value === selectedCity).map(city => city.zip)[0]
        const accountType = selectedAccount === "vip" ? 2 : 1
        const additional = {
            ...data,
            email: data.email.trim(),
            deposit: 0,
            zip: zipCode,
            acc_type: accountType,
            customer_service_id: data.cs
        }

        if (additional) {
            this.setState({ done: true }, () => {
                this.props.signupUser(additional, () => {
                    this.props.reset()
                    this.props.history.push(
                        selectedAccount === "reguler" ? "/register/success" : "/register/success/vip"
                    )
                })
            })
        }
    }

    renderCustomerServices = () => {
        return (
            this.props.cs &&
            this.props.cs
                // .sort(() => 0.57 - Math.random())
                .map(cs => {
                    const whatsapp = cs.whatsapp && cs.whatsapp !== null ? cs.whatsapp : "-"
                    const line = cs.line && cs.line !== null ? cs.line : "-"

                    return {
                        key: cs.name,
                        value: cs.id,
                        name: cs.name,
                        text: `😉 ${cs.name} - Whatsapp: ${whatsapp}  Line: ${line}`
                    }
                })
        )
    }

    renderProvinces = () => {
        const { provinces } = this.props
        const provinceList =
            provinces &&
            provinces.map(province => ({
                key: Number(province.province_id),
                value: Number(province.province_id),
                text: province.province
            }))

        return provinceList
    }

    renderCities = () => {
        const { cities } = this.props
        const cityList =
            cities &&
            cities.map(city => ({
                key: Number(city.city_id),
                value: Number(city.city_id),
                province: Number(city.province_id),
                zip: Number(city.postal_code),
                text: city.city_name,
                description: city.type
            }))

        return cityList
    }

    handleChangeProvince = (e, data) => {
        this.setState({ selectedProvince: data || null })
    }

    handleChangeCity = (e, data) => {
        this.setState({ selectedCity: data, helpText: true })
        this.props.fetchSubdistricts(data)
    }

    handleSelectAccount = selectedAccount => {
        this.setState({ selectedAccount })
    }

    handleShowPassword = e => {
        if (e || e.keyCode === 32) {
            this.setState(prevState => ({ passwordVisible: !prevState.passwordVisible }))
        }
    }

    render() {
        const { handleSubmit, errorMessage, errorType, location, subdistricts } = this.props
        const provinceList = this.renderProvinces()
        const cityList = this.renderCities()

        const { selectedProvince, selectedCity, selectedAccount, helpText, done, passwordVisible } = this.state

        const city = cityList && cityList.filter(city => city.province === selectedProvince)
        const zipCode = cityList && cityList.filter(city => city.value === selectedCity).map(city => city.zip)

        // console.log(this.renderCustomerServices())

        return (
            <Grid stackable centered>
                <PoseGroup>
                    <Grid.Column as={RouteContainer} width={16} key={location.pathname}>
                        <Link to="/">
                            <Logo as={Img} size="tiny" />
                        </Link>
                        <Switch>
                            <Route
                                exact
                                path="/register"
                                key="register"
                                render={() => (
                                    <React.Fragment>
                                        <Grid
                                            stackable
                                            centered
                                            columns={2}
                                            className="centered-grid main-form register"
                                        >
                                            <Grid.Column as={PosedContainer}>
                                                <Header as="h2" className="form-header">
                                                    <Header.Content as={Div}>
                                                        Register <Emoji label="clouds" symbol="⛅" />
                                                        <Header.Subheader>
                                                            Ayo jadi member Zigzag dan dapatkan kecupan hangat nya!
                                                        </Header.Subheader>
                                                    </Header.Content>
                                                </Header>
                                                <Form as={Formee} onSubmit={handleSubmit(this.handleSignup)}>
                                                    <Form.Group widths="equal">
                                                        <Field
                                                            name="email"
                                                            type="text"
                                                            placeholder="Masukkan email kamu..."
                                                            component={TextInput}
                                                            label="Email"
                                                        />
                                                        <Field
                                                            name="name"
                                                            type="text"
                                                            placeholder="Masukkan nama lengkap kamu..."
                                                            component={TextInput}
                                                            label="Nama Lengkap"
                                                        />
                                                    </Form.Group>
                                                    <Field
                                                        name="password"
                                                        type={passwordVisible ? "text" : "password"}
                                                        placeholder="Masukkan password pilihan kamu..."
                                                        component={TextInput}
                                                        label="Password"
                                                    />
                                                    <Form.Field>
                                                        <Checkbox
                                                            label="Tunjukkan password"
                                                            tabIndex="-1"
                                                            onChange={this.handleShowPassword}
                                                        />
                                                    </Form.Field>
                                                    <Field
                                                        name="password2"
                                                        type={passwordVisible ? "text" : "password"}
                                                        placeholder="Ulangi password kamu tadi..."
                                                        component={TextInput}
                                                        label="Ulangi Password"
                                                    />
                                                    <Header as="h5" content="Tipe Akun" />
                                                    <Segment.Group horizontal={!mobile} className="account-type">
                                                        <Segment
                                                            padded="very"
                                                            onClick={() => this.handleSelectAccount("reguler")}
                                                            className={selectedAccount === "reguler" ? "active" : null}
                                                            style={{
                                                                marginRight: mobile ? "initial" : "1.6em",
                                                                marginBottom: mobile ? "1em" : "initial"
                                                            }}
                                                        >
                                                            <Radio
                                                                label="Reguler"
                                                                onChange={this.handleSelectAccount}
                                                                checked={selectedAccount === "reguler"}
                                                            />
                                                            {textReguler}
                                                        </Segment>
                                                        <Segment
                                                            padded="very"
                                                            onClick={() => this.handleSelectAccount("vip")}
                                                            className={selectedAccount === "vip" ? "active" : null}
                                                        >
                                                            <Radio
                                                                label="VIP"
                                                                onChange={this.handleSelectAccount}
                                                                checked={selectedAccount === "vip"}
                                                            />
                                                            {textVIP}
                                                        </Segment>
                                                    </Segment.Group>
                                                    <Transition
                                                        visible={selectedAccount === "vip"}
                                                        animation="slide down"
                                                        duration={200}
                                                    >
                                                        <Message
                                                            info
                                                            icon="comments outline"
                                                            header="Kamu pilih VIP"
                                                            content="Kalo kamu pilih menjadi VIP member kami, maka kamu akan diwajibkan untuk membayar biaya pendaftaran sebesar Rp 50.000 setelah proses registrasi ini. Wajib belanja minimal 4 (empat) items (direset tiap awal bulan)"
                                                        />
                                                    </Transition>
                                                    <Field
                                                        name="cs"
                                                        type="text"
                                                        placeholder="Pilih CS yang kamu mau..."
                                                        component={SelectInput}
                                                        options={this.renderCustomerServices()}
                                                        label="Pilih CS kamu"
                                                        className="cs-option"
                                                        helpText="CS yang kamu pilih akan selalu menjadi orang pertama yang akan meng-assist kamu selama menggunakan website Zigzag. Bisa dibilang, mereka lah yang bakal jadi asisten pribadi kamu. Bila perlu, silakan catat nomor Whatsapp/Line nya."
                                                        search
                                                        selection
                                                    />
                                                    <Form.Group widths="equal">
                                                        <Field
                                                            name="province"
                                                            type="text"
                                                            placeholder="Pilih provinsi kamu..."
                                                            component={SelectInput}
                                                            value={selectedProvince}
                                                            options={provinceList}
                                                            onChange={this.handleChangeProvince}
                                                            label="Provinsi"
                                                            search
                                                            selection
                                                        />
                                                        <Field
                                                            name="city"
                                                            type="text"
                                                            placeholder="Pilih kota kamu..."
                                                            component={SelectInput}
                                                            value={selectedCity}
                                                            options={city}
                                                            onChange={this.handleChangeCity}
                                                            label="Kota/Kabupaten"
                                                            search
                                                            selection
                                                        />
                                                    </Form.Group>
                                                    <Form.Group widths="equal">
                                                        <Field
                                                            name="subdistrict"
                                                            type="text"
                                                            placeholder="Pilih kecamatan kamu..."
                                                            component={SelectInput}
                                                            options={subdistricts}
                                                            label="Kecamatan"
                                                            search
                                                            selection
                                                        />
                                                        {helpText ? (
                                                            <div className="field">
                                                                <label>Kode Pos</label>
                                                                <input
                                                                    readOnly
                                                                    type="number"
                                                                    name="zip"
                                                                    placeholder="Kode pos kota kamu"
                                                                    value={zipCode ? zipCode : ""}
                                                                />
                                                                <span className="help-text">
                                                                    Kode pos terisi otomatis sesuai kota mu
                                                                </span>
                                                            </div>
                                                        ) : (
                                                            <Field
                                                                name="zip"
                                                                type="number"
                                                                placeholder="Kode pos daerah mu..."
                                                                value={zipCode}
                                                                label="Kode pos"
                                                                readOnly
                                                                maxLength={99999}
                                                                component={TextInput}
                                                            />
                                                        )}
                                                    </Form.Group>
                                                    <Field
                                                        name="address"
                                                        placeholder="Alamat rumah/pengiriman kamu..."
                                                        component={TextArea}
                                                        rows={3}
                                                        label="Alamat"
                                                    />
                                                    <Field
                                                        name="tele"
                                                        type="number"
                                                        placeholder="Nomor telepon kamu..."
                                                        component={TextInput}
                                                        label="Telepon"
                                                    />
                                                    {errorMessage && errorType && errorType === "register" && (
                                                        <Message
                                                            style={{ display: "block" }}
                                                            error
                                                            header="Oops!"
                                                            content={errorMessage}
                                                        />
                                                    )}
                                                    <Par className="sk-agreement">
                                                        Dengan mendaftar di Zigzag, kamu dianggap telah menyetujui{" "}
                                                        <Link to="/info/syarat-dan-ketentuan">Syarat & Ketentuan</Link>{" "}
                                                        yang ada di Zigzag.
                                                    </Par>
                                                    <Form.Button
                                                        type="submit"
                                                        content="Submit"
                                                        fluid
                                                        className="mt2em"
                                                    />
                                                </Form>
                                                <Div className="switching">
                                                    <Button to="/login" as={Link} basic className="link-btn mb2em">
                                                        Sudah punya akun? Login di sini &nbsp;{" "}
                                                        <Emoji label="palette" symbol="🎨" />
                                                    </Button>
                                                </Div>
                                            </Grid.Column>
                                        </Grid>
                                    </React.Fragment>
                                )}
                            />
                            <Route
                                path="/register/success/vip"
                                key="success"
                                render={() => <RegisterSuccessVIP done={done} />}
                            />
                            <Route
                                path="/register/success"
                                key="successVip"
                                render={() => <RegisterSuccess done={done} />}
                            />
                            <Route path="/register/upgrade" key="upgrade" render={() => <UpgradeFirst done={done} />} />
                            <Route component={NotFound} />
                        </Switch>
                    </Grid.Column>
                </PoseGroup>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.email) errors.email = "Email nya kok kosong?"
    // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) errors.email = "Ini sih bukan email ya"
    if (!values.name) errors.name = "Nama nya kok kosong?"
    else if (values.name.length < 4) errors.name = "Yakin ini nama lengkap kamu, nih?"
    if (!values.password) errors.password = "Password nya kok kosong?"
    else if (values.password.length < 8) errors.password = "Minimal 8 karakter ya"
    if (!values.address) errors.address = "Alamat nya kok kosong?"
    if (values.password !== values.password2) errors.password2 = "Password nya kok gak match?"
    if (!values.tele) errors.tele = "Nomor telepon juga wajib diisi, ya"

    return errors
}

const mapState = ({ rajaOngkir, auth, user }) => {
    const errorMessage =
        auth.message && auth.message === existed ? "Kayak kenal emailnya, nih. Udah punya akun ya?" : auth.message
    const subdistricts =
        rajaOngkir.subdistricts &&
        rajaOngkir.subdistricts.map(item => ({
            key: item.subdistrict_id,
            value: item.subdistrict_id,
            text: item.subdistrict_name,
            city: item.city_id
        }))

    return {
        provinces: rajaOngkir.provinces,
        cities: rajaOngkir.cities,
        loading: auth.loading,
        errorType: auth.errorType,
        cs: user.cs,
        authed: auth.authenticated,
        errorMessage,
        subdistricts
    }
}

const actionList = { fetchCities, fetchProvinces, fetchProvince, signupUser, fetchCustomerServices, fetchSubdistricts }

export default reduxForm({
    form: "Register",
    validate,
    enableReinitialize: true,
    destroyOnUnmount: false
})(
    connect(
        mapState,
        actionList
    )(withRouter(Register))
)
