import React from "react"
import { Grid, Button, Header, Container, Image } from "semantic-ui-react"
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { upgradeAccount } from "../../../state/actions/userActions";
import Prompt from "../../../common/Prompt";
import DepoLanding from "../../../../images/depo-landing.png";


class UpgradeFirst extends React.Component {

    state = { prompt: false, received: true }

    handleCancel = () => this.setState({ prompt: false })

    handleShowPrompt = () => this.setState({ prompt: true })

    handleSubmitProposal = () => {
        this.setState({ prompt: false })
        this.props.upgradeAccount()
        this.props.history.push('/upgrade/sent')
    }

    render() {
        return (
            <Grid centered padded="very" className="upgrade">
                <Grid.Column width={6}>
                    <Button basic content="Kembali" icon="chevron left" onClick={this.props.history.goBack} />
                    <Container>
                        <Header as="h2" content="Daftar VIP" />
                        <div className="tc">
                            <p>
                                Enak kok jadi VIP member nya Zigzag. Ut cursus, felis sit amet congue sagittis, eros lacus dapibus metus, id faucibus tellus quam vitae ex. Phasellus vulputate libero at iaculis fermentum. Phasellus neque quam, condimentum nec elementum ut, tincidunt non mi. Donec purus lacus, auctor eget mi id, faucibus elementum ligula. Sed a aliquam lorem. Aliquam mauris augue, pellentesque non magna nec, rhoncus mattis augue. Cras ut arcu sit amet magna aliquet tristique. Donec at eros id eros suscipit accumsan. Proin sed mi in lacus pharetra malesuada sit amet sit amet leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus mi erat, ullamcorper et consectetur a, elementum eu sem. Nam efficitur id tortor sed consectetur.
                                <br /><br />
                                Duis et nisl urna. Duis nec fermentum erat. Aenean ac lorem suscipit quam iaculis consequat. Praesent molestie dictum ullamcorper. Fusce imperdiet faucibus ligula sed commodo. Donec ornare ultrices lorem, id posuere urna ullamcorper sit amet. Cras a mollis nulla. Duis ornare scelerisque ante vel pretium. Nullam sollicitudin, eros non aliquam porta, est felis suscipit lacus, in pulvinar turpis quam eget risus. Phasellus efficitur dui vitae est ornare egestas finibus sed libero. Quisque nec nisl molestie, consequat nibh id, hendrerit purus. Quisque in lacus sed dolor semper euismod. Ut ac commodo orci, ut pellentesque erat. Vestibulum ullamcorper sagittis lectus, nec sagittis neque vehicula in. Sed vel risus sit amet ante interdum consectetur. Maecenas quis vulputate lectus.
                                <br /><br />
                                Duis pretium nibh at lorem euismod accumsan. Sed id ante augue. Quisque eu orci eget nisl semper ornare. Nunc tristique ullamcorper efficitur. Vestibulum finibus, libero id condimentum hendrerit, nulla nunc ullamcorper justo, vel sodales odio sem sed dui. Praesent eget nisi arcu. Proin tincidunt, lacus quis vestibulum commodo, neque enim fermentum diam, vel ornare sapien nulla eget erat. Nam fringilla commodo sagittis. Vivamus vestibulum velit enim, ac vehicula eros placerat ac.
                                <br /><br />
                                Donec vel nisi in eros ullamcorper maximus nec in felis. Nam eu nunc bibendum, fermentum sapien sed, molestie enim. Praesent accumsan aliquet nulla ut cursus. Aliquam nec dapibus odio. Sed nec nibh interdum, pulvinar eros in, facilisis lorem. Phasellus vitae interdum arcu. Mauris mollis, turpis nec consequat ultricies, tortor urna laoreet lacus, quis suscipit ipsum mi in orci. Integer viverra malesuada felis, eget elementum nisi cursus vel. Praesent quis finibus sapien, quis convallis enim. Aenean mollis orci quam, vitae congue ligula maximus sit amet. Etiam sit amet massa non sem ullamcorper commodo a at libero. Proin ultrices, nibh in mattis fermentum, est ex gravida diam, ac euismod mauris nulla nec eros. Nullam aliquam tempus placerat. Aenean nec neque pretium, luctus sapien eu, fringilla risus. Aenean dictum eleifend lorem, sed varius risus fermentum vel. Donec pellentesque tortor in ornare porttitor.
                            </p>
                        </div>
                    </Container>
                    <Button className="btn-zigzag" content="Ya, saya setuju" icon="plus" onClick={this.handleShowPrompt} />
                    <Prompt open={this.state.prompt} confirm={this.handleSubmitProposal} onClose={this.handleCancel} yesText="Pastinya" header="Yakin ingin jadi VIP member Zigzag?">
                        Dengan ngeklik button ini, kamu dianggap udah menyetujui Syarat & Ketentuan yang telah dibuat Zigzag perihal VIP member ini. Apa kamu yakin untuk diproses lebih lanjut?
                    </Prompt>
                </Grid.Column>
                <Grid.Column width={6}>
                    <Image src={DepoLanding} />
                </Grid.Column>
            </Grid>
        )
    }
}


const mapState = ({ user }) => ({
    loading: user.loading
})

export default connect(mapState, { upgradeAccount })(withRouter(UpgradeFirst))