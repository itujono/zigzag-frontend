import React from "react";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { Button } from "semantic-ui-react";



class Facebook extends React.Component {

	responseFacebook = (res) => {
		console.log(res)
		window.location.replace('/')
	}


    render() {
		return (
			<FacebookLogin
				appId="544661822651706"
				fields="name,email,picture"
				callback={this.responseFacebook}
				render={renderProps => (
					<Button icon="facebook f" onClick={renderProps.onClick} color="facebook" content="Login pake Facebook" />
				)}
			/>
		)
	}
};

export default Facebook
