import React from "react"
import { Grid, Card, Header } from "semantic-ui-react"
import { Link, withRouter } from "react-router-dom"
import Emoji from "../common/Emoji"
import { imageCategory, mobile } from "../common"

const CategorySection = ({ categories }) => {
    const mainCategories = categories && categories.filter(cat => cat.parent === 0)

    return (
        <Grid stackable className="category-section">
            <Grid.Column>
                <Header as="h2" className="section-header">
                    <Header.Content>
                        Kategori Produk&nbsp; <Emoji label="purse" symbol="👛" />
                        <Header.Subheader>Mau belanja apa kamu hari ini?</Header.Subheader>
                    </Header.Content>
                </Header>
                <Card.Group itemsPerRow={mobile ? 3 : 5}>
                    {mainCategories &&
                        mainCategories.map(card => {
                            const toUrl = card.name.replace(" ", "-").toLowerCase()

                            return (
                                <Card
                                    as={Link}
                                    header={card.name}
                                    to={`/category/${toUrl}`}
                                    key={card.id}
                                    image={imageCategory + "/" + card.picture}
                                    {...card}
                                />
                            )
                        })}
                </Card.Group>
            </Grid.Column>
        </Grid>
    )
}

export default withRouter(CategorySection)
