import React from "react"
import { Form, Button } from "semantic-ui-react"
import { Field, reduxForm } from "redux-form"
import TextInput from "../../form/TextInput";


const ChangeEmail = ({ editing, onChangeEditing }) => {
    return (
        <Form>
            <Field type="text" name="changeEmail" component={TextInput} disabled={!editing} label="Ubah email" />
            {
                editing ? (
                    <div>
                        <Button type="submit" color="black" content="Ubah" onClick={() => onChangeEditing(true)} />
                        <Button content="Batal" onClick={() => onChangeEditing(false)} />
                    </div>
                ) :
                <Form.Button color="black" content="Ubah" onClick={() => onChangeEditing(true)} />
            }
        </Form>
    )
}

export default reduxForm({ form: 'ChangeEmail' })(ChangeEmail)