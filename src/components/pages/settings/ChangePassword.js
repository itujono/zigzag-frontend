import React from "react";
import { Form, Button } from "semantic-ui-react";
import { Field, reduxForm } from "redux-form";
import TextInput from "../../form/TextInput";

const ChangePassword = ({ editing, onChangeEditing }) => {
    return (
        <Form>
            <Field
                type="password"
                name="changePassword"
                value="Sembarang"
                component={TextInput}
                disabled={!editing}
                label="Ubah password"
            />
            {editing ? (
                <div>
                    <Button type="submit" color="black" content="Ubah" onClick={() => onChangeEditing(true)} />
                    <Button content="Batal" onClick={() => onChangeEditing(false)} />
                </div>
            ) : (
                <Form.Button color="black" content="Ubah" onClick={() => onChangeEditing(true)} />
            )}
        </Form>
    );
};

export default reduxForm({ form: "ChangePassword" })(ChangePassword);
