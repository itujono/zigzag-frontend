import React from "react"
import { Link } from "react-router-dom"
import { Field, reduxForm } from "redux-form"
import TextInput from "../../form/TextInput"
import SelectInput from "../../form/SelectInput"
import DateInput from "../../form/DateInput"
import { banks, mobile } from "../../common"
import { Grid, Form, Button, Header, Checkbox, Image, Message, Segment, Container, Dropdown } from "semantic-ui-react"
import { connect } from "react-redux"
import { fetchDeposits, confirmDeposit, removeEntireError } from "../../state/actions/depositActions"
import { fetchBankAccount } from "../../state/actions/orderActions"
import moment from "moment"

const ulStyle = {
    paddingLeft: 15
}

class DepositConfirmation extends React.Component {
    state = { received: false, file: null, imagePreview: "" }

    componentDidMount() {
        this.props.fetchDeposits()
        this.props.fetchBankAccount()
    }

    componentWillUnmount() {
        localStorage.removeItem("kodeOrder")
        if (this.props.error) this.props.removeEntireError()
    }

    renderKodeOrder = () => {
        const deposits = this.props.deposits

        if (deposits) {
            return deposits
                .sort((a, b) => b.id - a.id)
                .map((depo, idx) => {
                    if (idx === 0) {
                        return {
                            key: depo.id,
                            text: depo.kode_order,
                            value: depo.kode_order,
                            description: "Terbaru"
                        }
                    }

                    return {
                        key: depo.id,
                        text: depo.kode_order,
                        value: depo.kode_order
                    }
                })
        }
    }

    handleImageChange = e => {
        this.setState({ file: e.target.files[0] })
    }

    handleSubmitForm = data => {
        const kodeOrder = localStorage.getItem("kodeOrder") && JSON.parse(localStorage.getItem("kodeOrder"))
        const { file, kode_order } = this.state
        let formData = new FormData()
        formData.append("evidence", file)
        formData.append("kode_order", kode_order || kodeOrder)
        formData.append("bank", data.bank)
        formData.append("sender_bank", data.sender_bank)
        formData.append("received_bank", data.received_bank)
        formData.append("total", data.total)
        formData.append("transfer_date", moment(data.transfer_date).format("Y-M-DD"))

        if (formData && formData !== null) {
            this.props.confirmDeposit(formData, () => {
                this.setState({ received: true })
            })
        }
    }

    handleChange = (e, { name, value }) => {
        this.setState({ [name]: value })
    }

    handleConfirmationSubmit = () => this.setState({ received: true })

    render() {
        const { handleSubmit, loading, error, bankOptions } = this.props
        const { imagePreview, file, kode_order } = this.state
        const kodeOrder = localStorage.getItem("kodeOrder") && JSON.parse(localStorage.getItem("kodeOrder"))
        const err = error()

        const errorMessage = err && (
            <ul style={ulStyle}>
                {err.map(item => (
                    <li key={item}>{item}</li>
                ))}
            </ul>
        )

        console.log("Hello baru!")

        if (this.state.received) {
            return (
                <Grid centered columns={mobile ? 1 : 3} className="centered-grid no-border">
                    <Grid.Column>
                        <Segment padded="very" centered className="wizard-success">
                            <svg
                                width="133px"
                                height="133px"
                                viewBox="0 0 133 133"
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                style={{ marginBottom: "2.5em" }}
                            >
                                <g id="check-group" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <circle id="filled-circle" fill="#8732ff" cx="66.5" cy="66.5" r="54.5" />
                                    <circle id="white-circle" fill="#FFFFFF" cx="66.5" cy="66.5" r="55.5" />
                                    <circle
                                        id="outline"
                                        stroke="#8732ff"
                                        strokeWidth="4"
                                        cx="66.5"
                                        cy="66.5"
                                        r="54.5"
                                    />
                                    <polyline id="check" stroke="#FFFFFF" strokeWidth="4" points="41 70 56 85 92 49" />
                                </g>
                            </svg>
                            <Container textAlign="center">
                                <Header as="h2" content="Konfirmasi deposit selesai!" />
                                <p className="mb2em">
                                    Oke, terima kasih udah ngelakuin konfirmasi deposit. Akan segera kami validasi.
                                    Silakan duduk tenang, and keep shopping all the way! <br /> <br /> P.S. Kalo
                                    emailnya gak masuk, jangan lupa cek folder spam nya juga ya.
                                </p>
                                <Button
                                    className="btn-zigzag"
                                    content="Kembali ke Home"
                                    onClick={() => window.location.replace("/")}
                                />
                            </Container>
                        </Segment>
                    </Grid.Column>
                </Grid>
            )
        }

        return (
            <Grid stackable centered columns={2} className="centered-grid main-form">
                <Grid.Column>
                    <Header as="h2" className="form-header">
                        <Header.Content>
                            Konfirmasi Pembayaran Deposit
                            <Header.Subheader>
                                Silakan masukkan data-data di bawah ini, agar deposit kamu bisa segera diproses.
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    <Form
                        loading={loading}
                        encType="multipart/form-data"
                        onSubmit={handleSubmit(this.handleSubmitForm)}
                    >
                        <Form.Field>
                            <label>Kode order</label>
                            <Dropdown
                                name="kode_order"
                                placeholder="Masukkan kode order kamu..."
                                options={this.renderKodeOrder()}
                                defaultValue={kodeOrder}
                                onChange={this.handleChange}
                                search
                                selection
                            />
                        </Form.Field>
                        <Form.Group>
                            <Field
                                name="bank"
                                type="text"
                                placeholder="Nama bank yang kamu gunakan sebagai pengirim"
                                component={TextInput}
                                label="Bank pengirim"
                                width={4}
                            />
                            <Field
                                name="sender_bank"
                                type="text"
                                placeholder="Masukkan nomor rekening kamu (pengirim)"
                                component={TextInput}
                                maxLength={13}
                                label="Nomor rekening pengirim"
                                width={12}
                            />
                        </Form.Group>
                        <Field
                            name="received_bank"
                            type="text"
                            placeholder="Nomor rekening Zigzag yang kamu transfer"
                            component={SelectInput}
                            options={bankOptions}
                            label="Nomor rekening yang dituju"
                            search
                            selection
                        />
                        <Form.Group>
                            <Field
                                name="total"
                                type="text"
                                placeholder="Tidak perlu pakai titik/koma. Benar: 50000 - Salah: 50.000"
                                component={TextInput}
                                width={8}
                                label="Nominal yang ditransfer (Rp)"
                            />
                            <Field
                                name="transfer_date"
                                todayButton="Hari ini"
                                type="text"
                                placeholder="Ditransfer pada tanggal..."
                                component={DateInput}
                                width={8}
                                label="Tanggal transfer"
                            />
                        </Form.Group>
                        <Form.Field>
                            <label>Upload Bukti</label>

                            {file && imagePreview !== "" ? (
                                <React.Fragment>
                                    <Image src={imagePreview} size="medium" style={{ marginBottom: "1em" }} />
                                    <Button
                                        icon="picture"
                                        className="btn-zigzag"
                                        content="Ganti"
                                        onClick={() => this.setState({ file: null, imagePreview: "" })}
                                    />
                                </React.Fragment>
                            ) : (
                                <div className="upload-btn-wrapper edit-img-wrapper wizard">
                                    <input
                                        type="file"
                                        name="evidence"
                                        style={{ height: "100%", cursor: "pointer" }}
                                        // accept=".png, .jpg, .jpeg"
                                        onChange={this.handleImageChange}
                                    />
                                </div>
                            )}
                        </Form.Field>

                        {err && err && (
                            <Message style={{ display: "block" }} error header="Wah!" content={errorMessage} />
                        )}

                        <Button
                            type="submit"
                            content="Submit"
                            disabled={(file === null && !kode_order) || !file}
                            fluid
                            className="mt2em btn-zigzag"
                        />
                    </Form>
                    <div className="switching">
                        <Button to="/" as={Link} basic className="link-btn mb2em">
                            Kembali ke Home
                        </Button>
                    </div>
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (!values.kode_order) errors.kode_order = "Kode order nya kok kosong?"
    if (!values.bank) errors.bank = "Nama bank nya juga gak boleh kosong ya"
    if (!values.sender_bank) errors.sender_bank = "Nomor rekening kamu sebagai pengirim wajib diisi loh ya"
    if (!values.total) errors.total = "Berapa yang kamu transfer?"
    else if (values.total.includes(".") || values.total.includes(","))
        errors.total = "Gak perlu pake titik atau koma ya"
    if (!values.received_bank) errors.received_bank = "Kirim ke rekening kami yang mana?"
    if (!values.transfer_date) errors.transfer_date = "Kapan transfer nya ini juga wajib dikasihtau ya"

    return errors
}

const mapState = ({ deposit, order }) => {
    const bankOptions =
        order.bankAccount &&
        order.bankAccount.map(item => ({
            key: item.id,
            text: item.bank_account + " " + item.number + " " + "a.n." + " " + item.under_name,
            value: item.bank_account + " " + item.number
        }))

    const error = () => {
        let err = deposit.error
        if (err) {
            for (let i in err) {
                return err[i]
            }
        }
    }

    const deposits = deposit.deposits && deposit.deposits.filter(item => item.status && item.status.status_id !== 4)

    return {
        loading: deposit.loading,
        bankOptions,
        deposits,
        error
    }
}

const actionList = { fetchDeposits, confirmDeposit, fetchBankAccount, removeEntireError }

// prettier-ignore
export default reduxForm({ form: "DepositConfirmation", validate, destroyOnUnmount: false, enableReinitialize: true })( connect( mapState, actionList )(DepositConfirmation) )
