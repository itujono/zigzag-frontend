import React from "react"
import { Grid, Button, Header, Container, Form, Image } from "semantic-ui-react"
import { Field, reduxForm } from "redux-form"
import { Link, withRouter } from "react-router-dom"
import TextInput from "../../form/TextInput"
import { connect } from "react-redux"
import { saveDeposit } from "../../state/actions/depositActions"
import DepoLanding from "../../../images/depo-landing.png"

const par = {
    color: "#999"
}

class DepositPage extends React.Component {
    state = { show: false }

    handleCancel = () => this.setState({ show: false })

    handleShowForm = () => this.setState({ show: true })

    handleSubmitDeposit = value => {
        if (value) {
            this.props.saveDeposit({ total: value.total }, () => {
                this.props.change("total", "")
                this.props.history.push("/deposit-sent")
            })
        }
    }

    render() {
        const { handleSubmit, pristine, loading, user } = this.props

        return (
            <Grid stackable centered padded="very" columns={2} className="deposit main-form">
                <Grid.Column>
                    <Button basic content="Kembali" icon="chevron left" onClick={this.props.history.goBack} />
                    {!this.state.show ? (
                        <React.Fragment>
                            <Container>
                                <Header as="h2" content="Deposit di Zigzag" />
                                <p>
                                    Deposit yang hakiki juga belum ada yang tau gimana, namun kita sebagai manusia
                                    diciptakan untuk saling melengkapi dan berenang ke tepian. Menunggu hujan tapi kalo
                                    gak suka ujan yaa percuma aja, sama kayak kamu makan ikan asin tapi ikan nya bukan
                                    main.
                                </p>
                                <p>
                                    Bagaimana cara nya kita bisa makan nasi pake rendang tanpa mengetahui bahwa Saipul
                                    Jamil adalah orang India? Tentu tak mungkin, dan ini tak baik bagi kesehatan
                                    masyarakat setempat, namun apa daya hujan di malam hari bukan lah pilihan anak-anak
                                    Tiban Koperasi. Terima kasih.
                                </p>
                            </Container>
                            {!user ? (
                                <div>
                                    <p style={par}>Tapi pertama-tama, login dulu ya</p>
                                    <Button as={Link} to="/login" className="btn-zigzag" content="Login" icon="plus" />
                                </div>
                            ) : (
                                <Button
                                    className="btn-zigzag"
                                    content="Deposit sekarang"
                                    icon="plus"
                                    onClick={this.handleShowForm}
                                />
                            )}
                        </React.Fragment>
                    ) : (
                        <Container>
                            <Header as="h2" content="Deposit di Zigzag" />
                            <Form loading={loading} onSubmit={handleSubmit(this.handleSubmitDeposit)}>
                                <Field
                                    name="total"
                                    type="number"
                                    placeholder="Ketik nominal nya..."
                                    label="Nominal deposit (Rp)"
                                    helpText="Minimal jumlah deposit Rp 50.000,00"
                                    component={TextInput}
                                />
                                <Button type="submit" content="Deposit" icon="plus" disabled={pristine} /> &nbsp; &nbsp;
                                <Button content="Batal" basic className="link-btn" onClick={this.handleCancel} />
                            </Form>
                        </Container>
                    )}
                </Grid.Column>
                <Grid.Column>
                    <Image src={DepoLanding} />
                </Grid.Column>
            </Grid>
        )
    }
}

const validate = values => {
    let errors = {}

    if (values.total < 50000) errors.total = "Minimal 50.000 ya"

    return errors
}

const mapState = ({ deposit, user }) => ({
    loading: deposit.loading,
    user: user.user
})

// prettier-ignore
export default reduxForm({ form: "Deposit", validate })(
    connect( mapState, { saveDeposit } )(withRouter(DepositPage))
)
