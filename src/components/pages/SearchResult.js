import React from "react"
import { Grid, Header, Item, Segment, Form, Input } from "semantic-ui-react"
import { withRouter } from "react-router-dom"
import { searchProducts, removeProductError } from "../state/actions/productActions"
import { connect } from "react-redux"
import { imageURL, priceType, idKoko, mobile } from "../common"
import NoData from "../common/NoData"

const { vip, member, partner } = priceType

class SearchResult extends React.Component {
    componentDidMount() {
        this.handleFocus()
    }

    componentWillUnmount() {
        this.props.removeProductError(this.props.error)
    }

    handleChange = (e, { value }) => this.setState({ keyword: value })

    handleSubmitSearch = () => {
        const { keyword } = this.state
        if (keyword && keyword.length > 1) {
            this.props.removeProductError(this.props.error)
            this.props.searchProducts(keyword)
        }
    }

    handleRedirect = item => {
        this.props.history.push(`/product/${item.id}-${item.name}`)
    }

    handleRef = c => (this.inputRef = c)

    handleFocus = () => this.inputRef.focus()

    render() {
        const { results, loading, error, user } = this.props

        return (
            <Grid
                centered
                columns={mobile ? 1 : 2}
                stackable
                className={loading ? "search-result loading" : "search-result"}
            >
                <Grid.Column>
                    <Segment basic>
                        <Header>Mau nyari apa?</Header>
                        <Form style={{ marginBottom: 0 }} onSubmit={this.handleSubmitSearch}>
                            <Input
                                fluid
                                name="keyword"
                                onChange={this.handleChange}
                                icon={{ name: "search", link: true, onClick: this.handleSubmitSearch }}
                                placeholder="Cari apa saja..."
                                style={{ width: mobile ? "100%" : "inherit" }}
                                ref={c => (this.inputRef = c)}
                            />
                        </Form>
                        {error && error !== null ? (
                            <NoData image empty noHeader message={error} />
                        ) : (
                            <Item.Group unstackable link divided>
                                {results &&
                                    results.map(item => {
                                        const priceAll = item.price
                                            .filter(price => {
                                                if (user) {
                                                    if (user.id === idKoko) return price.price_type === member
                                                    else {
                                                        if (user.acc_type === 1) return price.price_type === member
                                                        else if (user.acc_type === 2) return price.price_type === vip
                                                        else return price.price_type === partner
                                                    }
                                                } else return price.price_type === member
                                            })
                                            .map(price => price.price)[0]

                                        const moreDetail = item.product_more_detail

                                        const colors =
                                            item.out_of_stock === false &&
                                            Object.keys(moreDetail).length > 0 &&
                                            Object.entries(moreDetail).map(([color, details]) => {
                                                if (!color || !details) return "-"

                                                return {
                                                    color,
                                                    size: details && details.length > 0 ? details[0].size : "-"
                                                }
                                            })

                                        return (
                                            <Item onClick={() => this.handleRedirect(item)} key={item.id}>
                                                <Item.Image size="tiny" src={imageURL + "/" + item.picture.image} />
                                                <Item.Content>
                                                    <Item.Header as="a">
                                                        {item.name} &middot; Rp {priceAll}
                                                    </Item.Header>
                                                    <Item.Extra>
                                                        Kategori: <strong>{item.categories.name}</strong> <br />
                                                        {item.out_of_stock === false ? (
                                                            <div>
                                                                Warna tersedia:{" "}
                                                                <strong>
                                                                    {colors.length > 1
                                                                        ? colors.map(item => item.color).join(", ")
                                                                        : colors.map(item => item.color) || "-"}
                                                                </strong>{" "}
                                                                <br />
                                                                {/* {item.categories.id === 2 && (
                                                                    <span>
                                                                        Size tersedia:{" "}
                                                                        <strong>
                                                                            {colors.length > 1
                                                                                ? colors
                                                                                      .map(item => item.size)
                                                                                      .join(", ")
                                                                                : colors.map(item => item.size) || "-"}
                                                                        </strong>
                                                                    </span>
                                                                )} */}
                                                            </div>
                                                        ) : (
                                                            <span style={{ color: "pink" }}>Stok lagi habis</span>
                                                        )}
                                                    </Item.Extra>
                                                </Item.Content>
                                            </Item>
                                        )
                                    })}
                            </Item.Group>
                        )}{" "}
                        <br /> <br />
                        {results && results.length > 4 && (
                            <Header
                                as="h5"
                                content="Dah habis hasil pencarian nya"
                                textAlign="center"
                                color="grey"
                                disabled
                            />
                        )}
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ products, user }) => ({
    results: products.searchResults,
    user: user.user,
    loading: products.loading,
    error: products.error
})

// prettier-ignore
export default withRouter(connect( mapState, { searchProducts, removeProductError } )(SearchResult))
