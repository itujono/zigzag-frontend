import React from "react"
import { Grid, Header, Card, Menu } from "semantic-ui-react"
import { mobile, tablet } from "../common"
import ProductItem from "../common/ProductItem"
import Emoji from "../common/Emoji"
import NoData from "../common/NoData"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { fetchCategories, fetchProductsByCategory } from "../state/actions/productActions"
import { fetchUser } from "../state/actions/userActions"

class CategoryPage extends React.Component {
    state = { activeItem: "Semua Bag" }

    componentDidMount() {
        this.props.fetchCategories()
        this.props.fetchProductsByCategory(this.props.categoryId)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.categoryId !== this.props.categoryId) {
            this.props.fetchProductsByCategory(this.props.categoryId)
        }
    }

    handleExpand = () => this.setState({ expanded: true })

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
        const { user, loading, tasCats, category, productByCategory } = this.props
        const { activeItem } = this.state
        const catName = category && category.split(" ").join("-")

        console.log("Gurindam Terus!")

        return (
            <Grid stackable centered className={`category-page ${loading ? "loading" : ""}`}>
                <Grid.Column width={3}>
                    <Header as="h1" className="section-header">
                        <Header.Content>
                            {catName}
                            <Header.Subheader>
                                Belanja {catName.toLowerCase()} paling populer di sini!
                                <div style={{ marginTop: ".5em" }}>
                                    <Emoji label="lightning" symbol="⚡" />
                                </div>
                            </Header.Subheader>
                        </Header.Content>
                    </Header>
                    {category && category === "tas" && (
                        <Menu secondary vertical style={{ marginTop: "2em" }}>
                            {tasCats &&
                                tasCats.map(cat => (
                                    <Menu.Item
                                        key={cat.id}
                                        active={activeItem === cat.name}
                                        onClick={this.handleItemClick}
                                        name={cat.name}
                                    />
                                ))}
                        </Menu>
                    )}
                </Grid.Column>
                <Grid.Column width={12}>
                    <Card.Group itemsPerRow={mobile ? 2 : tablet ? 3 : 4}>
                        {productByCategory && productByCategory.length < 1 ? (
                            <NoData image empty message="Belum ada produk nya buat kategori ini" />
                        ) : (
                            productByCategory &&
                            productByCategory.map(product => (
                                <ProductItem
                                    as={Card}
                                    productId={product.product_id}
                                    loading={loading}
                                    key={product.product_id}
                                    user={user ? user : ""}
                                    product={product}
                                    noLazy
                                />
                            ))
                        )}
                    </Card.Group>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapState = ({ products, user }, ownProps) => {
    const category = ownProps.match.params.name
    const tasCat = products.categories && products.categories.filter(cat => cat.parent === 5)
    const categoryId =
        products.categories &&
        products.categories.filter(cat => cat.name.toLowerCase() === category).map(cat => cat.id)[0]

    return {
        products: products.products,
        user: user.user,
        categories: products.categories,
        loading: products.loading,
        productByCategory: products.productByCategory && products.productByCategory,
        tasCats:
            tasCat &&
            [{ id: "all", name: "Semua Bag", parent: 5, picture: "" }, ...tasCat].filter(item => item.parent === 5),
        category,
        categoryId
    }
}

// prettier-ignore
export default connect( mapState, { fetchUser, fetchCategories, fetchProductsByCategory } )(withRouter(CategoryPage))
