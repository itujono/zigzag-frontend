import React from "react"
import { Grid, Header, Card, Menu } from "semantic-ui-react"
import Emoji from "../common/Emoji";
import { Link, Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import { fetchProducts, fetchCategories } from "../state/actions/productActions"
import { fetchUser } from "../state/actions/userActions";



class SubcategoryPage extends React.Component {

	componentDidMount() {
		this.props.fetchProducts()
		this.props.fetchCategories()
		this.props.fetchUser()
	}

    render() {
        const { products, user, categories } = this.props
        
        console.log(this.props.match.params)

        return (
			<Grid>
				<Grid.Column width={3}>
					<Header as="h1" className="section-header category-page">
						<Header.Content>
							Header
							<Header.Subheader>
								Belanja keren heheh paling populer di sini!
								<div style={{ marginTop: '.5em' }}><Emoji label="lightning" symbol="⚡" /></div>
							</Header.Subheader>
						</Header.Content>
					</Header>
				</Grid.Column>
				<Grid.Column width={13}>
					<Card.Group itemsPerRow={4}>
                        Holaaaa
					</Card.Group>
				</Grid.Column>
			</Grid>
		)
    }
}


const mapState = ({ products, user }, ownProps) => {

    const categoryId = ownProps.match.params.id

    let category = {}

    if (products && categoryId) {
        category = products.filter(product => product.categories_id === categoryId)[0]
    }

    return {
        category,
        products: products.products,
        user: user.user,
        categories: products.categories
    }
}

export default connect(mapState, { fetchProducts, fetchUser, fetchCategories })(SubcategoryPage);
