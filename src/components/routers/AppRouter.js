import React, { Fragment } from "react"
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom"
import Header from "../Header/Header"
import HomePage from "../pages/HomePage"
import AboutPage from "../pages/AboutPage"
import RefundPage from "../pages/RefundPage"
import Footer from "../Footer/Footer"
import Login from "../../components/pages/auth/Login"
import Forgot from "../../components/pages/auth/Forgot"
import Register from "../pages/auth/Register/index"
import ForgotSent from "../../components/pages/auth/ForgotSent"
import AccountActive from "../../components/pages/auth/AccountActive"
import EnterNewPassword from "../../components/pages/auth/EnterNewPassword"
import PasswordChanged from "../../components/pages/auth/PasswordChanged"
import ProductDetail from "../common/ProductDetail"
import ProductDetailTemp from "../common/ProductDetail.1"
import NotFound from "../common/NotFound"
import Profile from "../pages/profile/Profile"
import Logout from "../pages/auth/Logout"
import Deposit from "../pages/deposit/Deposit"
import Checkout from "../pages/checkout"
import PaymentConfirmation from "../pages/checkout/PaymentConfirmation"
import CategoryPage from "../pages/CategoryPage"
import WithAuth from "../common/WithAuth"
import WithNoAuth from "../common/WithNoAuth"
import DepositSent from "../pages/deposit/DepositSent"
import DepositConfirmation from "../pages/deposit/DepositConfirmation"
import CartPage from "../pages/checkout/CartPage"
import Upgrade from "../pages/upgrade"
import ScrollToTop from "../common/ScrollToTop"
import Ongkir from "../pages/Ongkir"
import BannerPage from "../pages/BannerPage"
import ProductsPage from "../pages/ProductsPage"
import InfoPage from "../pages/InfoPage"
import SearchResult from "../pages/SearchResult"
import RegisterPartner from "../pages/auth/RegisterPartner"

const SpecialRoute = ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={matchProps => (
                <Fragment>
                    <Header />
                    <div className="main-wrapper">
                        <Component {...matchProps} />
                    </div>
                    <Footer />
                </Fragment>
            )}
        />
    )
}

export const AppRouter = () => {
    return (
        <BrowserRouter>
            <ScrollToTop>
                <Switch>
                    <SpecialRoute path="/" component={HomePage} exact />
                    <SpecialRoute path="/about" component={AboutPage} />
                    <SpecialRoute path="/product/:id-:name" component={ProductDetail} />
                    <SpecialRoute path="/temp/product/:id-:name" component={ProductDetailTemp} />
                    <SpecialRoute path="/category/:name" component={CategoryPage} />
                    <SpecialRoute path="/p/:name" component={ProductsPage} />
                    <SpecialRoute path="/info/:name" component={InfoPage} />
                    <SpecialRoute path="/profile" component={WithAuth(Profile)} />
                    <SpecialRoute path="/cart" component={CartPage} />
                    <SpecialRoute path="/checkout" component={Checkout} />
                    <SpecialRoute path="/order-confirmation" component={WithAuth(PaymentConfirmation)} />
                    <SpecialRoute path="/promo/:title" component={BannerPage} />
                    <SpecialRoute path="/search/results" component={SearchResult} />
                    <Route path="/login" component={WithNoAuth(Login)} />
                    <Route path="/register/p/partner" component={WithNoAuth(RegisterPartner)} />
                    <Route path="/register" component={WithNoAuth(Register)} />
                    <Route path="/forgot-sent" component={WithNoAuth(ForgotSent)} />
                    <Route path="/forgot" component={WithNoAuth(Forgot)} />
                    <Route path="/account-active" component={WithNoAuth(AccountActive)} />
                    <Route path="/new-password" component={WithNoAuth(EnterNewPassword)} />
                    <Route path="/password-changed" component={WithNoAuth(PasswordChanged)} />
                    <Route path="/logout" component={Logout} />
                    <Route path="/deposit-sent" component={WithAuth(DepositSent)} />
                    <Route path="/deposit-confirmation" component={WithAuth(DepositConfirmation)} />
                    <Route path="/deposit" component={Deposit} />
                    <Route path="/upgrade" component={Upgrade} />
                    <Route path="/account/active" component={AccountActive} />
                    <Route path="/refund" component={WithAuth(RefundPage)} />
                    <Route path="/ongkir" component={Ongkir} />
                    <Route component={NotFound} />
                </Switch>
            </ScrollToTop>
        </BrowserRouter>
    )
}
