import React, { Component } from "react"
import { Search } from "semantic-ui-react"
import { withRouter } from "react-router-dom"
import { formatter, imageURL, priceType, idKoko } from "../common"

const { promo, vip, member, partner } = priceType

class Searchable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false
        }
    }

    componentDidMount() {
        this.resetSearch()
    }

    resetSearch = () => this.setState({ value: "", isLoading: false })

    handleSearchChange = (e, { value }) => {
        const { products, user } = this.props
        this.setState({ isLoading: true, value })

        const search = value.toLowerCase()
        const result =
            products &&
            products
                .filter(product => product.name.toLowerCase().indexOf(search) > -1)
                .map(product => {
                    const priceTemp = product.price
                    const picParsed = product.picture && product.picture
                    const image = imageURL + "/" + picParsed[0].image

                    const promoPrice =
                        product &&
                        priceTemp
                            .filter(price => {
                                if (product.promo && product.promo === 1) return price.price_type === promo
                            })
                            .map(price => price.price)[0]

                    const priceAll =
                        product &&
                        priceTemp
                            .filter(price => {
                                if (user) {
                                    if (user.id === idKoko) return price.price_type === member
                                    else {
                                        if (user.acc_type === 1) return price.price_type === member
                                        else if (user.acc_type === 2) return price.price_type === vip
                                        else return price.price_type === partner
                                    }
                                } else return price.price_type === member
                            })
                            .map(price => price.price)[0]

                    const actualPrice = promoPrice ? promoPrice : priceAll

                    return {
                        id: product.product_id,
                        title: product.name,
                        code: product.code,
                        price: formatter.format(actualPrice),
                        image
                    }
                })
        this.setState({ isLoading: false, result, value })
    }

    handleResultSelect = (e, { result }) => {
        const title = result.title
            .split(" ")
            .join("-")
            .toLowerCase()

        if (result) {
            this.props.onCloseModal()
            this.resetSearch()
            this.props.history.push(`/product/${result.id}-${title}`)
        }
    }

    render() {
        return (
            <Search
                fluid
                aligned="center"
                loading={this.state.isLoading || !this.props.products}
                placeholder="Cari produk..."
                onResultSelect={this.handleResultSelect}
                onSearchChange={this.handleSearchChange}
                value={this.state.value}
                results={this.state.result}
                className="search-fullscreen"
                onBlur={this.resetSearch}
                minCharacters={1}
                noResultsMessage="Coba cari lagi..."
                selectFirstResult
                {...this.props}
            />
        )
    }
}

export default withRouter(Searchable)
