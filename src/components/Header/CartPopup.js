import React from "react"
import { Grid, Header, List, Image, Button, Segment } from "semantic-ui-react"
import { formatter, imageURL, priceType, idKoko, photoURL } from "../common"
import { Link } from "react-router-dom"

const { promo, member, vip, partner } = priceType

const CartPopup = ({ carts, user, total }) => {
    if (carts && carts.length < 1) return "Belum ada cart nih kamu. Belanja dong, ah!"

    const cartsConditional = carts && carts.length > 4 ? carts.slice(0, 4) : carts

    return (
        <Grid className="cart-popup">
            <Grid.Column>
                <Header as="h4" content="Cart kamu" />
                <List relaxed="very" divided>
                    {cartsConditional.map(cart => {
                        const priceTemp = cart.price && JSON.parse(cart.price)
                        const picParsed = cart.picture && cart.picture
                        const picture =
                            picParsed && picParsed !== "" && picParsed !== null
                                ? photoURL + "/product/" + picParsed
                                : "https://react.semantic-ui.com/images/avatar/small/steve.jpg"

                        const promoPrice =
                            priceTemp &&
                            priceTemp
                                .filter(price => {
                                    if (cart.promo && cart.promo === 1) return price.price_type === promo
                                })
                                .map(price => price.price)[0]

                        const priceAll =
                            cart &&
                            priceTemp
                                .filter(price => {
                                    if (user) {
                                        if (user.id === idKoko) return price.price_type === member
                                        else {
                                            if (user.acc_type === 1) return price.price_type === member
                                            else if (user.acc_type === 2) return price.price_type === vip
                                            else return price.price_type === partner
                                        }
                                    } else return price.price_type === member
                                })
                                .map(price => price.price)[0]

                        const actualPrice = promoPrice ? promoPrice : priceAll

                        return (
                            <List.Item key={cart.id}>
                                <Image avatar src={picture} />
                                <List.Content>
                                    <List.Header as="h4">
                                        {Number(cart.qty)} x {cart.name}
                                    </List.Header>
                                    <List.Description>{formatter.format(actualPrice)}</List.Description>
                                </List.Content>
                            </List.Item>
                        )
                    })}
                </List>
                {carts.length > 4 && (
                    <div style={{ marginTop: "2em" }}>
                        <Button basic fluid as={Link} to="/cart" content={`... dan ${carts.length - 4} lainnya`} />
                    </div>
                )}
                <h4 className="total-cart">
                    Total: <strong>{total && formatter.format(total)}</strong>
                </h4>
                <Button
                    icon="shopping bag"
                    as={Link}
                    to="/cart"
                    content="Lihat cart"
                    color="black"
                    fluid
                    className="btn-zigzag"
                />
            </Grid.Column>
        </Grid>
    )
}

export default CartPopup
