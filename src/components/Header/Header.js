import React, { Component } from "react"
import {
    Divider,
    Menu,
    Responsive,
    Button,
    Header as HeaderComponent,
    Image,
    Icon,
    Popup,
    Transition,
    Dropdown,
    Input,
    Form
} from "semantic-ui-react"
import CartPopup from "./CartPopup"
import Prompt from "../common/Prompt"
import { connect } from "react-redux"
import { signoutUser } from "../state/actions/authActions"
import { fetchUser } from "../state/actions/userActions"
import { fetchCategories, searchProducts } from "../state/actions/productActions"
import { fetchCarts, createCart, deleteCart, getTotalCarts } from "../state/actions/cartActions"
import { Link, NavLink, withRouter } from "react-router-dom"
import Logo from "../common/Logo"
import Emoji from "../common/Emoji"
import { mobile, imageCategory, idKoko, photoURL } from "../common"
import Tag from "../common/Tag"

class Header extends Component {
    state = { activeItem: "home", selectedCart: null, prompt: false, searchModal: false, category: true }

    componentDidMount() {
        const auth = localStorage.getItem("token")
        if (auth) {
            this.props.fetchCarts()
            this.props.fetchUser()
        }
        window.addEventListener("scroll", this.handleHide)
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleHide)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.auth !== prevProps.auth) {
            this.props.fetchUser()
        }

        if (prevState.category !== this.state.category) {
            this.handleHide()
        }

        // if (this.props.carts) {
        //     if (this.props.carts !== prevProps.carts) {
        //         this.props.fetchCarts()
        //     }
        // }
    }

    handleHide = () => {
        const YOffset = window.scrollY
        if (YOffset > 2000) this.setState({ category: false })
        else this.setState({ category: true })
    }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    handleDeleteCart = cartId => () => {
        this.props.deleteCart(cartId)
    }

    handleOpenPrompt = () => this.setState({ prompt: true })

    handleGetTotal = () => {
        const prices = this.props.carts && this.props.carts.map(cart => Number(cart.price))

        this.props.getTotalCarts(prices)
    }

    handleSignoutUser = () => {
        this.setState({ prompt: false }, () => this.props.signoutUser())
    }

    handleChange = (e, { value }) => this.setState({ keyword: value })

    handleSubmitSearch = () => {
        const { keyword } = this.state
        this.handleCloseModal()
        if (keyword && keyword.length > 2) {
            localStorage.setItem("keyword", keyword)
            this.props.history.push("/search/results")
        }
    }

    handleOpenModal = () => this.setState({ searchModal: true })
    handleCloseModal = () => this.setState({ searchModal: false })

    render() {
        const { carts, user, total, loading, match, history, categoryOptions } = this.props
        const { prompt } = this.state
        const url = match && match.url
        const avatar =
            user && user.picture !== "" && user.picture !== null
                ? photoURL + "/" + user.picture
                : "https://react.semantic-ui.com/images/avatar/small/steve.jpg"

        return (
            <React.Fragment>
                <Responsive as={Menu} minWidth={415} stackable pointing className="menu-navbar">
                    <Menu.Item as={NavLink} to="/" className="logo">
                        <Logo size="mini" />
                    </Menu.Item>
                    <Responsive as={Dropdown} minWidth={768} pointing item text="Produk kami">
                        <Dropdown.Menu>
                            <Dropdown.Header content="Pilih kategori" />
                            {categoryOptions &&
                                categoryOptions.map(item => (
                                    <Dropdown.Item
                                        as={Link}
                                        to={`/category/${item.value.toLowerCase()}`}
                                        key={item.key}
                                        {...item}
                                    />
                                ))}
                        </Dropdown.Menu>
                    </Responsive>
                    <Responsive as={Menu.Item} minWidth={768}>
                        <Button
                            as={Link}
                            icon="plus"
                            content="Deposit"
                            to="/deposit"
                            size="small"
                            className="btn-zigzag"
                        />
                    </Responsive>
                    <Menu.Menu position="right">
                        <Menu.Item>
                            <Link to="/search/results">
                                <Icon name="search" />
                            </Link>
                            {/* <Searchable
                                className="transparent"
                                products={products}
                                onCloseModal={this.handleCloseModal}
                                user={user}
                            /> */}
                        </Menu.Item>
                        <Menu.Item>
                            <Transition visible={carts && carts.length > 0} animation="swing up" duration={700}>
                                <Tag
                                    floating
                                    color="red"
                                    size="small"
                                    className="cart-notifier"
                                    text={carts && carts.length}
                                />
                            </Transition>
                            <Popup
                                trigger={
                                    <Button
                                        icon="shopping basket"
                                        content={mobile ? "Cart kamu" : ""}
                                        basic
                                        className="cart-button navbar"
                                    />
                                }
                                position="bottom right"
                                wide
                                flowing
                                hoverable
                            >
                                <CartPopup
                                    carts={carts ? carts : ""}
                                    user={user}
                                    deleteCart={this.handleDeleteCart}
                                    total={total}
                                />
                            </Popup>
                        </Menu.Item>
                        <Menu.Item>
                            <Popup
                                trigger={
                                    <Button
                                        icon="user circle outline"
                                        content={mobile ? "Profil kamu" : ""}
                                        basic
                                        className="cart-button navbar"
                                    />
                                }
                                wide
                                flowing
                                hoverable={true}
                                position="bottom right"
                            >
                                <Menu text vertical>
                                    {user ? (
                                        <React.Fragment>
                                            <Menu.Item>
                                                <div className="user-avatar">
                                                    <div>
                                                        <Image size="tiny" circular src={avatar} />
                                                    </div>
                                                    <div>
                                                        <HeaderComponent
                                                            as="h4"
                                                            content={`Hi, ${user.name && user.name.split(" ")[0]}`}
                                                        />
                                                        {user && user.id !== idKoko && (
                                                            <span onClick={() => history.push("/profile/deposit")}>
                                                                Deposit kamu <br />
                                                                {user.deposit === 0 ? (
                                                                    <span>
                                                                        Masih Rp 0 &nbsp;{" "}
                                                                        <Emoji label="smh" symbol="😑" />
                                                                    </span>
                                                                ) : (
                                                                    <strong>Rp {user.deposit},00</strong>
                                                                )}
                                                            </span>
                                                        )}
                                                    </div>
                                                </div>
                                            </Menu.Item>
                                            <Menu.Item as={Link} to="/profile">
                                                <Icon name="smile outline" /> Profile kamu
                                            </Menu.Item>
                                            <Menu.Item as={Link} to="/profile/wishlist">
                                                <Icon name="heart" /> Wishlist
                                            </Menu.Item>
                                            <Menu.Item as={Link} to="/profile/history">
                                                <Icon name="file alternate outline" /> Histori
                                            </Menu.Item>
                                            <Divider />
                                            <Menu.Item as={Link} to="/profile/settings">
                                                <Icon name="keyboard outline" /> Settings
                                            </Menu.Item>
                                            <Menu.Item onClick={this.handleOpenPrompt}>Log out</Menu.Item>
                                        </React.Fragment>
                                    ) : (
                                        <Menu.Item as={Link} to="/login">
                                            <Icon name="user outline" /> Login
                                        </Menu.Item>
                                    )}
                                </Menu>
                            </Popup>
                        </Menu.Item>
                    </Menu.Menu>
                    <Prompt
                        header="Logout?"
                        open={prompt}
                        onClose={() => this.setState({ prompt: false })}
                        confirm={this.handleSignoutUser}
                        yesText="Pastinya"
                        size="tiny"
                    >
                        Yakin mau log out, nih? Serius?
                    </Prompt>
                </Responsive>
                <Responsive maxWidth={414}>
                    {url === "/" ? (
                        <Logo size="tiny" />
                    ) : (
                        <Button
                            basic
                            onClick={this.props.history.goBack}
                            icon="chevron left"
                            content="Kembali"
                            className="navigator"
                        />
                    )}
                    <Menu fixed="top" borderless fluid className="menu-navbar-mobile">
                        <Menu.Item as={NavLink} to="/">
                            <Logo size="mini" />
                        </Menu.Item>
                        <Menu.Item as={NavLink} to="/search/results">
                            <Button className="link-btn" basic circular icon>
                                <Icon name="search" />
                            </Button>
                        </Menu.Item>
                        <Menu.Menu position="right">
                            {user ? (
                                <Menu.Item>
                                    <Popup
                                        // on="click"
                                        position="bottom right"
                                        hideOnScroll
                                        trigger={
                                            <Button
                                                icon="user circle outline"
                                                content="Profil kamu"
                                                basic
                                                className="cart-button navbar"
                                            />
                                        }
                                        wide
                                        flowing
                                        hoverable
                                    >
                                        <Menu text vertical>
                                            <Menu.Item as={Link} to="/deposit">
                                                <Button fluid basic icon="plus" content="Tambah deposit" />
                                            </Menu.Item>
                                            <Menu.Item as={Link} to="/profile">
                                                <Icon name="smile outline" /> Profile kamu
                                            </Menu.Item>
                                            <Menu.Item as={Link} to="/profile/wishlist">
                                                <Icon name="heart" /> Wishlist
                                            </Menu.Item>
                                            <Menu.Item as={Link} to="/profile/history">
                                                <Icon name="file alternate outline" /> Histori
                                            </Menu.Item>
                                            <Divider />
                                            <Menu.Item as={Link} to="/profile/settings">
                                                <Icon name="keyboard outline" /> Settings
                                            </Menu.Item>
                                            <Menu.Item onClick={this.handleOpenPrompt}>Log out</Menu.Item>
                                        </Menu>
                                    </Popup>
                                </Menu.Item>
                            ) : (
                                <Menu.Item>
                                    <Button
                                        as={Link}
                                        to="/login"
                                        basic
                                        className="link-btn"
                                        icon="user circle outline"
                                        content=" Login"
                                    />
                                </Menu.Item>
                            )}

                            <Menu.Item>
                                <Transition visible={carts && carts.length > 0} animation="swing up" duration={700}>
                                    <Tag floating color="red" size="small" text={carts && carts.length} />
                                </Transition>
                                <Popup
                                    on="click"
                                    hideOnScroll
                                    trigger={
                                        <Button
                                            icon="shopping basket"
                                            content={mobile ? "Cart kamu" : ""}
                                            basic
                                            className="cart-button navbar"
                                        />
                                    }
                                    position="bottom right"
                                    wide
                                    flowing
                                    hoverable={true}
                                >
                                    <CartPopup
                                        carts={carts ? carts : ""}
                                        user={user}
                                        deleteCart={this.handleDeleteCart}
                                        total={total}
                                        getTotalCarts={this.handleGetTotal}
                                    />
                                </Popup>
                            </Menu.Item>
                        </Menu.Menu>
                        <Prompt
                            loading={loading}
                            header="Logout?"
                            open={prompt}
                            onClose={() => this.setState({ prompt: false })}
                            confirm={this.handleSignoutUser}
                            yesText="Pastinya"
                            size="tiny"
                        >
                            Yakin mau log out, nih? Serius?
                        </Prompt>
                    </Menu>
                    <Transition visible={this.state.category} animation="fly up" duration={300}>
                        <Menu fixed="bottom" className="menu-category">
                            <Dropdown closeOnBlur fluid item icon="ellipsis vertical">
                                <Dropdown.Menu>
                                    <Dropdown.Header content="Kategori" />
                                    <Dropdown.Item as={Link} to="/category/tas" content="Tas" />
                                    <Dropdown.Item as={Link} to="/category/sepatu" content="Sepatu" />
                                    <Dropdown.Item as={Link} to="/category/beauty" content="Beauty" />
                                    <Dropdown.Item as={Link} to="/category/dompet" content="Dompet" />
                                    <Dropdown.Item as={Link} to="/category/lingerie" content="Lingerie" />
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu>
                    </Transition>
                </Responsive>
            </React.Fragment>
        )
    }
}

const actionList = {
    signoutUser,
    fetchCarts,
    deleteCart,
    createCart,
    fetchUser,
    getTotalCarts,
    fetchCategories,
    searchProducts
}

// prettier-ignore
const mapState = ({ cart, user, auth, products }) => {
    const pic = user && user.user && user.user.picture
    const picture = user && pic && pic !== null ? pic : "https://react.semantic-ui.com/images/avatar/small/steve.jpg"
    const categoryOptions = products.categories && products.categories
        .filter(item => item.parent === 0)
        .map(item => ({
            key: item.id,
            text: item.name,
            value: item.name,
            image: { avatar: true, src: imageCategory + "/" + item.picture }
        }))

    // const total = cart.carts && cart.carts.length > 0 && cart.carts
    //     .map(item => ({ price: JSON.parse(item.price), qty: item.qty }))
    //     .flat()
    //     .map(item => {
    //         const pricey = item.price
    //             .filter(price => {
    //                 if (user.user) {
    //                     if (user.user.id === idKoko) return price.price_type === member
    //                     else {
    //                         if (user.user.acc_type === 1) return price.price_type === member
    //                         else if (user.user.acc_type === 2) return price.price_type === vip
    //                         else return price.price_type === partner
    //                     }
    //                 } else return price.price_type === member
    //             })
    //             .map(item => item.price)
    //         return pricey * item.qty
    //     })
    //     .reduce((acc, curr) => acc + curr, 0)

    const total = cart.carts && cart.carts.map(item => item.total_price).reduce((acc, curr) => acc + curr, 0);


    return {
        total,
        carts: cart.carts,
        user: user.user,
        auth: auth.authenticated,
        categories: products.categories,
        categoryOptions,
        products: products.products,
        picture
    }
}

export default connect(
    mapState,
    actionList
)(withRouter(Header))
