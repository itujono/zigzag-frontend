import React from "react"
import { Grid, Header, List, Divider } from "semantic-ui-react"
import Logo from "../common/Logo"
import { mobile } from "../common"
import { Link } from "react-router-dom"

const Footer = () => {
    return (
        <Grid centered stackable columns={1} divided={!mobile} relaxed className="footer">
            <Grid.Column>
                <Logo size="small" />
            </Grid.Column>
            {/* <Grid.Column>
                <Header as="h4" content="Quick Links" />
                <List relaxed>
                    <List.Item as={Link} to="/info/about">Tentang Zigzag</List.Item>
                    <List.Item as={Link} to="/">Bermitra</List.Item>
                    <Divider />
                    <List.Item as={Link} to="/">Kebijakan Privasi</List.Item>
                    <List.Item as={Link} to="/">FAQ</List.Item>
                    <List.Item as={Link} to="/">Ketentuan Pengguna</List.Item>
                </List>
            </Grid.Column>
            <Grid.Column>
                <Header as="h4" content="How to" />
                <List relaxed>
                    <List.Item as={Link} to="/">Cara Order</List.Item>
                    <List.Item as={Link} to="/">Retur Barang</List.Item>
                    <List.Item as={Link} to="/">Refund Dana</List.Item>
                    <Divider />
                    <List.Item as={Link} to="/">Konfirmasi Pembayaran</List.Item>
                    <List.Item as={Link} to="/">Deposit</List.Item>
                </List>
            </Grid.Column>
            <Grid.Column>
                <Header as="h4" content="Payments" />
                <List relaxed>
                    <List.Item as={Link} to="/">Tentang Zigzag</List.Item>
                    <List.Item as={Link} to="/">Bermitra</List.Item>
                    <Divider />
                    <List.Item as={Link} to="/">Kebijakan Privasi</List.Item>
                    <List.Item as={Link} to="/">FAQ</List.Item>
                    <List.Item as={Link} to="/">Ketentuan Pengguna</List.Item>
                </List>
            </Grid.Column> */}
        </Grid>
    )
}

export default Footer
