const webpack = require("webpack")
const CleanWebPackPlugin = require("clean-webpack-plugin")
const HtmlWebPackPlugin = require("html-webpack-plugin")
const ExtractTextWebpackPlugin = require("extract-text-webpack-plugin")
const commonPaths = require("./common-paths")
const NameAllModulesPlugin = require("name-all-modules-plugin")
const path = require("path")

const config = {
    entry: "./src/index.js",
    output: {
        filename: "[name].[chunkhash].js",
        path: commonPaths.outputPath,
        publicPath: "/"
    },
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "eslint-loader",
                options: {
                    failOnWarning: true,
                    failOnerror: true
                },
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        cacheDirectory: true
                    }
                }
            },
            {
                test: /\.s?css$/,
                use: ExtractTextWebpackPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: "css-loader"
                        },
                        {
                            loader: "sass-loader"
                        }
                    ]
                })
                //exclude: /node_modules/
            },
            {
                test: /\.svg|.png|.jpg$/,
                loader: "url-loader",
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.NamedChunksPlugin(chunk => {
            if (chunk.name) {
                return chunk.name
            }
            let n = []
            chunk.forEachModule(m => n.push(path.relative(m.context, m.request)))
            return n.join("_")
        }),
        new webpack.ProgressPlugin(),
        new ExtractTextWebpackPlugin("styles.css"),
        new webpack.optimize.CommonsChunkPlugin({
            minChunks: Infinity,
            name: "vendor"
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "runtime"
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     filename: "common.js",
        //     minChunks: 3,
        //     name: "common"
        // }),
        new CleanWebPackPlugin(["public"], { root: commonPaths.root }),
        new HtmlWebPackPlugin({
            template: commonPaths.template,
            favicon: commonPaths.favicon,
            inject: true,
            title: "Caching"
        }),
        new NameAllModulesPlugin()
    ]
}

module.exports = config
