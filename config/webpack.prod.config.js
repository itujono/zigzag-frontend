const UglifyJsWebpackPlugin = require("uglifyjs-webpack-plugin")
const webpack = require("webpack")
const dotenv = require("dotenv")

const env = dotenv.config().parsed

const envKeys =
    env &&
    Object.keys(env).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(env[next])
        return prev
    }, {})

const config = {
    devtool: "source-map",
    devServer: {
        historyApiFallback: true
    },
    output: {
        publicPath: "/"
    },
    plugins: [
        new UglifyJsWebpackPlugin({
            sourceMap: true
        }),
        new webpack.DefinePlugin(envKeys)
    ]
}

module.exports = config
