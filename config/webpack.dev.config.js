const commonPaths = require('./common-paths');
const webpack = require('webpack');
const dotenv = require('dotenv')


const env = dotenv.config().parsed

const envKeys = env && Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next]);
    return prev;
  }, {});

const config = {
    devtool: 'inline-source-map',
    devServer: {
        contentBase: commonPaths.outputPath,
        compress: true,
        historyApiFallback: true,
        hot: false,
        port: 9000
    },
    plugins: [
        new webpack.DefinePlugin(envKeys)
    ]
};

module.exports = config;